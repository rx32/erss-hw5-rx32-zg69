## Run
**Modify corresponding .config files in resource**
- sudo docker-compose up # database
- sudo ./mvnw install -DskipTests
- sudo ./mvnw spring-boot:run

## Reference
### 1. Dataset: Product details on Flipkart
- From: DATASET BY PROMPTCLOUD
- Availability: https://data.world/promptcloud/product-details-on-flipkart-com
### 2. Frontend Template:
- From: furgan html template
- Availability: https://themeforest.net/item/furgan-furniture-store-html-template/25902850
### 3. Data parsing
