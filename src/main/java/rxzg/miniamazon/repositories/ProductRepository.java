package rxzg.miniamazon.repositories;

//import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
//import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rxzg.miniamazon.model.Product;

import java.util.List;


@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
  List<Product> findProductsByProductName(String name);
}
