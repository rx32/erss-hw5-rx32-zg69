package rxzg.miniamazon.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import rxzg.miniamazon.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
  Role findRoleByName(String name);
}
