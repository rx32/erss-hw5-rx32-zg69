package rxzg.miniamazon.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import rxzg.miniamazon.model.Url;

@Repository
public interface UrlRepository extends CrudRepository<Url, Long> {

}
