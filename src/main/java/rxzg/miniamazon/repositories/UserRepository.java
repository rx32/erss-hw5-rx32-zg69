package rxzg.miniamazon.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import rxzg.miniamazon.model.User;

import java.util.Optional;


public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
}
