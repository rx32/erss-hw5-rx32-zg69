package rxzg.miniamazon.repositories;

import org.springframework.data.repository.CrudRepository;
import rxzg.miniamazon.model.Shipment;

public interface ShipmentRepository extends CrudRepository<Shipment, Long> {

}
