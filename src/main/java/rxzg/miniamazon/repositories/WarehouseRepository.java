package rxzg.miniamazon.repositories;

import org.springframework.data.repository.CrudRepository;
import rxzg.miniamazon.model.Warehouse;

public interface WarehouseRepository extends CrudRepository<Warehouse, Integer> {
}
