package rxzg.miniamazon.controllers.dto;

import lombok.Data;

@Data
public class PurchaseMoreProductDTO {
  private long productID;
  private String productName;
  private String productDesc;
  private int warehouseID;
  private int quantity;
  private double productPrice;
}
