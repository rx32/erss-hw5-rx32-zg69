package rxzg.miniamazon.controllers;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rxzg.miniamazon.controllers.dto.OrderInfoDTO;
import rxzg.miniamazon.model.Shipment;
import rxzg.miniamazon.services.ShipmentService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Set;


@RequestMapping
@RestController
public class ShipmentController {
    @Autowired
    ShipmentService shipmentService;

    /**
     * Return -1: product quantity is not enough in warehouse
     * On success, return shipment id.
     * @param id
     * @param orderInfoDTO (quantity and address)
     * @return
     */

    @PostMapping(path = "/buy/{id}")
    public Number buyProductById(Principal principal, @PathVariable("id") long id, @Valid @NonNull @RequestBody OrderInfoDTO orderInfoDTO) {
        //should save shipment order to User. How to find user after login?
        String userName = principal.getName();
        return shipmentService.buyProductById(id, orderInfoDTO, userName);
    }

    @GetMapping(path = "/checkAllStatus")
    public Set<Shipment> checkAllStatus(Principal principal) {
        String userName = principal.getName();
        //String userName = "adminadmin";
        System.out.println("UserName: " + userName);
        if (userName == null || userName.length() == 0) return null;
        return shipmentService.checkAllStatus(userName);
    }
    @RequestMapping("/whoAmI")   
    public String showCurrentUser(Principal principal) {
        return principal.getName();
    }
}
