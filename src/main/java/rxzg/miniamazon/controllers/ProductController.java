package rxzg.miniamazon.controllers;


import org.apache.lucene.search.Query;


//import org.hibernate.search.jpa.FullTextEntityManager;
//import org.hibernate.search.jpa.FullTextQuery;
//
//import org.hibernate.search.jpa.Search;
//import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rxzg.miniamazon.controllers.dto.PurchaseMoreProductDTO;
import rxzg.miniamazon.exceptions.InvalidProductReferenceException;
import rxzg.miniamazon.exceptions.InvalidWarehouseIDException;
import rxzg.miniamazon.model.Product;
import rxzg.miniamazon.services.ProductService;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
public class ProductController {
  @Autowired
  ProductService productService;

  @PostMapping("/purchaseMoreProducts")
  public Product createProduct(@RequestBody PurchaseMoreProductDTO purchaseMoreProductDTO) {
    try {
      return productService.purchaseMoreProduct(purchaseMoreProductDTO);
    } catch (InvalidWarehouseIDException e) {
      // when the user enter an non-existing warehouseID, catch exception
      System.err.println(e.getMessage());
    }
    return null;
  }

  @GetMapping("/viewAll")
  public Iterable<Product> viewAllProduct() {
    return productService.getAllProducts();
  }

  @GetMapping("/view/{id}")
  public Product viewProductById(@PathVariable("id") Long id) {
    Optional<Product> product = productService.getProduct(id);
    if(product.isPresent()) {
      return product.get();
    }

    throw new InvalidProductReferenceException("Invalid transaction reference provided");
  }

  @GetMapping("/search/name={name}")
  public List<Product> fuzzySearch(@PathVariable("name") String name) throws InterruptedException {
    if (name == null) {
      name = "";
    }
    return productService.fuzzySearch(name);
  }

  @GetMapping("/view/name={name}")
  public List<Product> viewProductsByProductName(@PathVariable("name") String name) {
    return productService.getProductsByProductName(name);
  }

}
