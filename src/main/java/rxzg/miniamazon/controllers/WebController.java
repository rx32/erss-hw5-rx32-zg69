package rxzg.miniamazon.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import rxzg.miniamazon.model.Product;
import rxzg.miniamazon.repositories.ProductRepository;
import rxzg.miniamazon.services.ProductService;
import rxzg.miniamazon.services.ShipmentService;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class WebController {

  @Autowired
  ProductService productService;

  @Autowired
  ProductRepository productRepository;

  @Autowired
  ShipmentService shipmentService;

  @GetMapping(value = "/")
  public String index(Model model) {
    model.addAttribute("products", productRepository.findAll());
    return "index";
  }

  @RequestMapping(value = "/product/{id}")
  public String productView(Model model, @PathVariable long id) {
    Optional<Product> productOpt = productRepository.findById(id);
    if (productOpt.isPresent()) {
      model.addAttribute("product", productOpt.get());
      return "single-product";
    } else {
      return "404";
    }
  }

  @RequestMapping(value = "/checkout")
  public String checkoutView() {
    return "checkout";
  }

  @RequestMapping(value = "/wishlist")
  public String wishListView() {
    return "wishlist";
  }

  @RequestMapping(value = "/cart")
  public String cartView() {
    return "cart";
  }

  @RequestMapping(value = "/order")
  public String ordersView(Principal principal, Model model) {
    model.addAttribute("shipments", shipmentService.checkAllStatus(principal.getName()));
    return "order";
  }

  @RequestMapping(value = "/search")
  public String searchView(Principal principal, Model model, @RequestParam(value = "name", required = false) String name) {
    if (name == null) {
      name = "";
    }
    model.addAttribute("products", productService.fuzzySearch(name));
    return "shop-list";
  }

  @GetMapping(value = "/search/{name}")
  public String searchViewWithParam(Principal principal, Model model, @PathVariable String name) {
    if (name == null) {
      name = "";
    }
    model.addAttribute("products", productService.fuzzySearch(name));
    return "shop-list";
  }

}
