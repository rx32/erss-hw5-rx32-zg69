package rxzg.miniamazon.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:config.properties")
@ConfigurationProperties(prefix = "mini-amazon-connection")
@Getter
@Setter
public class ConfigProperties {
    private String worldHostname;
    private int worldPort;
    private int worldUpsPort;
    private int upsPort;
    private boolean test;
    private long worldID;
}
