package rxzg.miniamazon.init;

import javax.persistence.*;
import javax.transaction.*;

import org.hibernate.search.jpa.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.*;

@Component
public class ProductSearchInit implements ApplicationListener<ContextRefreshedEvent> {

        @PersistenceContext
        private EntityManager entityManager;

    /***************************************************************************************
   *    Title:Full-text search with Hibernate Search (Lucene) — part 1
   *    Author: Wojciech Krzywiec
   *    Date: May 6, 2018 
   *    Code version: 1
   *    Availability:https://medium.com/@wkrzywiec/full-text-search-with-hibernate-search-lucene-part-1-e245b889aa8e
   *
   ***************************************************************************************/

        @Override
        @Transactional
        public void onApplicationEvent(ContextRefreshedEvent event) {
            System.out.println("executing entityManager");
            FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
            try {
                fullTextEntityManager.createIndexer().startAndWait();
            } catch (InterruptedException e) {
                System.out.println("Error occured trying to build Hibernate Search indexes "
                        + e.toString());
            }
        }
}
