package rxzg.miniamazon.protobuf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import rxzg.miniamazon.config.ConfigProperties;

import rxzg.miniamazon.model.Product;

import rxzg.miniamazon.protobuf.WorldAmazon.*;
import rxzg.miniamazon.protobuf.utils.SequenceNumManager;
import rxzg.miniamazon.repositories.ProductRepository;
import rxzg.miniamazon.repositories.WarehouseRepository;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

@Component
public class WorldConnector {

  OutputStream outputStream;
  InputStream inputStream;

  @Autowired
  ConfigProperties configProperties;

  @Autowired
  SequenceNumManager sequenceNumManager;

  @Autowired
  WorldHandler worldHandler;

  @Autowired
  WarehouseRepository warehouseRepository;
  @Autowired
  ProductRepository productRepository;


//  public WorldConnector(ConfigProperties configProperties, ProductRepository productRepository, WarehouseRepository warehouseRepository, SequenseNumManager sequenseNumManager) {
//    this.configProperties = configProperties;
//    this. productRepository = productRepository;
//    this.warehouseRepository = warehouseRepository;
//    this.sequenseNumManager = sequenseNumManager;
//  }


  public void checkHostnameAndPort() {
    System.out.println("hostname: " + configProperties.getWorldHostname());
    System.out.println("port: " + configProperties.getWorldPort());
    System.out.println("ups-port: " + configProperties.getWorldUpsPort());
  }

//  public void createWorld() throws ExecutionException, InterruptedException {
//    CompletableFuture<Void> completableFuture = CompletableFuture.runAsync(() -> {
//      Socket clientSocket = null;
//      try {
//        clientSocket = new Socket(configProperties.getHostname(), configProperties.getUpsPort());
//        outputStream = clientSocket.getOutputStream();
//        inputStream = clientSocket.getInputStream();
//        WorldUps.UConnect.newBuilder()
//            .setIsAmazon(false)
//            .setWorldid(1)
//            .build()
//            .writeDelimitedTo(outputStream);
//        WorldUps.UConnected uConnected = WorldUps.UConnected.parseDelimitedFrom(inputStream);
//        System.out.println("----- response -----");
//        System.out.println(uConnected);
//      } catch (IOException e) {
//        e.printStackTrace();
//        throw new RuntimeException("error.");
//      }
//    });
//    completableFuture.get();
//  }

  public void connect() throws IOException {

    Socket clientSocket = new Socket(configProperties.getWorldHostname(), configProperties.getWorldPort());
    outputStream = clientSocket.getOutputStream();
    inputStream = clientSocket.getInputStream();
    AInitWarehouse.Builder aInitWarehouseBuilder = AInitWarehouse.newBuilder();
    AConnect.Builder aConnectBuilder = AConnect.newBuilder();
    aConnectBuilder.setIsAmazon(true);
    warehouseRepository.findAll().forEach(warehouse -> {
      aInitWarehouseBuilder
              .setId(warehouse.getWarehouseID())
              .setX(warehouse.getX())
              .setY(warehouse.getY());
      aConnectBuilder.addInitwh(aInitWarehouseBuilder.build());
    });


    if (!configProperties.isTest()) {
      aConnectBuilder.setWorldid(configProperties.getWorldID());
    }

    aConnectBuilder.build().writeDelimitedTo(outputStream);

    AConnected aConnected = AConnected.parseDelimitedFrom(inputStream);
    System.out.println(aConnected);
    //initialize product
    Iterable<Product> products = productRepository.findAll();
    products.forEach(product -> {
      worldHandler.sendPurchaseMoreByProduct(product);
    });
  }


  public void serve() {
    CompletableFuture.runAsync(this::handleWorldResponse);
  }

  public void handleWorldResponse() {
    while (true) {
      try {
        AResponses aResponses = AResponses.parseDelimitedFrom(inputStream);

        // filter out repeated response and record new sequence numbers
        sequenceNumManager.removeFromRetryPool(aResponses);
        aResponses = sequenceNumManager.recordAndFilterResponse(aResponses);
        worldHandler.responseHandler(aResponses);

      } catch (IOException e) {
        e.printStackTrace();
      }
    }

  }




  public void sendACommands(ACommands aCommands, long seq) throws IOException {
    sequenceNumManager.addToRetryService(aCommands, seq);
    try {
      writeToOutputStream(aCommands);
    } catch (SocketException e) {
      connect();
      // fail twice, ignore
    } catch (IOException ignore) {}

  }

  public void sendAResponsesACK(long seq) throws IOException {
    System.out.println("[World Connector] send ack " + seq);
    writeToOutputStream(ACommands.newBuilder().addAcks(seq).build());
  }

  public synchronized void writeToOutputStream(ACommands aCommands) throws IOException {
    aCommands.writeDelimitedTo(outputStream);
  }




}
