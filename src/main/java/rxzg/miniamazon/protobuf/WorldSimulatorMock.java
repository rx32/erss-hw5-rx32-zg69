package rxzg.miniamazon.protobuf;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import rxzg.miniamazon.protobuf.WorldAmazon.*;

public class WorldSimulatorMock {
  private ServerSocket serverSocket;
  private int port = 12345;

  public WorldSimulatorMock() throws IOException {
    serverSocket = new ServerSocket(port);
  }

  public void handleConnection() {
    System.out.println("Waiting for client message...");

    //
    // The server do a loop here to accept all connection initiated by the
    // client application.
    //
    while (true) {
      try {
        Socket socket = serverSocket.accept();
        new ConnectionHandler(socket);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public static void main(String args[]) throws IOException, ExecutionException, InterruptedException {
    WorldSimulatorMock worldSimulatorMock = new WorldSimulatorMock();
    CompletableFuture.runAsync(()->{
      try {
        Socket clientSocket = new Socket("localhost", 12345);
        System.out.println("Connected");
      } catch (IOException e) {
        e.printStackTrace();
      }
    });
    worldSimulatorMock.handleConnection();
  }

}

class ConnectionHandler implements Runnable {
  private Socket socket;

  public ConnectionHandler(Socket socket) {
    this.socket = socket;

    Thread t = new Thread(this);
    t.start();
  }

  public void run() {
    try {
      //
      // Read a message sent by client application
      //

//      CodedInputStream cis = CodedInputStream.newInstance(socket
//          .getInputStream());

      System.out.println("Just connected to "
          + socket.getRemoteSocketAddress());

      AConnect aConnect = AConnect.parseDelimitedFrom(socket.getInputStream());

      System.out.println("Responce: " + aConnect);

      socket.close();

      System.out.println("Waiting for client message...");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}