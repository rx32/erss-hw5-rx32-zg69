package rxzg.miniamazon.protobuf.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import rxzg.miniamazon.protobuf.WorldAmazon;

import java.util.Date;

@Data
@AllArgsConstructor
public class ACommandsRequestInfo {
  private Date date;
  // Simplify: Assume in world server, we only send one command per ACommands.
  private long seq;
  private WorldAmazon.ACommands aCommands;
}
