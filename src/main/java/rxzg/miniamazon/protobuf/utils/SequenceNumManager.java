package rxzg.miniamazon.protobuf.utils;

import org.aspectj.weaver.World;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import rxzg.miniamazon.protobuf.WorldAmazon;
import rxzg.miniamazon.protobuf.WorldConnector;

import java.io.IOException;
import java.net.SocketException;
import java.util.Date;
import java.util.HashSet;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class SequenceNumManager {

  @Autowired
  WorldConnector worldConnector;

  private final AtomicLong seq = new AtomicLong();
  private final ConcurrentHashMap<Long, Integer> worldFinishedSeqSet = new ConcurrentHashMap<>();

  private final ConcurrentLinkedQueue<ACommandsRequestInfo> requestQueue = new ConcurrentLinkedQueue<>();
  private final ConcurrentHashMap<Long, Integer> retryPool = new ConcurrentHashMap<>();


  public void serve() {
    CompletableFuture.runAsync(this::retryService);
  }

  public void retryService() {
    while (true) {
      try {
        // CONFIG: Sleep 5 sec
        TimeUnit.MILLISECONDS.sleep(5000);
        // CONFIG: Time out in 5 sec
        System.out.println("[Retry Service] Wake Up: check timeout request: ");
        while (!requestQueue.isEmpty() && isTimeOut(requestQueue.peek().getDate(), 5000)) {
          ACommandsRequestInfo aCommandsRequestInfo = requestQueue.poll();
          assert aCommandsRequestInfo != null;
          // lazy deletion
          if (retryPool.containsKey(aCommandsRequestInfo.getSeq())) {
            // retry
            System.out.println("[Retry Service] retry--------------------");
            System.out.println(aCommandsRequestInfo.getACommands());
            System.out.println("[Retry Service] -------------------------");
            worldConnector.sendACommands(aCommandsRequestInfo.getACommands(), aCommandsRequestInfo.getSeq());
            System.out.println("[Retry Service] request sent");
          }
        }
      } catch (InterruptedException | IOException ignore) {
      }
    }
  }

  public WorldAmazon.AResponses recordAndFilterResponse(WorldAmazon.AResponses aResponses) {
    WorldAmazon.AResponses.Builder aResponsesFilteredOutFinishRequestBuilder = WorldAmazon.AResponses.newBuilder();

    System.out.println("[Retry Service] response received");
    System.out.println(aResponses);
    for (WorldAmazon.APurchaseMore aPurchaseMore : aResponses.getArrivedList()) {
      if (!worldFinishedSeqSet.containsKey(aPurchaseMore.getSeqnum())) {
        worldFinishedSeqSet.put(aPurchaseMore.getSeqnum(), 0);
        aResponsesFilteredOutFinishRequestBuilder.addArrived(aPurchaseMore);
      } else {
        System.out.println("[Retry Service] filter out " + aPurchaseMore.getSeqnum());
      }
    }

    for (WorldAmazon.APacked aPacked : aResponses.getReadyList()) {
      if (!worldFinishedSeqSet.containsKey(aPacked.getSeqnum())) {
        worldFinishedSeqSet.put(aPacked.getSeqnum(), 0);
        aResponsesFilteredOutFinishRequestBuilder.addReady(aPacked);
      }
    }

    for (WorldAmazon.ALoaded aLoaded : aResponses.getLoadedList()) {
      if (!worldFinishedSeqSet.containsKey(aLoaded.getSeqnum())) {
        worldFinishedSeqSet.put(aLoaded.getSeqnum(), 0);
        aResponsesFilteredOutFinishRequestBuilder.addLoaded(aLoaded);
      }
    }

    for (WorldAmazon.APackage aPackage : aResponses.getPackagestatusList()) {
      if (!worldFinishedSeqSet.containsKey(aPackage.getSeqnum())) {
        worldFinishedSeqSet.put(aPackage.getSeqnum(), 0);
        aResponsesFilteredOutFinishRequestBuilder.addPackagestatus(aPackage);
      }
    }

    for (WorldAmazon.AErr aErr : aResponses.getErrorList()) {
      // ignore for now
      if (!worldFinishedSeqSet.containsKey(aErr.getSeqnum())) {
        worldFinishedSeqSet.put(aErr.getSeqnum(), 0);
        aResponsesFilteredOutFinishRequestBuilder.addError(aErr);
      }
    }

    return aResponsesFilteredOutFinishRequestBuilder.build();
  }

  private boolean isTimeOut(Date sentDate, long threshold) {
    return (new Date().getTime() - sentDate.getTime()) > threshold;
  }

  public long getSeq() {
    return seq.getAndAdd(1);
  }

  public void addToRetryService(WorldAmazon.ACommands aCommands, long seq) {
    System.out.println("[Retry Service] add request to retry pool");
    requestQueue.offer(new ACommandsRequestInfo(new Date(), seq, aCommands));
    retryPool.put(seq, 0);
    System.out.println("[Retry Service] current queue: ");
    System.out.println(requestQueue);
  }

  public void removeFromRetryPool(WorldAmazon.AResponses aResponses) {
    // record
    for (long ack : aResponses.getAcksList()) {
      System.out.println("[Retry Service] received ack: " + ack);
      retryPool.remove(ack);
    }
  }


}
