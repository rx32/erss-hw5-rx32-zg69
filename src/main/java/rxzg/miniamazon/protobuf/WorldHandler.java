package rxzg.miniamazon.protobuf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import rxzg.miniamazon.controllers.dto.PurchaseMoreProductDTO;
import rxzg.miniamazon.model.Product;
import rxzg.miniamazon.model.Shipment;
import rxzg.miniamazon.model.Warehouse;
import rxzg.miniamazon.protobuf.WorldAmazon.*;
import rxzg.miniamazon.protobuf.utils.SequenceNumManager;
import rxzg.miniamazon.repositories.ProductRepository;
import rxzg.miniamazon.repositories.ShipmentRepository;
import rxzg.miniamazon.repositories.WarehouseRepository;

import javax.swing.text.html.Option;
import java.io.IOException;
import java.util.Optional;

@Component
public class WorldHandler {
    @Autowired
    SequenceNumManager sequenceNumManager;

    @Autowired
    WorldConnector worldConnector;

    @Autowired
    UPSConnector upsConnector;


    @Autowired
    ProductRepository productRepository;
    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    ShipmentRepository shipmentRepository;

    public void sendPurchaseMoreByProduct(Product product) {
        String productDec = product.getProductDesc();
        if (productDec.length() >= 50) {
            productDec = productDec.substring(0, 50);
        }
        WorldAmazon.AProduct aProduct = AProduct.newBuilder()
                .setId(product.getProductID())
                .setCount(product.getQuantity())
                .setDescription(productDec)
                .build();
        long seq = sequenceNumManager.getSeq();
        APurchaseMore aPurchaseMore = APurchaseMore.newBuilder()
                .setSeqnum(seq)
                .addThings(aProduct)
                .setWhnum(product.getWarehouse().getWarehouseID())
                .build();
        try {
            ACommands aCommands = ACommands.newBuilder().addBuy(aPurchaseMore).build();
            worldConnector.sendACommands(aCommands, seq);
        } catch (IOException e) {
            // we do not need to handle this exception, retry mechanism takes care of it.
            e.printStackTrace();
        }
    }

    public void sendPurchaseMore(PurchaseMoreProductDTO purchaseMoreProductDTO) {
        String productDec = purchaseMoreProductDTO.getProductDesc();
        if (productDec.length() >= 50) {
            productDec = productDec.substring(0, 50);
        }
        WorldAmazon.AProduct aProduct = AProduct.newBuilder()
                .setId(purchaseMoreProductDTO.getProductID())
                .setCount(purchaseMoreProductDTO.getQuantity())
                .setDescription(productDec)
                .build();
        long seq = sequenceNumManager.getSeq();
        APurchaseMore aPurchaseMore = APurchaseMore.newBuilder()
                .setSeqnum(seq)
                .addThings(aProduct)
                .setWhnum(purchaseMoreProductDTO.getWarehouseID())
                .build();
        try {
            ACommands aCommands = ACommands.newBuilder().addBuy(aPurchaseMore).build();
            worldConnector.sendACommands(aCommands, seq);
        } catch (IOException e) {
            // we do not need to handle this exception, retry mechanism takes care of it.
            e.printStackTrace();
        }
    }

    public void sendPackReq(Shipment shipment) {
        AProduct aProduct = generateAProduct(shipment.getProduct(), shipment.getQuantity());
        long seq = sequenceNumManager.getSeq();
        APack aPack = APack.newBuilder()
                .setSeqnum(seq)
                .setShipid(shipment.getShipID())
                .setWhnum(shipment.getProduct().getWarehouse().getWarehouseID())
                .addThings(aProduct)
                .build();
        try {
            ACommands aCommands = ACommands.newBuilder().addTopack(aPack).build();
            worldConnector.sendACommands(aCommands, seq);
        } catch (IOException e) {
            // we do not need to handle this exception, retry mechanism takes care of it.
            e.printStackTrace();
        }

    }

    public void sendPutOnTruckReq(Shipment shipment) {
        long seq = sequenceNumManager.getSeq();
        APutOnTruck aPutOnTruck = APutOnTruck.newBuilder()
                .setShipid(shipment.getShipID())
                .setTruckid((int)shipment.getTruckID())
                .setWhnum(shipment.getProduct().getWarehouse().getWarehouseID())
                .setSeqnum(seq).build();
        try {
            ACommands aCommands = ACommands.newBuilder().addLoad(aPutOnTruck).build();
            worldConnector.sendACommands(aCommands, seq);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendQueryReq(long shipmentId) {
        long seq = sequenceNumManager.getSeq();
        AQuery aQuery = AQuery.newBuilder()
                .setPackageid(shipmentId)
                .setSeqnum(seq).build();
        try {
            ACommands aCommands = ACommands.newBuilder().addQueries(aQuery).build();
            worldConnector.sendACommands(aCommands, seq);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * transfer Product->AProduct: generate message Aproduct according to Product and quantity
     * @param product
     * @param quantity
     * @return
     */
    public AProduct generateAProduct(Product product, int quantity) {
        String productDec = product.getProductDesc();
        if (productDec.length() >= 50) {
            productDec = productDec.substring(0, 50);
        }
        AProduct aProduct = AProduct.newBuilder()
                .setId(product.getProductID())
                .setDescription(productDec)
                .setCount(quantity)
                .build();
        return aProduct;
    }
    public void handleArrived(APurchaseMore aPurchaseMore) {
        int whnum = aPurchaseMore.getWhnum();
        Optional<Warehouse> warehouse = warehouseRepository.findById(whnum);
        for (AProduct aProduct : aPurchaseMore.getThingsList()) {
            long id = aProduct.getId();
            Optional<Product> productOpt = productRepository.findById(id);
            Product product;
            // product have already been added into database, when web server received purchaseMoreProduct request
            if (productOpt.isPresent()) {
                product = productOpt.get();
                product.setQuantity(product.getQuantity() + aProduct.getCount());
                productRepository.save(product);
            } else {
                System.err.println("Product missing! Check if product is added in purchaseMoreProduct handler.");
            }
        }
    }

    public void handleArrivedPack(APacked apacked) {
        Optional<Shipment> shipmentOpt = shipmentRepository.findById(apacked.getShipid());
        if (shipmentOpt.isPresent()) {
            Shipment shipment = shipmentOpt.get();
            shipment.setStatus("packed");
            shipmentRepository.save(shipment);
            if (shipment.getTruckID() != 0) {
                sendPutOnTruckReq(shipment);
                shipment.setStatus("loading");
                shipmentRepository.save(shipment);
            }
        }
        else {
            System.err.println("Ship id not found in APacked response");
        }
    }

    public void handleArrivedLoaded(ALoaded aLoaded) {
        Optional<Shipment> shipmentOptional = shipmentRepository.findById(aLoaded.getShipid());
        if (shipmentOptional.isPresent()) {
            Shipment shipment = shipmentOptional.get();
            shipment.setStatus("loaded");
            shipmentRepository.save(shipment);
            upsConnector.sendAULoaded(shipment);
            shipment.setStatus("delivering");
            shipmentRepository.save(shipment);
        }
    }

    public void responseHandler(AResponses aResponses) throws IOException {
        for (APurchaseMore aPurchaseMore : aResponses.getArrivedList()) {
            // business logic
            handleArrived(aPurchaseMore);
            //  ack
            worldConnector.sendAResponsesACK(aPurchaseMore.getSeqnum());
        }

        for (APacked aPacked : aResponses.getReadyList()) {
            handleArrivedPack(aPacked);
            worldConnector.sendAResponsesACK(aPacked.getSeqnum());
        }

        for (ALoaded aLoaded : aResponses.getLoadedList()) {
            handleArrivedLoaded(aLoaded);
        }

        for (APackage aPackage : aResponses.getPackagestatusList()) {
            worldConnector.sendAResponsesACK(aPackage.getSeqnum());
        }

        for (AErr aErr : aResponses.getErrorList()) {
            worldConnector.sendAResponsesACK(aErr.getSeqnum());
        }
    }

}
