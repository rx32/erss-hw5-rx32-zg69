package rxzg.miniamazon.protobuf;

import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import rxzg.miniamazon.config.ConfigProperties;
import rxzg.miniamazon.model.Shipment;
import rxzg.miniamazon.protobuf.utils.SequenceNumManager;
import rxzg.miniamazon.repositories.ProductRepository;
import rxzg.miniamazon.repositories.ShipmentRepository;
import rxzg.miniamazon.repositories.WarehouseRepository;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class UPSConnector {

  private final AtomicLong seq = new AtomicLong();

  ServerSocket serverSocket;
  Socket clientSocket;
  OutputStream outputStream;
  InputStream inputStream;

  @Autowired
  ConfigProperties configProperties;

  @Autowired
  ProductRepository productRepository;
  @Autowired
  WarehouseRepository warehouseRepository;
  @Autowired
  SequenceNumManager sequenceNumManager;
  @Autowired
  ShipmentRepository shipmentRepository;
  @Autowired
  WorldHandler worldHandler;

  public void checkHostnameAndPort() {
    if (!configProperties.isTest()) {
      System.out.println("UPS port: " + configProperties.getUpsPort());
    }
  }


  public void connect() throws IOException {
    if (!configProperties.isTest()) {
      serverSocket = new ServerSocket(configProperties.getUpsPort());
      clientSocket = serverSocket.accept();
      outputStream = clientSocket.getOutputStream();
      inputStream = clientSocket.getInputStream();

      AmazonUps.UAConnect uaConnect = AmazonUps.UAConnect.parseDelimitedFrom(inputStream);

      configProperties.setWorldID(uaConnect.getWorldid());

      AmazonUps.AUConnectResp auConnectResp = AmazonUps.AUConnectResp.newBuilder()
          .setAck(uaConnect.getSeqnum())
          .build();
      auConnectResp.writeDelimitedTo(outputStream);

    }

  }

  public void serve() {
    CompletableFuture.runAsync(this::handleWorldResponse);
  }

  public void handleWorldResponse() {
    while (true) {
      try {
        AmazonUps.UACommands uaCommands = AmazonUps.UACommands.parseDelimitedFrom(inputStream);

        for (AmazonUps.UAGoPickUpResp uaGoPickUpResp : uaCommands.getGopickuprespList()) {
          handleGoPickUpResp(uaGoPickUpResp);
        }

        for (AmazonUps.UATruckArrived uaTruckArrived : uaCommands.getTruckarrivedList()) {
          handleTruckArrived(uaTruckArrived);
          sendAUTruckArrivedResp(uaTruckArrived);
        }

        for (AmazonUps.UALoadedResp uaLoadedResp : uaCommands.getLoadedrespList()) {
          handleUALoadedResp(uaLoadedResp);
        }

        for (AmazonUps.UAReturnStatus uaReturnStatus : uaCommands.getReturnstatusList()) {
          handleUAReturnStatus(uaReturnStatus);
          sendAUReturnStatusResp(uaReturnStatus);
        }

        for (AmazonUps.UACheckStatusResp uaCheckStatusResp : uaCommands.getCheckstatusrespList()) {
          handleUACheckStatusResp(uaCheckStatusResp);
        }

        for (AmazonUps.UADelivered uaDelivered : uaCommands.getDeliveredList()) {
          handleUADelivered(uaDelivered);
          sendAUDeliveredResp(uaDelivered);
        }

      } catch (IOException e) {
        e.printStackTrace();
      }
    }

  }

  private void handleGoPickUpResp(AmazonUps.UAGoPickUpResp uaGoPickUpResp) {
    System.out.println(uaGoPickUpResp);
  }

  private void handleTruckArrived(AmazonUps.UATruckArrived uaTruckArrived) {
    List<Long> shipList = uaTruckArrived.getShipidsList();
    for (Long shipid: shipList) {
      Optional<Shipment> shipmentOptional = shipmentRepository.findById(shipid);

      if (shipmentOptional.isPresent()) {
        Shipment shipment = shipmentOptional.get();
        shipment.setTruckID(uaTruckArrived.getTruckid());
        if (shipment.getStatus().equals("packed")) {
          worldHandler.sendPutOnTruckReq(shipment);
          shipment.setStatus("loading");          
        }
        shipmentRepository.save(shipment);
      }
    }

    System.out.println(uaTruckArrived);
  }

  private void sendAUTruckArrivedResp(AmazonUps.UATruckArrived uaTruckArrived) throws IOException {
    AmazonUps.AUTruckArrivedResp auTruckArrivedResp = AmazonUps.AUTruckArrivedResp.newBuilder()
        .setAck(uaTruckArrived.getSeqnum())
        .build();
    AmazonUps.AUCommands auCommands = AmazonUps.AUCommands.newBuilder()
        .addTruckarrivedresp(auTruckArrivedResp)
        .build();
    writeToOutputStream(auCommands);
  }

  private void handleUALoadedResp(AmazonUps.UALoadedResp uaLoadedResp) {
    
    System.out.println(uaLoadedResp);
  }

  private void handleUAReturnStatus(AmazonUps.UAReturnStatus uaReturnStatus) {
    System.out.println(uaReturnStatus);
  }

  private void sendAUReturnStatusResp(AmazonUps.UAReturnStatus uaReturnStatus) throws IOException {
    AmazonUps.AUReturnStatusResp auReturnStatusResp = AmazonUps.AUReturnStatusResp.newBuilder()
        .setAck(uaReturnStatus.getSeqnum())
        .build();
    AmazonUps.AUCommands auCommands = AmazonUps.AUCommands.newBuilder()
        .addReturnstatusresp(auReturnStatusResp)
        .build();
    writeToOutputStream(auCommands);
  }

  private void handleUACheckStatusResp(AmazonUps.UACheckStatusResp uaCheckStatusResp) {
    System.out.println(uaCheckStatusResp);
  }

  private void handleUADelivered(AmazonUps.UADelivered uaDelivered) {
    Optional<Shipment> shipmentOptional = shipmentRepository.findById(uaDelivered.getShipid());
    if (shipmentOptional.isPresent()) {
      Shipment shipment = shipmentOptional.get();
      shipment.setStatus("delivered");
      shipmentRepository.save(shipment);
    }
    System.out.println(uaDelivered);
  }

  private void sendAUDeliveredResp (AmazonUps.UADelivered uaDelivered) throws IOException {
    AmazonUps.AUDeliveredResp auDeliveredResp = AmazonUps.AUDeliveredResp.newBuilder()
        .setAck(uaDelivered.getSeqnum())
        .build();
    AmazonUps.AUCommands auCommands = AmazonUps.AUCommands.newBuilder()
        .addDeliveredresp(auDeliveredResp)
        .build();
    writeToOutputStream(auCommands);
  }

  public synchronized void writeToOutputStream(AmazonUps.AUCommands auCommands) throws IOException {
    auCommands.writeDelimitedTo(outputStream);
  }

  public void sendAULoaded(Shipment shipment) {
    AmazonUps.AULoaded auLoaded = AmazonUps.AULoaded.newBuilder()
            .setSeqnum(seq.getAndAdd(1))
            .setShipid(shipment.getShipID())
            .setTruckid(shipment.getTruckID())
            .build();
    AmazonUps.AUCommands auCommands = AmazonUps.AUCommands.newBuilder()
            .addLoaded(auLoaded)
            .build();
    try {
      writeToOutputStream(auCommands);
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void sendAUGoPickUp(Shipment shipment) {
    AmazonUps.AUGoPickUp auGoPickUp = AmazonUps.AUGoPickUp.newBuilder()
        .setSeqnum(seq.getAndAdd(1))
        .setWhid(shipment.getProduct().getWarehouse().getWarehouseID())
        .setDstX(shipment.getAddressX())
        .setDstY(shipment.getAddressY())
        .setShipid(shipment.getShipID())
        .setProductCount(shipment.getQuantity())
        .setProductName(shipment.getProduct().getProductName())
        .setUpsAccount("No account")
        .build();
    AmazonUps.AUCommands auCommands = AmazonUps.AUCommands.newBuilder()
        .addGopickup(auGoPickUp)
        .build();
    try {
      writeToOutputStream(auCommands);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
