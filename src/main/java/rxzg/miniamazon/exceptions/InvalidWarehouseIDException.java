package rxzg.miniamazon.exceptions;

public class InvalidWarehouseIDException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public InvalidWarehouseIDException(String errorMessage) {
    super(errorMessage);
  }

}
