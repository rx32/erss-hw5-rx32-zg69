package rxzg.miniamazon.exceptions;

public class InvalidProductReferenceException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public InvalidProductReferenceException(String errorMessage) {
    super(errorMessage);
  }

}
