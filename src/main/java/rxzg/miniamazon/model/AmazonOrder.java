package rxzg.miniamazon.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class AmazonOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long orderID;

    @OneToMany(
            targetEntity = Shipment.class,
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "os_fk", referencedColumnName = "orderID")
    private Set<Shipment> shipments = new HashSet<>();
}
