package rxzg.miniamazon.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User {
   @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "USER_ID")
  private Long userID;

  @Size(min = 6, max = 14)
  private String username;
  private String password;

  @Transient
  private String passwordConfirm;

  @ManyToMany
  private Set<Role> roles;

  @OneToMany(
          targetEntity = Shipment.class,
          cascade = CascadeType.ALL
  )
  @JoinColumn(name = "us_fk", referencedColumnName = "USER_ID")
  private Set<Shipment> orders = new HashSet<>();

  public void addShipment(Shipment shipment) {
    orders.add(shipment);
  }
}
