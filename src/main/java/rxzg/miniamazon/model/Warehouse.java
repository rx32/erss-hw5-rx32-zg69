package rxzg.miniamazon.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class Warehouse {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "WAREHOUSE_ID", unique = true, nullable = false)
    private int warehouseID;
    private int x;
    private int y;

}