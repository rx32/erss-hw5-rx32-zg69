package rxzg.miniamazon.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Shipment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "SHIP_ID", unique = true, nullable = false)
    private long shipID;
    private long truckID;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "PRODUCT_ID")
    private Product product;
    private int quantity;
    private int addressX;
    private int addressY;
    private String status; // [订单确认，运送中，已送达]

}

