package rxzg.miniamazon.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import lombok.*;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TermVector;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Indexed
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "PRODUCT_ID", unique = true, nullable = false)
  private long productID;

  private int quantity;

  @OneToOne(cascade = CascadeType.REMOVE)
  @JoinColumn(name = "WAREHOUSE_ID")
  private Warehouse warehouse;

  @Field(termVector = TermVector.YES)
  //@CsvBindByPosition(position = 0)
  //@CsvBindByName(column = "product_name")
  private String productName;


  //@CsvBindByName(column = "product_category_tree")
  @Field(termVector = TermVector.YES)
  @Size(max = 1024)
  private String productDesc;

  //@CsvBindByName(column = "retail_price")
  private double productPrice;

  //@CsvBindByName(column = "discounted_price")
  private double dicountPrice;

  //@CsvBindByName(column = "image")
  @OneToMany(
          targetEntity = Url.class,
          cascade = CascadeType.REMOVE
  )
  @JoinColumn(name = "pu_fk", referencedColumnName = "PRODUCT_ID")
  private Set<Url> productUrl = new HashSet<>();

  @Field(termVector = TermVector.YES)
  private String catalog;

  @Field(termVector = TermVector.YES)
  private String brand;

  public void addUrl(Url url) {
    productUrl.add(url);
  }

}




