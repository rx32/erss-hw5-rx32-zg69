package rxzg.miniamazon.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import rxzg.miniamazon.model.Role;
import rxzg.miniamazon.model.User;
import rxzg.miniamazon.repositories.RoleRepository;
import rxzg.miniamazon.repositories.UserRepository;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;


@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Transactional
    public void addAdmin() {
        Optional<User> adminOpt = findByUsername("adminadmin");
        if (!adminOpt.isPresent()) {
            User admin = new User();
            admin.setUsername("adminadmin");
            admin.setPassword("adminadmin");
            admin.setRoles(new HashSet<Role>() {{
                add(roleRepository.findRoleByName("ROLE_ADMIN"));
                add(roleRepository.findRoleByName("ROLE_USER"));
            }});
            save(admin);
        }

    }
}
