package rxzg.miniamazon.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rxzg.miniamazon.model.Role;
import rxzg.miniamazon.repositories.RoleRepository;

import javax.transaction.Transactional;
import java.util.HashSet;

@Service
public class RoleService {

  @Autowired
  RoleRepository roleRepository;

  public Role findRoleByName(String name) {
    return roleRepository.findRoleByName(name);
  }

  public void save(Role role) {
    roleRepository.save(role);
  }

  @Transactional
  public void addRoles() {
    Role adminRole = findRoleByName("ROLE_ADMIN");
    if (adminRole == null) {
      adminRole = new Role();
      adminRole.setName("ROLE_ADMIN");
      adminRole.setUsers(new HashSet<>());
      save(adminRole);
    }
    Role userRole = findRoleByName("ROLE_USER");
    if (userRole == null) {
      userRole = new Role();
      userRole.setName("ROLE_USER");
      userRole.setUsers(new HashSet<>());
      save(userRole);
    }

  }

}
