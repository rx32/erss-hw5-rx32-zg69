package rxzg.miniamazon.services;

import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rxzg.miniamazon.controllers.dto.PurchaseMoreProductDTO;
import rxzg.miniamazon.exceptions.InvalidWarehouseIDException;
import rxzg.miniamazon.model.Product;
import rxzg.miniamazon.model.Warehouse;
import rxzg.miniamazon.protobuf.WorldConnector;
import rxzg.miniamazon.protobuf.WorldHandler;
import rxzg.miniamazon.repositories.ProductRepository;
import rxzg.miniamazon.repositories.WarehouseRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

@Service
public class ProductService {

  @Autowired
  ProductRepository productRepository;

  @Autowired
  WarehouseRepository warehouseRepository;

  @Autowired
  WorldConnector worldConnector;

  @Autowired
  WorldHandler worldHandler;

  @PersistenceContext
  private EntityManager entityManager;


  public void saveProduct(Product product) {
    productRepository.save(product);
  }

  public Iterable<Product> getAllProducts() {
    return productRepository.findAll();
  }

  public Optional<Product> getProduct(Long id) {
    return productRepository.findById(id);
  }

  public List<Product> getProductsByProductName(String name) {
    return productRepository.findProductsByProductName(name);
  }

  public Product purchaseMoreProduct(PurchaseMoreProductDTO purchaseMoreProductDTO) {
    long productID = purchaseMoreProductDTO.getProductID();
    Optional<Product> productOpt = productRepository.findById(productID);
    Product product;
    if (productOpt.isPresent()) {
      product = productOpt.get();
    } else {
      product = new Product();
      product.setProductID(productID);
      product.setProductName(purchaseMoreProductDTO.getProductName());
      product.setProductDesc(purchaseMoreProductDTO.getProductDesc());
      product.setProductPrice(purchaseMoreProductDTO.getProductPrice());
      Optional<Warehouse> warehouseOpt = warehouseRepository.findById(purchaseMoreProductDTO.getWarehouseID());
      if (warehouseOpt.isPresent()) {
        product.setWarehouse(warehouseOpt.get());
        productRepository.save(product);
      } else {
        System.err.println("warehouse not found");
        throw new InvalidWarehouseIDException("warehouse ID does not exist");
      }
    }
    worldHandler.sendPurchaseMore(purchaseMoreProductDTO);
    return product;
  }

  public List<Product> fuzzySearch(String name) {
    if (name == "") {
      List<Product> res = new ArrayList<>();
      productRepository.findAll().forEach(res::add); 
      return res;
    }

    FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

    QueryBuilder queryBuilder =  fullTextEntityManager.getSearchFactory()
            .buildQueryBuilder()
            .forEntity(Product.class)
            .get();
    org.apache.lucene.search.Query fuzzyQuery = queryBuilder
            .keyword()
            .fuzzy()
            .withEditDistanceUpTo(1)
            .withPrefixLength(0)
            //.wildcard()
            .onFields("productName", "productDesc", "catalog", "brand")
            .matching(name + "*")
            .createQuery();
    FullTextQuery jpaQuery = fullTextEntityManager.createFullTextQuery(fuzzyQuery, Product.class);
    return jpaQuery.getResultList();
  }


}