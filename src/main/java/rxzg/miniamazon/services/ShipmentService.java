package rxzg.miniamazon.services;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rxzg.miniamazon.controllers.dto.OrderInfoDTO;
import rxzg.miniamazon.model.Shipment;
import rxzg.miniamazon.model.User;
import rxzg.miniamazon.protobuf.UPSConnector;
import rxzg.miniamazon.protobuf.WorldHandler;
import rxzg.miniamazon.repositories.ProductRepository;
import rxzg.miniamazon.repositories.ShipmentRepository;
import rxzg.miniamazon.repositories.UserRepository;
import rxzg.miniamazon.config.ConfigProperties;


import java.io.IOException;
import java.util.Optional;
import java.util.Set;

@Service
public class ShipmentService {
    @Autowired
    ShipmentRepository shipmentRepository;

    @Autowired  //instantiation
    ProductRepository productRepository;

    @Autowired
    WorldHandler worldHandler;
    @Autowired
    UPSConnector upsConnector;
    @Autowired
    UserRepository userRepository;

    @Autowired
    ConfigProperties configProperties;

    public void saveShipment(Shipment shipment) {
        shipmentRepository.save(shipment);
    }

    public Number buyProductById(long id, OrderInfoDTO orderInfoDTO, String userName) {
        System.out.println("Username: " + userName);
        int quantity = orderInfoDTO.getQuantity();
        int addressX = orderInfoDTO.getX();
        int addressY = orderInfoDTO.getY();
        return productRepository.findById(id)
                .map(p->{
                    if (p.getQuantity() >= quantity) {
                        p.setQuantity(p.getQuantity() - quantity);
                        Shipment shipment = new Shipment();
                        shipment.setAddressX(addressX);
                        shipment.setAddressY(addressY);
                        shipment.setProduct(p);
                        shipment.setStatus("packing");
                        shipment.setQuantity(quantity);
                        saveShipment(shipment);
                        Optional<User> userOpt = userRepository.findByUsername(userName);
                        if (!userOpt.isPresent()) return -1;
                        System.out.println("----------------123---------------");
                        User user = userOpt.get();
                        user.addShipment(shipment);
                        userRepository.save(user);
                        worldHandler.sendPackReq(shipment);
                        if (!configProperties.isTest()) {
                            upsConnector.sendAUGoPickUp(shipment);
                        }
                        return shipment.getShipID();
                    } else {
                        return -1;
                    }
                }).orElse(-1);
    }

    public Set<Shipment> checkAllStatus(String userName) {
        Optional<User> userOptional = userRepository.findByUsername(userName);
        if (!userOptional.isPresent()) return null;
        User user = userOptional.get();
        Set<Shipment> shipments = user.getOrders();
        return  shipments;
    }

    public int sendToUPS() {
        return 0;
    }
}
