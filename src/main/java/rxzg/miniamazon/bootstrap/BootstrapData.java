package rxzg.miniamazon.bootstrap;

import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.hibernate.boot.model.source.spi.ColumnBindingDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import rxzg.miniamazon.model.*;
import rxzg.miniamazon.protobuf.UPSConnector;
import rxzg.miniamazon.protobuf.WorldConnector;
import rxzg.miniamazon.protobuf.utils.SequenceNumManager;
import rxzg.miniamazon.repositories.ProductRepository;
import rxzg.miniamazon.repositories.RoleRepository;
import rxzg.miniamazon.repositories.UrlRepository;
import rxzg.miniamazon.services.RoleService;
import rxzg.miniamazon.services.UserService;

import javax.transaction.Transactional;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import rxzg.miniamazon.repositories.WarehouseRepository;

@Component
public class BootstrapData implements CommandLineRunner {

  @Autowired
  WorldConnector worldConnector;

  @Autowired
  UPSConnector upsConnector;

  @Autowired
  RoleService roleService;

  @Autowired
  RoleRepository roleRepository;

  @Autowired
  SequenceNumManager sequenceNumManager;

  @Autowired
  UserService userService;

  @Autowired
  ProductRepository productRepository;

  @Autowired
  WarehouseRepository warehouseRepository;

  @Autowired
  UrlRepository urlRepository;
  private static final int warehouseNum = 5;
  private static final char SEPARATOR = ',';
  private static final char QUOTE = '"';

  @Override
  public void run(String... args) throws Exception {
           roleService.addRoles();
    userService.addAdmin();
    try {
      initializeDB();
 
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println("Do some initialization here...");

    upsConnector.checkHostnameAndPort();
    upsConnector.connect();

    worldConnector.checkHostnameAndPort();
    worldConnector.connect();

    upsConnector.serve();
    worldConnector.serve();
    sequenceNumManager.serve();



  }

  @Transactional
  public void addRoles() {
    Role adminRole = roleService.findRoleByName("ROLE_ADMIN");
    if (adminRole == null) {
      adminRole = new Role();
      adminRole.setName("ROLE_ADMIN");
      adminRole.setUsers(new HashSet<>());
      roleService.save(adminRole);
    }
    Role userRole = roleService.findRoleByName("ROLE_USER");
    if (userRole == null) {
      userRole = new Role();
      userRole.setName("ROLE_USER");
      userRole.setUsers(new HashSet<>());
      roleService.save(userRole);
    }

  }

  @Transactional
  public void addAdmin() {
    Optional<User> adminOpt = userService.findByUsername("adminadmin");
    if (!adminOpt.isPresent()) {
      User admin = new User();
      admin.setUsername("adminadmin");
      admin.setPassword("adminadmin");
      admin.setRoles(new HashSet<Role>(){{
        add(roleRepository.findRoleByName("ROLE_ADMIN"));
      }});
      userService.save(admin);
      System.out.println(admin);
    }
  }

  private void initializeDB() throws IOException {
    String file = "src/main/java/rxzg/miniamazon/data/flipkart_com-ecommerce_sample.csv";
    String line = "";
    //initialize warehouse
    for (int i = 0; i < warehouseNum; i++) {
      Warehouse warehouse = new Warehouse();
      warehouse.setY(i*50);
      warehouse.setX(i*50);
      warehouseRepository.save(warehouse);
    }

    try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
      int count = 0;
      while ((line = bufferedReader.readLine()) != null && count < 30) {
        count++;
        List<String> productInfo = parseLine(line);
        if (productInfo.size() < 6) continue;
        String productName = productInfo.get(0);
        String productCatalog = productInfo.get(1);
        String productDesc = productInfo.get(4);
        String productBrand = productInfo.get(5);
        String productImage = productInfo.get(6);
        System.out.println("url: " + productImage);
        String[] urls = parseURL(productImage);
        if (productDesc.length() >= 256) {
          productDesc = productDesc.substring(0, 255);
        }
        double productPrice = 99.99;
        double discountPrice = 99.99;
        if (productInfo.get(2) != "" && productInfo.get(3) != "") {
          try {
            productPrice = Double.parseDouble(productInfo.get(3));
            discountPrice = Double.parseDouble(productInfo.get(3));
          } catch (NumberFormatException e) {
            continue;
          }
        }
        else {
          continue;
        }


        int quantity = 100;
        int warehouseID = (int) ((Math.random() * (warehouseNum)) + 1);
        Product product = new Product();
        for (int i = 0; i < urls.length; i++) {
          if (urls[i].length() >= 256) continue;
          Url url = new Url();
          url.setUrl(urls[i]);
          product.addUrl(url);
          urlRepository.save(url);
        }
        if (product.getProductUrl() == null || product.getProductUrl().size() == 0) continue;
        product.setQuantity(quantity);
        product.setProductDesc(productDesc);
        product.setProductName(productName);
        product.setProductPrice(productPrice);
        product.setCatalog(productCatalog);
        product.setDicountPrice(discountPrice);
        product.setBrand(productBrand);
        if (warehouseRepository.findById(warehouseID).isPresent()) {
          product.setWarehouse(warehouseRepository.findById(warehouseID).get());
        }
        else {
          System.out.println("warehouse id not found");
          continue;
        }
        productRepository.save(product);
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  public static String[] parseURL(String productImage) {
    String splitBy = "\"\", \"\"";
    String[] urls = productImage.split(splitBy);
    int start = 0;
    int end = urls.length;

    if (end != 0) {
      urls[start] = urls[start].substring(4);
      urls[end - 1] = urls[end - 1].substring(0, urls[end - 1].length() - 3);
    }
    return urls;

  }


 /***************************************************************************************
   *    Title: How to read and parse CSV file in Java
   *    Author: mkyong
   *    Date: April 25, 2013
   *    Code version: 1
   *    Availability: https://mkyong.com/java/how-to-read-and-parse-csv-file-in-java/
   *
   ***************************************************************************************/
  public static List<String> parseLine(String cvsLine) {

    List<String> result = new ArrayList<>();

    //if empty, return!
    if (cvsLine == null && cvsLine.isEmpty()) {
      return result;
    }

      char customQuote = QUOTE;
      char separators = SEPARATOR;


    StringBuffer curVal = new StringBuffer();
    boolean inQuotes = false;
    boolean startCollectChar = false;
    boolean doubleQuotesInColumn = false;

    char[] chars = cvsLine.toCharArray();

    for (char ch : chars) {

      if (inQuotes) {
        startCollectChar = true;
        if (ch == customQuote) {
          inQuotes = false;
          doubleQuotesInColumn = false;
        } else {

          //Fixed : allow "" in custom quote enclosed
          if (ch == '\"') {
            if (!doubleQuotesInColumn) {
              curVal.append(ch);
              doubleQuotesInColumn = true;
            }
          } else {
            curVal.append(ch);
          }

        }
      } else {
        if (ch == customQuote) {

          inQuotes = true;

          //Fixed : allow "" in empty quote enclosed
          if (chars[0] != '"' && customQuote == '\"') {
            curVal.append('"');
          }

          //double quotes in column will hit this!
          if (startCollectChar) {
            curVal.append('"');
          }

        } else if (ch == separators) {

          result.add(curVal.toString());

          curVal = new StringBuffer();
          startCollectChar = false;

        } else if (ch == '\r') {
          //ignore LF characters
          continue;
        } else if (ch == '\n') {
          //the end, break!
          break;
        } else {
          curVal.append(ch);
        }
      }

    }

    result.add(curVal.toString());

    return result;
  }

  /*


   */

}
