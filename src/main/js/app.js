'use strict';

// tag::vars[]
const React = require('react'); // <1>
const ReactDOM = require('react-dom'); // <2>
const client = require('./client'); // <3>
// end::vars[]

// tag::app[]
class App extends React.Component { // <1>

	constructor(props) {
		super(props);
		this.state = {products: []};
	}

	componentDidMount() { // <2>
		client({method: 'GET', path: '/api/products'}).done(response => {
			console.log(response.entity);
			this.setState({products: response.entity._embedded.products});
		});
	}

	render() { // <3>
		return (
			<ProductList products={this.state.products}/>
		)
	}
}
// end::app[]

// tag::employee-list[]
class ProductList extends React.Component{
	render() {
		const products = this.props.products.map(product =>
			<Product key={product._links.self.href} product={product}/>
		);
		return (
			<table>
				<tbody>
					<tr>
						<th>Product Name</th>
						<th>Description</th>
						<th>Price</th>
					</tr>
					{products}
				</tbody>
			</table>
		)
	}
}
// end::employee-list[]

// tag::employee[]
class Product extends React.Component{
	render() {
		return (
			<tr>
				<td>{this.props.product.productName}</td>
				<td>{this.props.product.productDesc}</td>
				<td>{this.props.product.productPrice}</td>
			</tr>
		)
	}
}
// end::employee[]

// tag::render[]
ReactDOM.render(
	<App />,
	document.getElementById('react')
)
// end::render[]
