import axios from 'axios'

export const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS";

const fetchProductsSuccess = products => ({
  type: FETCH_PRODUCTS_SUCCESS,
  payload: products
});

// fetch products
export const fetchProducts = products => {
  return dispatch => {
    axios.get(`http://localhost:8080/viewAll`)
        .then(res => {
          console.log(res);
          window.viewAll = res;
        });
    dispatch(fetchProductsSuccess(products));
  };
};
