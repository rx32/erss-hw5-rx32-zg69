(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[61],{

/***/ "./src/main/js/src/components/product/ProductImageGallerySticky.js":
/*!*************************************************************************!*\
  !*** ./src/main/js/src/components/product/ProductImageGallerySticky.js ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



var productImageGallerySticky = function productImageGallerySticky(_ref) {
  var product = _ref.product;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "product-large-image-wrapper product-large-image-wrapper--sticky"
  }, product.discount || product["new"] ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "product-img-badges"
  }, product.discount ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "pink"
  }, "-", product.discount, "%") : "", product["new"] ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "purple"
  }, "New") : "") : "", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "product-sticky-image mb--10"
  }, product.image && product.image.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "product-sticky-image__single mb-10",
      key: key
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
      src: process.env.PUBLIC_URL + single,
      alt: "",
      className: "img-fluid"
    }));
  })));
};

productImageGallerySticky.propTypes = {
  product: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object
};
/* harmony default export */ __webpack_exports__["default"] = (productImageGallerySticky);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/pages/shop-product/ProductSticky.js":
/*!*************************************************************!*\
  !*** ./src/main/js/src/pages/shop-product/ProductSticky.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-meta-tags */ "./node_modules/react-meta-tags/lib/index.js");
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_meta_tags__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_breadcrumbs_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-breadcrumbs-dynamic */ "./node_modules/react-breadcrumbs-dynamic/dist/src/index.js");
/* harmony import */ var react_breadcrumbs_dynamic__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_breadcrumbs_dynamic__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _layouts_LayoutOne__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../layouts/LayoutOne */ "./src/main/js/src/layouts/LayoutOne.js");
/* harmony import */ var _wrappers_breadcrumb_Breadcrumb__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../wrappers/breadcrumb/Breadcrumb */ "./src/main/js/src/wrappers/breadcrumb/Breadcrumb.js");
/* harmony import */ var _wrappers_product_RelatedProductSlider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../wrappers/product/RelatedProductSlider */ "./src/main/js/src/wrappers/product/RelatedProductSlider.js");
/* harmony import */ var _wrappers_product_ProductDescriptionTab__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../wrappers/product/ProductDescriptionTab */ "./src/main/js/src/wrappers/product/ProductDescriptionTab.js");
/* harmony import */ var _wrappers_product_ProductImageDescriptionSticky__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../wrappers/product/ProductImageDescriptionSticky */ "./src/main/js/src/wrappers/product/ProductImageDescriptionSticky.js");











var ProductSticky = function ProductSticky(_ref) {
  var location = _ref.location,
      product = _ref.product;
  var pathname = location.pathname;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_meta_tags__WEBPACK_IMPORTED_MODULE_2___default.a, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("title", null, "Flone | Product Page"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "description",
    content: "Product page of flone react minimalist eCommerce template."
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_breadcrumbs_dynamic__WEBPACK_IMPORTED_MODULE_3__["BreadcrumbsItem"], {
    to: process.env.PUBLIC_URL + "/"
  }, "Home"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_breadcrumbs_dynamic__WEBPACK_IMPORTED_MODULE_3__["BreadcrumbsItem"], {
    to: process.env.PUBLIC_URL + pathname
  }, "Shop Product"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_layouts_LayoutOne__WEBPACK_IMPORTED_MODULE_5__["default"], {
    headerTop: "visible"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_breadcrumb_Breadcrumb__WEBPACK_IMPORTED_MODULE_6__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_product_ProductImageDescriptionSticky__WEBPACK_IMPORTED_MODULE_9__["default"], {
    spaceTopClass: "mt-100",
    spaceBottomClass: "mb-100",
    product: product
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_product_ProductDescriptionTab__WEBPACK_IMPORTED_MODULE_8__["default"], {
    spaceBottomClass: "pb-90",
    productFullDesc: product.fullDescription
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_product_RelatedProductSlider__WEBPACK_IMPORTED_MODULE_7__["default"], {
    spaceBottomClass: "pb-95",
    category: product.category[0]
  })));
};

ProductSticky.propTypes = {
  location: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  product: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object
};

var mapStateToProps = function mapStateToProps(state, ownProps) {
  var itemId = ownProps.match.params.id;
  return {
    product: state.productData.products.filter(function (single) {
      return single.id === itemId;
    })[0]
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_4__["connect"])(mapStateToProps)(ProductSticky));
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/wrappers/product/ProductImageDescriptionSticky.js":
/*!***************************************************************************!*\
  !*** ./src/main/js/src/wrappers/product/ProductImageDescriptionSticky.js ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var react_toast_notifications__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-toast-notifications */ "./node_modules/react-toast-notifications/dist/index.js");
/* harmony import */ var react_toast_notifications__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_toast_notifications__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_sticky_el__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-sticky-el */ "./node_modules/react-sticky-el/lib/index.js");
/* harmony import */ var react_sticky_el__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_sticky_el__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _helpers_product__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../helpers/product */ "./src/main/js/src/helpers/product.js");
/* harmony import */ var _components_product_ProductDescriptionInfo__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/product/ProductDescriptionInfo */ "./src/main/js/src/components/product/ProductDescriptionInfo.js");
/* harmony import */ var _components_product_ProductImageGallerySticky__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/product/ProductImageGallerySticky */ "./src/main/js/src/components/product/ProductImageGallerySticky.js");









var ProductImageDescriptionSticky = function ProductImageDescriptionSticky(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass,
      product = _ref.product,
      currency = _ref.currency,
      cartItems = _ref.cartItems,
      wishlistItems = _ref.wishlistItems,
      compareItems = _ref.compareItems;
  var wishlistItem = wishlistItems.filter(function (wishlistItem) {
    return wishlistItem.id === product.id;
  })[0];
  var compareItem = compareItems.filter(function (compareItem) {
    return compareItem.id === product.id;
  })[0];

  var _useToasts = Object(react_toast_notifications__WEBPACK_IMPORTED_MODULE_3__["useToasts"])(),
      addToast = _useToasts.addToast;

  var discountedPrice = Object(_helpers_product__WEBPACK_IMPORTED_MODULE_5__["getDiscountPrice"])(product.price, product.discount);
  var finalProductPrice = +(product.price * currency.currencyRate).toFixed(2);
  var finalDiscountedPrice = +(discountedPrice * currency.currencyRate).toFixed(2);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "shop-area ".concat(spaceTopClass ? spaceTopClass : "", " ").concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-lg-6 col-md-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_product_ProductImageGallerySticky__WEBPACK_IMPORTED_MODULE_7__["default"], {
    product: product
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-lg-6 col-md-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_sticky_el__WEBPACK_IMPORTED_MODULE_4___default.a, {
    boundaryElement: ".shop-area",
    style: {
      position: "relative"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_product_ProductDescriptionInfo__WEBPACK_IMPORTED_MODULE_6__["default"], {
    product: product,
    discountedPrice: discountedPrice,
    currency: currency,
    finalDiscountedPrice: finalDiscountedPrice,
    finalProductPrice: finalProductPrice,
    cartItems: cartItems,
    wishlistItem: wishlistItem,
    compareItem: compareItem,
    addToast: addToast
  }))))));
};

ProductImageDescriptionSticky.propTypes = {
  cartItems: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.array,
  compareItems: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.array,
  currency: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  product: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  wishlistItems: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.array
};

var mapStateToProps = function mapStateToProps(state) {
  return {
    currency: state.currencyData,
    cartItems: state.cartData,
    wishlistItems: state.wishlistData,
    compareItems: state.compareData
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps)(ProductImageDescriptionSticky));

/***/ })

}]);
//# sourceMappingURL=61.bundle.js.map