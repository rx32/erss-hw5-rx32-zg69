(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[17],{

/***/ "./src/main/js/src/components/product/ShopCategories.js":
/*!**************************************************************!*\
  !*** ./src/main/js/src/components/product/ShopCategories.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers_product__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../helpers/product */ "./src/main/js/src/helpers/product.js");




var ShopCategories = function ShopCategories(_ref) {
  var categories = _ref.categories,
      getSortParams = _ref.getSortParams;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "sidebar-widget"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", {
    className: "pro-sidebar-title"
  }, "Categories "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "sidebar-widget-list mt-30"
  }, categories ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "sidebar-widget-list-left"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    onClick: function onClick(e) {
      getSortParams("category", "");
      Object(_helpers_product__WEBPACK_IMPORTED_MODULE_2__["setActiveSort"])(e);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "checkmark"
  }), " All Categories"))), categories.map(function (category, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
      key: key
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "sidebar-widget-list-left"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
      onClick: function onClick(e) {
        getSortParams("category", category);
        Object(_helpers_product__WEBPACK_IMPORTED_MODULE_2__["setActiveSort"])(e);
      }
    }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "checkmark"
    }), " ", category, " ")));
  })) : "No categories found"));
};

ShopCategories.propTypes = {
  categories: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.array,
  getSortParams: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func
};
/* harmony default export */ __webpack_exports__["default"] = (ShopCategories);

/***/ }),

/***/ "./src/main/js/src/components/product/ShopColor.js":
/*!*********************************************************!*\
  !*** ./src/main/js/src/components/product/ShopColor.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers_product__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../helpers/product */ "./src/main/js/src/helpers/product.js");




var ShopColor = function ShopColor(_ref) {
  var colors = _ref.colors,
      getSortParams = _ref.getSortParams;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "sidebar-widget mt-50"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", {
    className: "pro-sidebar-title"
  }, "Color "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "sidebar-widget-list mt-20"
  }, colors ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "sidebar-widget-list-left"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    onClick: function onClick(e) {
      getSortParams("color", "");
      Object(_helpers_product__WEBPACK_IMPORTED_MODULE_2__["setActiveSort"])(e);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "checkmark"
  }), " All Colors", " "))), colors.map(function (color, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
      key: key
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "sidebar-widget-list-left"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
      onClick: function onClick(e) {
        getSortParams("color", color);
        Object(_helpers_product__WEBPACK_IMPORTED_MODULE_2__["setActiveSort"])(e);
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "checkmark"
    }), " ", color, " ")));
  })) : "No colors found"));
};

ShopColor.propTypes = {
  colors: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.array,
  getSortParams: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func
};
/* harmony default export */ __webpack_exports__["default"] = (ShopColor);

/***/ }),

/***/ "./src/main/js/src/components/product/ShopSearch.js":
/*!**********************************************************!*\
  !*** ./src/main/js/src/components/product/ShopSearch.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


var ShopSearch = function ShopSearch() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sidebar-widget"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "pro-sidebar-title"
  }, "Search "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "pro-sidebar-search mb-50 mt-25"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
    className: "pro-sidebar-search-form",
    action: "#"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "text",
    placeholder: "Search here..."
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "pe-7s-search"
  })))));
};

/* harmony default export */ __webpack_exports__["default"] = (ShopSearch);

/***/ }),

/***/ "./src/main/js/src/components/product/ShopSize.js":
/*!********************************************************!*\
  !*** ./src/main/js/src/components/product/ShopSize.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers_product__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../helpers/product */ "./src/main/js/src/helpers/product.js");




var ShopSize = function ShopSize(_ref) {
  var sizes = _ref.sizes,
      getSortParams = _ref.getSortParams;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "sidebar-widget mt-40"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", {
    className: "pro-sidebar-title"
  }, "Size "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "sidebar-widget-list mt-20"
  }, sizes ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "sidebar-widget-list-left"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    onClick: function onClick(e) {
      getSortParams("size", "");
      Object(_helpers_product__WEBPACK_IMPORTED_MODULE_2__["setActiveSort"])(e);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "checkmark"
  }), " All Sizes", " "))), sizes.map(function (size, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
      key: key
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "sidebar-widget-list-left"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
      className: "text-uppercase",
      onClick: function onClick(e) {
        getSortParams("size", size);
        Object(_helpers_product__WEBPACK_IMPORTED_MODULE_2__["setActiveSort"])(e);
      }
    }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "checkmark"
    }), size, " ")));
  })) : "No sizes found"));
};

ShopSize.propTypes = {
  getSortParams: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func,
  sizes: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.array
};
/* harmony default export */ __webpack_exports__["default"] = (ShopSize);

/***/ }),

/***/ "./src/main/js/src/components/product/ShopTag.js":
/*!*******************************************************!*\
  !*** ./src/main/js/src/components/product/ShopTag.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers_product__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../helpers/product */ "./src/main/js/src/helpers/product.js");




var ShopTag = function ShopTag(_ref) {
  var tags = _ref.tags,
      getSortParams = _ref.getSortParams;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "sidebar-widget mt-50"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", {
    className: "pro-sidebar-title"
  }, "Tag "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "sidebar-widget-tag mt-25"
  }, tags ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, tags.map(function (tag, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
      key: key
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
      onClick: function onClick(e) {
        getSortParams("tag", tag);
        Object(_helpers_product__WEBPACK_IMPORTED_MODULE_2__["setActiveSort"])(e);
      }
    }, tag));
  })) : "No tags found"));
};

ShopTag.propTypes = {
  getSortParams: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func,
  tags: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.array
};
/* harmony default export */ __webpack_exports__["default"] = (ShopTag);

/***/ }),

/***/ "./src/main/js/src/components/product/ShopTopAction.js":
/*!*************************************************************!*\
  !*** ./src/main/js/src/components/product/ShopTopAction.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers_product__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../helpers/product */ "./src/main/js/src/helpers/product.js");




var ShopTopAction = function ShopTopAction(_ref) {
  var getLayout = _ref.getLayout,
      getFilterSortParams = _ref.getFilterSortParams,
      productCount = _ref.productCount,
      sortedProductCount = _ref.sortedProductCount;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "shop-top-bar mb-35"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "select-shoing-wrap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "shop-select"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("select", {
    onChange: function onChange(e) {
      return getFilterSortParams("filterSort", e.target.value);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
    value: "default"
  }, "Default"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
    value: "priceHighToLow"
  }, "Price - High to Low"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
    value: "priceLowToHigh"
  }, "Price - Low to High"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Showing ", sortedProductCount, " of ", productCount, " result")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "shop-tab"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    onClick: function onClick(e) {
      getLayout("grid two-column");
      Object(_helpers_product__WEBPACK_IMPORTED_MODULE_2__["setActiveLayout"])(e);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-th-large"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    onClick: function onClick(e) {
      getLayout("grid three-column");
      Object(_helpers_product__WEBPACK_IMPORTED_MODULE_2__["setActiveLayout"])(e);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-th"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    onClick: function onClick(e) {
      getLayout("list");
      Object(_helpers_product__WEBPACK_IMPORTED_MODULE_2__["setActiveLayout"])(e);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-list-ul"
  }))));
};

ShopTopAction.propTypes = {
  getFilterSortParams: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func,
  getLayout: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func,
  productCount: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.number,
  sortedProductCount: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.number
};
/* harmony default export */ __webpack_exports__["default"] = (ShopTopAction);

/***/ }),

/***/ "./src/main/js/src/wrappers/product/ShopSidebar.js":
/*!*********************************************************!*\
  !*** ./src/main/js/src/wrappers/product/ShopSidebar.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers_product__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../helpers/product */ "./src/main/js/src/helpers/product.js");
/* harmony import */ var _components_product_ShopSearch__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/product/ShopSearch */ "./src/main/js/src/components/product/ShopSearch.js");
/* harmony import */ var _components_product_ShopCategories__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/product/ShopCategories */ "./src/main/js/src/components/product/ShopCategories.js");
/* harmony import */ var _components_product_ShopColor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/product/ShopColor */ "./src/main/js/src/components/product/ShopColor.js");
/* harmony import */ var _components_product_ShopSize__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/product/ShopSize */ "./src/main/js/src/components/product/ShopSize.js");
/* harmony import */ var _components_product_ShopTag__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/product/ShopTag */ "./src/main/js/src/components/product/ShopTag.js");









var ShopSidebar = function ShopSidebar(_ref) {
  var products = _ref.products,
      getSortParams = _ref.getSortParams,
      sideSpaceClass = _ref.sideSpaceClass;
  var uniqueCategories = Object(_helpers_product__WEBPACK_IMPORTED_MODULE_2__["getIndividualCategories"])(products);
  var uniqueColors = Object(_helpers_product__WEBPACK_IMPORTED_MODULE_2__["getIndividualColors"])(products);
  var uniqueSizes = Object(_helpers_product__WEBPACK_IMPORTED_MODULE_2__["getProductsIndividualSizes"])(products);
  var uniqueTags = Object(_helpers_product__WEBPACK_IMPORTED_MODULE_2__["getIndividualTags"])(products);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "sidebar-style ".concat(sideSpaceClass ? sideSpaceClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_product_ShopSearch__WEBPACK_IMPORTED_MODULE_3__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_product_ShopCategories__WEBPACK_IMPORTED_MODULE_4__["default"], {
    categories: uniqueCategories,
    getSortParams: getSortParams
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_product_ShopColor__WEBPACK_IMPORTED_MODULE_5__["default"], {
    colors: uniqueColors,
    getSortParams: getSortParams
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_product_ShopSize__WEBPACK_IMPORTED_MODULE_6__["default"], {
    sizes: uniqueSizes,
    getSortParams: getSortParams
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_product_ShopTag__WEBPACK_IMPORTED_MODULE_7__["default"], {
    tags: uniqueTags,
    getSortParams: getSortParams
  }));
};

ShopSidebar.propTypes = {
  getSortParams: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func,
  products: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.array,
  sideSpaceClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (ShopSidebar);

/***/ }),

/***/ "./src/main/js/src/wrappers/product/ShopTopbar.js":
/*!********************************************************!*\
  !*** ./src/main/js/src/wrappers/product/ShopTopbar.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_product_ShopTopAction__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/product/ShopTopAction */ "./src/main/js/src/components/product/ShopTopAction.js");




var ShopTopbar = function ShopTopbar(_ref) {
  var getLayout = _ref.getLayout,
      getFilterSortParams = _ref.getFilterSortParams,
      productCount = _ref.productCount,
      sortedProductCount = _ref.sortedProductCount;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_product_ShopTopAction__WEBPACK_IMPORTED_MODULE_2__["default"], {
    getLayout: getLayout,
    getFilterSortParams: getFilterSortParams,
    productCount: productCount,
    sortedProductCount: sortedProductCount
  }));
};

ShopTopbar.propTypes = {
  getFilterSortParams: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func,
  getLayout: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func,
  productCount: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.number,
  sortedProductCount: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.number
};
/* harmony default export */ __webpack_exports__["default"] = (ShopTopbar);

/***/ })

}]);
//# sourceMappingURL=17.bundle.js.map