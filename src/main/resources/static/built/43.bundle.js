(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[43],{

/***/ "./src/main/js/src/components/header/OffcanvasMenu.js":
/*!************************************************************!*\
  !*** ./src/main/js/src/components/header/OffcanvasMenu.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _sub_components_HeaderSocial__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sub-components/HeaderSocial */ "./src/main/js/src/components/header/sub-components/HeaderSocial.js");
/* harmony import */ var _NavMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./NavMenu */ "./src/main/js/src/components/header/NavMenu.js");






var OffcanvasMenu = function OffcanvasMenu(_ref) {
  var activeState = _ref.activeState,
      getActiveState = _ref.getActiveState;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "clickable-mainmenu ".concat(activeState ? "inside" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "clickable-mainmenu-icon"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "clickable-mainmenu-close",
    onClick: function onClick() {
      return getActiveState(false);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "pe-7s-close"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "side-logo"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    to: process.env.PUBLIC_URL + "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    alt: "",
    src: process.env.PUBLIC_URL + "/assets/img/logo/logo.png"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_NavMenu__WEBPACK_IMPORTED_MODULE_4__["default"], {
    sidebarMenu: true
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_sub_components_HeaderSocial__WEBPACK_IMPORTED_MODULE_3__["default"], null));
};

OffcanvasMenu.propTypes = {
  activeState: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.bool,
  getActiveState: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func
};
/* harmony default export */ __webpack_exports__["default"] = (OffcanvasMenu);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/header/sub-components/HeaderSocial.js":
/*!**************************************************************************!*\
  !*** ./src/main/js/src/components/header/sub-components/HeaderSocial.js ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


var HeaderSocial = function HeaderSocial() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "side-social"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "facebook",
    href: "//www.facebook.com",
    target: "_blank",
    rel: "noopener noreferrer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-facebook"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "dribbble",
    href: "//www.dribbble.com",
    target: "_blank",
    rel: "noopener noreferrer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-dribbble"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "pinterest",
    href: "//www.pinterest.com",
    target: "_blank",
    rel: "noopener noreferrer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-pinterest-p"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "twitter",
    href: "//www.twitter.com",
    target: "_blank",
    rel: "noopener noreferrer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-twitter"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "linkedin",
    href: "//www.linkedin.com",
    target: "_blank",
    rel: "noopener noreferrer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-linkedin"
  })))));
};

/* harmony default export */ __webpack_exports__["default"] = (HeaderSocial);

/***/ }),

/***/ "./src/main/js/src/components/hero-slider/HeroSliderFourteenSingle.js":
/*!****************************************************************************!*\
  !*** ./src/main/js/src/components/hero-slider/HeroSliderFourteenSingle.js ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");




var HeroSliderFourteenSingle = function HeroSliderFourteenSingle(_ref) {
  var data = _ref.data,
      sliderClassName = _ref.sliderClassName;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-height-5 d-flex align-items-center bg-img ".concat(sliderClassName ? sliderClassName : ""),
    style: {
      backgroundImage: "url(".concat(process.env.PUBLIC_URL + data.image, ")")
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-xl-12 col-lg-12 col-md-12 col-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-content-6 slider-animated-1 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "animated"
  }, data.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "animated"
  }, data.subtitle), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-btn-5 btn-hover"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    className: "animated",
    to: process.env.PUBLIC_URL + data.url
  }, "SHOP NOW")))))));
};

HeroSliderFourteenSingle.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  sliderClassName: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (HeroSliderFourteenSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/image-slider/ImageSliderOneSingle.js":
/*!*************************************************************************!*\
  !*** ./src/main/js/src/components/image-slider/ImageSliderOneSingle.js ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");



var ImageSliderOneSingle = function ImageSliderOneSingle(_ref) {
  var data = _ref.data,
      sliderClass = _ref.sliderClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "single-image ".concat(sliderClass ? sliderClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + data.link
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + data.image,
    alt: ""
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (ImageSliderOneSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/newsletter/SubscribeEmailTwo.js":
/*!********************************************************************!*\
  !*** ./src/main/js/src/components/newsletter/SubscribeEmailTwo.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_mailchimp_subscribe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-mailchimp-subscribe */ "./node_modules/react-mailchimp-subscribe/es/index.js");




var CustomForm = function CustomForm(_ref) {
  var status = _ref.status,
      message = _ref.message,
      onValidated = _ref.onValidated,
      spaceTopClass = _ref.spaceTopClass,
      subscribeBtnClass = _ref.subscribeBtnClass;
  var email;

  var submit = function submit() {
    email && email.value.indexOf("@") > -1 && onValidated({
      EMAIL: email.value
    });
    email.value = "";
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "subscribe-form-3 ".concat(spaceTopClass ? spaceTopClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mc-form"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    className: "email",
    ref: function ref(node) {
      return email = node;
    },
    type: "email",
    placeholder: "Youe Email Addres",
    required: true
  })), status === "sending" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      color: "#3498db",
      fontSize: "12px"
    }
  }, "sending..."), status === "error" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      color: "#e74c3c",
      fontSize: "12px"
    },
    dangerouslySetInnerHTML: {
      __html: message
    }
  }), status === "success" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      color: "#2ecc71",
      fontSize: "12px"
    },
    dangerouslySetInnerHTML: {
      __html: message
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "clear-3 ".concat(subscribeBtnClass ? subscribeBtnClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "button",
    onClick: submit
  }, "SUBSCRIBE"))));
};

var SubscribeEmailTwo = function SubscribeEmailTwo(_ref2) {
  var mailchimpUrl = _ref2.mailchimpUrl,
      spaceTopClass = _ref2.spaceTopClass,
      subscribeBtnClass = _ref2.subscribeBtnClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_mailchimp_subscribe__WEBPACK_IMPORTED_MODULE_2__["default"], {
    url: mailchimpUrl,
    render: function render(_ref3) {
      var subscribe = _ref3.subscribe,
          status = _ref3.status,
          message = _ref3.message;
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(CustomForm, {
        status: status,
        message: message,
        onValidated: function onValidated(formData) {
          return subscribe(formData);
        },
        spaceTopClass: spaceTopClass,
        subscribeBtnClass: subscribeBtnClass
      });
    }
  }));
};

SubscribeEmailTwo.propTypes = {
  mailchimpUrl: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (SubscribeEmailTwo);

/***/ }),

/***/ "./src/main/js/src/components/section-title/SectionTitleWithText.js":
/*!**************************************************************************!*\
  !*** ./src/main/js/src/components/section-title/SectionTitleWithText.js ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



var SectionTitleWithText = function SectionTitleWithText(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "welcome-area ".concat(spaceTopClass ? spaceTopClass : "", " ").concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "welcome-content text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "Who Are We"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", null, "Welcome To Flone"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt labor et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commo consequat irure", " "))));
};

SectionTitleWithText.propTypes = {
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (SectionTitleWithText);

/***/ }),

/***/ "./src/main/js/src/data/hero-sliders/hero-slider-fourteen.json":
/*!*********************************************************************!*\
  !*** ./src/main/js/src/data/hero-sliders/hero-slider-fourteen.json ***!
  \*********************************************************************/
/*! exports provided: 0, 1, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"title\":\"Welcome to Flone\",\"subtitle\":\"30% off Summer Vacation\",\"image\":\"/assets/img/slider/slider-5.jpg\",\"url\":\"/shop-grid-standard\"},{\"id\":2,\"title\":\"Smart Products\",\"subtitle\":\"40% off Summer Vacation\",\"image\":\"/assets/img/slider/slider-5-1.jpg\",\"url\":\"/shop-grid-standard\"}]");

/***/ }),

/***/ "./src/main/js/src/data/image-slider/image-slider-one.json":
/*!*****************************************************************!*\
  !*** ./src/main/js/src/data/image-slider/image-slider-one.json ***!
  \*****************************************************************/
/*! exports provided: 0, 1, 2, 3, 4, 5, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":\"1\",\"image\":\"/assets/img/image-slider/image-slider-1.jpg\",\"link\":\"/shop-grid-standard\"},{\"id\":\"2\",\"image\":\"/assets/img/image-slider/image-slider-2.jpg\",\"link\":\"/shop-grid-standard\"},{\"id\":\"3\",\"image\":\"/assets/img/image-slider/image-slider-3.jpg\",\"link\":\"/shop-grid-standard\"},{\"id\":\"4\",\"image\":\"/assets/img/image-slider/image-slider-4.jpg\",\"link\":\"/shop-grid-standard\"},{\"id\":\"5\",\"image\":\"/assets/img/image-slider/image-slider-5.jpg\",\"link\":\"/shop-grid-standard\"},{\"id\":\"6\",\"image\":\"/assets/img/image-slider/image-slider-2.jpg\",\"link\":\"/shop-grid-standard\"}]");

/***/ }),

/***/ "./src/main/js/src/layouts/LayoutSeven.js":
/*!************************************************!*\
  !*** ./src/main/js/src/layouts/LayoutSeven.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wrappers_header_HeaderSix__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../wrappers/header/HeaderSix */ "./src/main/js/src/wrappers/header/HeaderSix.js");
/* harmony import */ var _wrappers_footer_FooterOne__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../wrappers/footer/FooterOne */ "./src/main/js/src/wrappers/footer/FooterOne.js");





var LayoutSeven = function LayoutSeven(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_header_HeaderSix__WEBPACK_IMPORTED_MODULE_2__["default"], {
    layout: "container-fluid"
  }), children, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_footer_FooterOne__WEBPACK_IMPORTED_MODULE_3__["default"], {
    spaceTopClass: "pt-100",
    spaceBottomClass: "pb-70"
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (LayoutSeven);
LayoutSeven.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.any
};

/***/ }),

/***/ "./src/main/js/src/pages/home/HomeFashionSix.js":
/*!******************************************************!*\
  !*** ./src/main/js/src/pages/home/HomeFashionSix.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-meta-tags */ "./node_modules/react-meta-tags/lib/index.js");
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_meta_tags__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_LayoutSeven__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../layouts/LayoutSeven */ "./src/main/js/src/layouts/LayoutSeven.js");
/* harmony import */ var _wrappers_hero_slider_HeroSliderFourteen__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../wrappers/hero-slider/HeroSliderFourteen */ "./src/main/js/src/wrappers/hero-slider/HeroSliderFourteen.js");
/* harmony import */ var _components_section_title_SectionTitleWithText__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/section-title/SectionTitleWithText */ "./src/main/js/src/components/section-title/SectionTitleWithText.js");
/* harmony import */ var _wrappers_product_TabProductEight__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../wrappers/product/TabProductEight */ "./src/main/js/src/wrappers/product/TabProductEight.js");
/* harmony import */ var _wrappers_newsletter_NewsletterTwo__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../wrappers/newsletter/NewsletterTwo */ "./src/main/js/src/wrappers/newsletter/NewsletterTwo.js");
/* harmony import */ var _wrappers_image_slider_ImageSliderOne__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../wrappers/image-slider/ImageSliderOne */ "./src/main/js/src/wrappers/image-slider/ImageSliderOne.js");









var HomeFashionSix = function HomeFashionSix() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_meta_tags__WEBPACK_IMPORTED_MODULE_1___default.a, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", null, "Flone | Fashion Home"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: "Fashion home of flone react minimalist eCommerce template."
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_LayoutSeven__WEBPACK_IMPORTED_MODULE_2__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_hero_slider_HeroSliderFourteen__WEBPACK_IMPORTED_MODULE_3__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_section_title_SectionTitleWithText__WEBPACK_IMPORTED_MODULE_4__["default"], {
    spaceTopClass: "pt-95",
    spaceBottomClass: "pb-90"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_product_TabProductEight__WEBPACK_IMPORTED_MODULE_5__["default"], {
    spaceBottomClass: "pb-70",
    category: "fashion",
    sectionTitle: false
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_newsletter_NewsletterTwo__WEBPACK_IMPORTED_MODULE_6__["default"], {
    spaceBottomClass: "pb-100"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_image_slider_ImageSliderOne__WEBPACK_IMPORTED_MODULE_7__["default"], null)));
};

/* harmony default export */ __webpack_exports__["default"] = (HomeFashionSix);

/***/ }),

/***/ "./src/main/js/src/wrappers/header/HeaderSix.js":
/*!******************************************************!*\
  !*** ./src/main/js/src/wrappers/header/HeaderSix.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _components_header_IconGroup__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/header/IconGroup */ "./src/main/js/src/components/header/IconGroup.js");
/* harmony import */ var _components_header_MobileMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/header/MobileMenu */ "./src/main/js/src/components/header/MobileMenu.js");
/* harmony import */ var _components_header_OffcanvasMenu__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/header/OffcanvasMenu */ "./src/main/js/src/components/header/OffcanvasMenu.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(n); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }








var HeaderSix = function HeaderSix(_ref) {
  var layout = _ref.layout,
      headerPaddingClass = _ref.headerPaddingClass,
      headerBgClass = _ref.headerBgClass;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      _useState2 = _slicedToArray(_useState, 2),
      scroll = _useState2[0],
      setScroll = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      _useState4 = _slicedToArray(_useState3, 2),
      headerTop = _useState4[0],
      setHeaderTop = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      offcanvasActive = _useState6[0],
      setOffcanvasActive = _useState6[1];

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var header = document.querySelector(".sticky-bar");
    setHeaderTop(header.offsetTop);
    window.addEventListener("scroll", handleScroll);
    return function () {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  var handleScroll = function handleScroll() {
    setScroll(window.scrollY);
  };

  var getActiveState = function getActiveState(state) {
    setOffcanvasActive(state);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("header", {
    className: "header-area sticky-bar header-padding-3 header-res-padding clearfix transparent-bar header-hm-7 ".concat(headerBgClass ? headerBgClass : "", " ").concat(headerPaddingClass ? headerPaddingClass : "", " ").concat(scroll > headerTop ? "stick" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: layout === "container-fluid" ? layout : "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-xl-5 col-lg-6 d-none d-lg-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "clickable-menu clickable-mainmenu-active"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    onClick: function onClick() {
      setOffcanvasActive(true);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "pe-7s-menu"
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-xl-2 col-lg-2 col-md-6 col-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "logo text-center logo-hm5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    className: "sticky-none",
    to: process.env.PUBLIC_URL + "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    alt: "",
    src: "assets/img/logo/logo-2.png"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    className: "sticky-block",
    to: process.env.PUBLIC_URL + "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    alt: "",
    src: "assets/img/logo/logo.png"
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-xl-5 col-lg-4 col-md-6 col-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_header_IconGroup__WEBPACK_IMPORTED_MODULE_3__["default"], {
    iconWhiteClass: "header-right-wrap-white"
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_header_OffcanvasMenu__WEBPACK_IMPORTED_MODULE_5__["default"], {
    activeState: offcanvasActive,
    getActiveState: getActiveState
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_header_MobileMenu__WEBPACK_IMPORTED_MODULE_4__["default"], null));
};

HeaderSix.propTypes = {
  headerBgClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  headerPaddingClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  layout: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (HeaderSix);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/wrappers/hero-slider/HeroSliderFourteen.js":
/*!********************************************************************!*\
  !*** ./src/main/js/src/wrappers/hero-slider/HeroSliderFourteen.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-id-swiper */ "./node_modules/react-id-swiper/lib/index.js");
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_id_swiper__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _data_hero_sliders_hero_slider_fourteen_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../data/hero-sliders/hero-slider-fourteen.json */ "./src/main/js/src/data/hero-sliders/hero-slider-fourteen.json");
var _data_hero_sliders_hero_slider_fourteen_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/hero-sliders/hero-slider-fourteen.json */ "./src/main/js/src/data/hero-sliders/hero-slider-fourteen.json", 1);
/* harmony import */ var _components_hero_slider_HeroSliderFourteenSingle_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/hero-slider/HeroSliderFourteenSingle.js */ "./src/main/js/src/components/hero-slider/HeroSliderFourteenSingle.js");





var HeroSliderFourteen = function HeroSliderFourteen() {
  var params = {
    effect: "fade",
    loop: true,
    speed: 1000,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    watchSlidesVisibility: true,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    renderPrevButton: function renderPrevButton() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "swiper-button-prev ht-swiper-button-nav"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "pe-7s-angle-left"
      }));
    },
    renderNextButton: function renderNextButton() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "swiper-button-next ht-swiper-button-nav"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "pe-7s-angle-right"
      }));
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "slider-area"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "slider-active-2 nav-style-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_id_swiper__WEBPACK_IMPORTED_MODULE_1___default.a, params, _data_hero_sliders_hero_slider_fourteen_json__WEBPACK_IMPORTED_MODULE_2__ && _data_hero_sliders_hero_slider_fourteen_json__WEBPACK_IMPORTED_MODULE_2__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_hero_slider_HeroSliderFourteenSingle_js__WEBPACK_IMPORTED_MODULE_3__["default"], {
      sliderClassName: "swiper-slide",
      data: single,
      key: key
    });
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (HeroSliderFourteen);

/***/ }),

/***/ "./src/main/js/src/wrappers/image-slider/ImageSliderOne.js":
/*!*****************************************************************!*\
  !*** ./src/main/js/src/wrappers/image-slider/ImageSliderOne.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-id-swiper */ "./node_modules/react-id-swiper/lib/index.js");
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_id_swiper__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_image_slider_ImageSliderOneSingle__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/image-slider/ImageSliderOneSingle */ "./src/main/js/src/components/image-slider/ImageSliderOneSingle.js");
/* harmony import */ var _data_image_slider_image_slider_one_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../data/image-slider/image-slider-one.json */ "./src/main/js/src/data/image-slider/image-slider-one.json");
var _data_image_slider_image_slider_one_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/image-slider/image-slider-one.json */ "./src/main/js/src/data/image-slider/image-slider-one.json", 1);





var ImageSliderOne = function ImageSliderOne() {
  var settings = {
    loop: false,
    grabCursor: true,
    breakpoints: {
      1024: {
        slidesPerView: 5
      },
      768: {
        slidesPerView: 4
      },
      640: {
        slidesPerView: 3
      },
      320: {
        slidesPerView: 2
      }
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "image-slider-area"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "image-slider-active"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_id_swiper__WEBPACK_IMPORTED_MODULE_1___default.a, settings, _data_image_slider_image_slider_one_json__WEBPACK_IMPORTED_MODULE_3__ && _data_image_slider_image_slider_one_json__WEBPACK_IMPORTED_MODULE_3__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_image_slider_ImageSliderOneSingle__WEBPACK_IMPORTED_MODULE_2__["default"], {
      data: single,
      sliderClass: "swiper-slide",
      key: key
    });
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (ImageSliderOne);

/***/ }),

/***/ "./src/main/js/src/wrappers/newsletter/NewsletterTwo.js":
/*!**************************************************************!*\
  !*** ./src/main/js/src/wrappers/newsletter/NewsletterTwo.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_newsletter_SubscribeEmailTwo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/newsletter/SubscribeEmailTwo */ "./src/main/js/src/components/newsletter/SubscribeEmailTwo.js");




var NewsletterTwo = function NewsletterTwo(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass,
      subscribeBtnClass = _ref.subscribeBtnClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "subscribe-area-3 ".concat(spaceTopClass ? spaceTopClass : "", " ").concat(spaceBottomClass ? spaceBottomClass : "", " ")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container-fluid"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-xl-5 col-lg-7 col-md-10 ml-auto mr-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "subscribe-style-3 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", null, "Subscribe "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Subscribe to our newsletter to receive news on update"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_newsletter_SubscribeEmailTwo__WEBPACK_IMPORTED_MODULE_2__["default"], {
    mailchimpUrl: "//devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&id=05d85f18ef",
    spaceTopClass: "mt-35",
    subscribeBtnClass: subscribeBtnClass
  }))))));
};

NewsletterTwo.propTypes = {
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (NewsletterTwo);

/***/ }),

/***/ "./src/main/js/src/wrappers/product/TabProductEight.js":
/*!*************************************************************!*\
  !*** ./src/main/js/src/wrappers/product/TabProductEight.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Tab */ "./node_modules/react-bootstrap/esm/Tab.js");
/* harmony import */ var react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/Nav */ "./node_modules/react-bootstrap/esm/Nav.js");
/* harmony import */ var _ProductGrid__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ProductGrid */ "./src/main/js/src/wrappers/product/ProductGrid.js");






var TabProductEight = function TabProductEight(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass,
      bgColorClass = _ref.bgColorClass,
      category = _ref.category;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "product-area ".concat(spaceTopClass ? spaceTopClass : "", " ").concat(spaceBottomClass ? spaceBottomClass : "", " ").concat(bgColorClass ? bgColorClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_2__["default"].Container, {
    defaultActiveKey: "bestSeller"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_3__["default"], {
    variant: "pills",
    className: "product-tab-list pb-55 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_3__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_3__["default"].Link, {
    eventKey: "newArrival"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, "New Arrivals"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_3__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_3__["default"].Link, {
    eventKey: "bestSeller"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, "Best Sellers"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_3__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_3__["default"].Link, {
    eventKey: "saleItems"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, "Sale Items")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_2__["default"].Content, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_2__["default"].Pane, {
    eventKey: "newArrival"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductGrid__WEBPACK_IMPORTED_MODULE_4__["default"], {
    category: category,
    type: "new",
    limit: 8,
    spaceBottomClass: "mb-25"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_2__["default"].Pane, {
    eventKey: "bestSeller"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductGrid__WEBPACK_IMPORTED_MODULE_4__["default"], {
    category: category,
    type: "bestSeller",
    limit: 8,
    spaceBottomClass: "mb-25"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_2__["default"].Pane, {
    eventKey: "saleItems"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductGrid__WEBPACK_IMPORTED_MODULE_4__["default"], {
    category: category,
    type: "saleItems",
    limit: 8,
    spaceBottomClass: "mb-25"
  })))))));
};

TabProductEight.propTypes = {
  bgColorClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  category: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (TabProductEight);

/***/ })

}]);
//# sourceMappingURL=43.bundle.js.map