(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[70],{

/***/ "./src/main/js/src/data/hero-sliders/scroll-slider.json":
/*!**************************************************************!*\
  !*** ./src/main/js/src/data/hero-sliders/scroll-slider.json ***!
  \**************************************************************/
/*! exports provided: 0, 1, 2, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":\"1\",\"title\":\"2020 Fashion For Womens\",\"subtitle\":\"-20% Spring <br /> Snickers\",\"image\":\"/assets/img/slider/hm-11-2.png\",\"url\":\"/shop-grid-standard\"},{\"id\":\"2\",\"title\":\"2020 Fashion For Womens\",\"subtitle\":\"-30% Spring <br /> Shoes\",\"image\":\"/assets/img/slider/single-slide-2.png\",\"url\":\"/shop-grid-standard\"},{\"id\":\"3\",\"title\":\"2020 Fashion For Womens\",\"subtitle\":\"-40% Spring <br /> Shoes\",\"image\":\"/assets/img/slider/single-slide-7.png\",\"url\":\"/shop-grid-standard\"}]");

/***/ }),

/***/ "./src/main/js/src/pages/home/HomeOnepageScroll.js":
/*!*********************************************************!*\
  !*** ./src/main/js/src/pages/home/HomeOnepageScroll.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-meta-tags */ "./node_modules/react-meta-tags/lib/index.js");
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_meta_tags__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _data_hero_sliders_scroll_slider_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../data/hero-sliders/scroll-slider.json */ "./src/main/js/src/data/hero-sliders/scroll-slider.json");
var _data_hero_sliders_scroll_slider_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/hero-sliders/scroll-slider.json */ "./src/main/js/src/data/hero-sliders/scroll-slider.json", 1);
/* harmony import */ var react_fullpage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-fullpage */ "./node_modules/react-fullpage/index.js");
/* harmony import */ var react_fullpage__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_fullpage__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wrappers_header_HeaderOne__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../wrappers/header/HeaderOne */ "./src/main/js/src/wrappers/header/HeaderOne.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }








var HomeOnepageScroll = function HomeOnepageScroll() {
  var anchors = [];
  _data_hero_sliders_scroll_slider_json__WEBPACK_IMPORTED_MODULE_3__.forEach(function (element) {
    anchors.push(element.id);
  });
  var options = {
    activeClass: "active",
    // the class that is appended to the sections links
    anchors: anchors,
    // the anchors for each sections
    arrowNavigation: false,
    // use arrow keys
    className: "SectionsContainer",
    // the class name for the section container
    delay: 1000,
    // the scroll animation speed
    navigation: true,
    // use dots navigatio
    scrollBar: false,
    // use the browser default scrollbar
    sectionClassName: "Section",
    // the section class name
    sectionPaddingTop: "0",
    // the section top padding
    sectionPaddingBottom: "0",
    // the section bottom padding
    verticalAlign: true // align the content of each section vertical

  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_meta_tags__WEBPACK_IMPORTED_MODULE_1___default.a, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", null, "Flone | Fashion Home"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: "Fashion home of flone react minimalist eCommerce template."
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "fullpage-slider-wrapper"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_fullpage__WEBPACK_IMPORTED_MODULE_4__["Header"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_header_HeaderOne__WEBPACK_IMPORTED_MODULE_5__["default"], {
    layout: "container-fluid",
    headerPaddingClass: "header-padding-1",
    headerBgClass: "bg-white"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_fullpage__WEBPACK_IMPORTED_MODULE_4__["SectionsContainer"], _extends({}, options, {
    className: "bg-purple-2"
  }), _data_hero_sliders_scroll_slider_json__WEBPACK_IMPORTED_MODULE_3__ && _data_hero_sliders_scroll_slider_json__WEBPACK_IMPORTED_MODULE_3__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_fullpage__WEBPACK_IMPORTED_MODULE_4__["Section"], {
      key: key
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "slider-section flone-fp-section"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "container"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row fullpage-slider-wrap-mrg"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-6 col-md-6 col-sm-6 col-12 d-flex align-items-center"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "slider-content-11 slider-animated-1 fullpage-slider-mrg fullpage-content"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      className: "animated"
    }, single.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
      className: "animated",
      dangerouslySetInnerHTML: {
        __html: single.subtitle
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "slider-btn-11 btn-hover"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
      className: "animated",
      to: process.env.PUBLIC_URL + single.url
    }, "SHOP NOW")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-6 col-md-6 col-sm-6 col-12"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "slider12-img-1 slider-animated-1"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "animated",
      alt: "",
      src: process.env.PUBLIC_URL + single.image
    })))))));
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (HomeOnepageScroll);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ })

}]);
//# sourceMappingURL=70.bundle.js.map