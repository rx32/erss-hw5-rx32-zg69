(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[44],{

/***/ "./src/main/js/src/components/banner/BannerTenSingle.js":
/*!**************************************************************!*\
  !*** ./src/main/js/src/components/banner/BannerTenSingle.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");



var BannerTenSingle = function BannerTenSingle(_ref) {
  var data = _ref.data,
      spaceBottomClass = _ref.spaceBottomClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-xl-3 col-md-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "single-banner ".concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + data.link
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + data.image,
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "banner-content"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", null, data.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", null, data.subtitle, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, data.price)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + data.link
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-long-arrow-right"
  })))));
};

/* harmony default export */ __webpack_exports__["default"] = (BannerTenSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/header/Logo.js":
/*!***************************************************!*\
  !*** ./src/main/js/src/components/header/Logo.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");




var Logo = function Logo(_ref) {
  var imageUrl = _ref.imageUrl,
      logoClass = _ref.logoClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "".concat(logoClass ? logoClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    to: process.env.PUBLIC_URL + "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    alt: "",
    src: process.env.PUBLIC_URL + imageUrl
  })));
};

Logo.propTypes = {
  imageUrl: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  logoClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (Logo);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/hero-slider/HeroSliderThirteenSingle.js":
/*!****************************************************************************!*\
  !*** ./src/main/js/src/components/hero-slider/HeroSliderThirteenSingle.js ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");




var HeroSliderThirteenSingle = function HeroSliderThirteenSingle(_ref) {
  var data = _ref.data,
      sliderClassName = _ref.sliderClassName;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "single-slider-3 slider-height-3 bg-gray-2 d-flex align-items-center slider-height-res-hm4 ".concat(sliderClassName ? sliderClassName : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row align-items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-xl-7 col-lg-7 col-md-6 col-12 col-sm-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-content-3 slider-content-4 slider-animated-1 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", {
    className: "animated"
  }, data.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "animated"
  }, data.subtitle), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "animated"
  }, data.text), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-btn btn-hover"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    className: "animated",
    to: process.env.PUBLIC_URL + data.url
  }, "SHOP NOW")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-xl-5 col-lg-5 col-md-6 col-12 col-sm-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "single-slider-img4 slider-animated-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    className: "animated img-fluid",
    src: process.env.PUBLIC_URL + data.image,
    alt: ""
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "single-price-wrap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + data.shapeImage,
    alt: ""
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "single-price"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, data.shapeTitle), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "dollar"
  }, data.shapeSubtitle))))))));
};

HeroSliderThirteenSingle.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  sliderClassName: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (HeroSliderThirteenSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/newsletter/SubscribeEmail.js":
/*!*****************************************************************!*\
  !*** ./src/main/js/src/components/newsletter/SubscribeEmail.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_mailchimp_subscribe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-mailchimp-subscribe */ "./node_modules/react-mailchimp-subscribe/es/index.js");




var CustomForm = function CustomForm(_ref) {
  var status = _ref.status,
      message = _ref.message,
      onValidated = _ref.onValidated;
  var email;

  var submit = function submit() {
    email && email.value.indexOf("@") > -1 && onValidated({
      EMAIL: email.value
    });
    email.value = "";
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "subscribe-form-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mc-form"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    className: "email",
    ref: function ref(node) {
      return email = node;
    },
    type: "email",
    placeholder: "Youe Email Addres",
    required: true
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "clear-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "button",
    onClick: submit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-long-arrow-right"
  })))), status === "sending" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      color: "#3498db",
      fontSize: "12px"
    }
  }, "sending..."), status === "error" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      color: "#e74c3c",
      fontSize: "12px"
    },
    dangerouslySetInnerHTML: {
      __html: message
    }
  }), status === "success" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      color: "#2ecc71",
      fontSize: "12px"
    },
    dangerouslySetInnerHTML: {
      __html: message
    }
  }));
};

var SubscribeEmail = function SubscribeEmail(_ref2) {
  var mailchimpUrl = _ref2.mailchimpUrl;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_mailchimp_subscribe__WEBPACK_IMPORTED_MODULE_2__["default"], {
    url: mailchimpUrl,
    render: function render(_ref3) {
      var subscribe = _ref3.subscribe,
          status = _ref3.status,
          message = _ref3.message;
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(CustomForm, {
        status: status,
        message: message,
        onValidated: function onValidated(formData) {
          return subscribe(formData);
        }
      });
    }
  }));
};

SubscribeEmail.propTypes = {
  mailchimpUrl: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (SubscribeEmail);

/***/ }),

/***/ "./src/main/js/src/components/section-title/SectionTitle.js":
/*!******************************************************************!*\
  !*** ./src/main/js/src/components/section-title/SectionTitle.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



var SectionTitle = function SectionTitle(_ref) {
  var titleText = _ref.titleText,
      subtitleText = _ref.subtitleText,
      positionClass = _ref.positionClass,
      spaceClass = _ref.spaceClass,
      borderClass = _ref.borderClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "section-title ".concat(positionClass ? positionClass : "", " ").concat(spaceClass ? spaceClass : "", " ").concat(borderClass ? borderClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", null, titleText), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, subtitleText));
};

SectionTitle.propTypes = {
  borderClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  positionClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  subtitleText: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  titleText: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (SectionTitle);

/***/ }),

/***/ "./src/main/js/src/data/banner/banner-ten.json":
/*!*****************************************************!*\
  !*** ./src/main/js/src/data/banner/banner-ten.json ***!
  \*****************************************************/
/*! exports provided: 0, 1, 2, 3, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"image\":\"/assets/img/banner/banner-4.png\",\"title\":\"Watches\",\"subtitle\":\"Starting at\",\"price\":\"$99.00\",\"link\":\"/shop-grid-standard\"},{\"id\":2,\"image\":\"/assets/img/banner/banner-5.png\",\"title\":\"Plo Bag\",\"subtitle\":\"Starting at\",\"price\":\"$79.00\",\"link\":\"/shop-grid-standard\"},{\"id\":3,\"image\":\"/assets/img/banner/banner-6.png\",\"title\":\"Sunglass\",\"subtitle\":\"Starting at\",\"price\":\"$29.00\",\"link\":\"/shop-grid-standard\"},{\"id\":4,\"image\":\"/assets/img/banner/banner-7.png\",\"title\":\"Sun Hat\",\"subtitle\":\"Starting at\",\"price\":\"$12.00\",\"link\":\"/shop-grid-standard\"}]");

/***/ }),

/***/ "./src/main/js/src/data/hero-sliders/hero-slider-thirteen.json":
/*!*********************************************************************!*\
  !*** ./src/main/js/src/data/hero-sliders/hero-slider-thirteen.json ***!
  \*********************************************************************/
/*! exports provided: 0, 1, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"title\":\"Stylish\",\"subtitle\":\"Male Clothes\",\"text\":\"30% off Summer Vacation\",\"image\":\"/assets/img/slider/single-slide-2.png\",\"url\":\"/shop-grid-standard\",\"shapeImage\":\"/assets/img/icon-img/slider-shape.png\",\"shapeTitle\":\"Sale\",\"shapeSubtitle\":\"$99.00\"},{\"id\":2,\"title\":\"Stylish\",\"subtitle\":\"Female Clothes\",\"text\":\"20% off Summer Vacation\",\"image\":\"/assets/img/slider/single-slide-7.png\",\"url\":\"/shop-grid-standard\",\"shapeImage\":\"/assets/img/icon-img/slider-shape.png\",\"shapeTitle\":\"Sale\",\"shapeSubtitle\":\"$20.00\"}]");

/***/ }),

/***/ "./src/main/js/src/layouts/LayoutSix.js":
/*!**********************************************!*\
  !*** ./src/main/js/src/layouts/LayoutSix.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wrappers_header_HeaderFive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../wrappers/header/HeaderFive */ "./src/main/js/src/wrappers/header/HeaderFive.js");
/* harmony import */ var _wrappers_footer_FooterOne__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../wrappers/footer/FooterOne */ "./src/main/js/src/wrappers/footer/FooterOne.js");





var LayoutSix = function LayoutSix(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_header_HeaderFive__WEBPACK_IMPORTED_MODULE_2__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "home-sidebar-right"
  }, children, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_footer_FooterOne__WEBPACK_IMPORTED_MODULE_3__["default"], {
    backgroundColorClass: "bg-gray",
    spaceTopClass: "pt-100",
    spaceBottomClass: "pb-70",
    containerClass: "container-fluid",
    extraFooterClass: "hm4-footer-padding",
    sideMenu: true
  })));
};

LayoutSix.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.any
};
/* harmony default export */ __webpack_exports__["default"] = (LayoutSix);

/***/ }),

/***/ "./src/main/js/src/pages/home/HomeFashionFive.js":
/*!*******************************************************!*\
  !*** ./src/main/js/src/pages/home/HomeFashionFive.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-meta-tags */ "./node_modules/react-meta-tags/lib/index.js");
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_meta_tags__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_LayoutSix__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../layouts/LayoutSix */ "./src/main/js/src/layouts/LayoutSix.js");
/* harmony import */ var _wrappers_product_TabProductSeven__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../wrappers/product/TabProductSeven */ "./src/main/js/src/wrappers/product/TabProductSeven.js");
/* harmony import */ var _wrappers_newsletter_Newsletter__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../wrappers/newsletter/Newsletter */ "./src/main/js/src/wrappers/newsletter/Newsletter.js");
/* harmony import */ var _wrappers_banner_BannerTen__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../wrappers/banner/BannerTen */ "./src/main/js/src/wrappers/banner/BannerTen.js");
/* harmony import */ var _wrappers_hero_slider_HeroSliderThirteen__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../wrappers/hero-slider/HeroSliderThirteen */ "./src/main/js/src/wrappers/hero-slider/HeroSliderThirteen.js");








var HomeFashionFive = function HomeFashionFive() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_meta_tags__WEBPACK_IMPORTED_MODULE_1___default.a, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", null, "Flone | Fashion Home"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: "Fashion home of flone react minimalist eCommerce template."
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_LayoutSix__WEBPACK_IMPORTED_MODULE_2__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_hero_slider_HeroSliderThirteen__WEBPACK_IMPORTED_MODULE_6__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_banner_BannerTen__WEBPACK_IMPORTED_MODULE_5__["default"], {
    spaceTopClass: "pt-10",
    spaceBottomClass: "pb-85"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_product_TabProductSeven__WEBPACK_IMPORTED_MODULE_3__["default"], {
    spaceBottomClass: "pb-100",
    category: "accessories",
    containerClass: "container-fluid",
    extraClass: "hm4-section-padding"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_newsletter_Newsletter__WEBPACK_IMPORTED_MODULE_4__["default"], {
    spaceBottomClass: "pb-100",
    spaceLeftClass: "pl-30",
    spaceRightClass: "pr-30"
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (HomeFashionFive);

/***/ }),

/***/ "./src/main/js/src/wrappers/banner/BannerTen.js":
/*!******************************************************!*\
  !*** ./src/main/js/src/wrappers/banner/BannerTen.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _data_banner_banner_ten_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../data/banner/banner-ten.json */ "./src/main/js/src/data/banner/banner-ten.json");
var _data_banner_banner_ten_json__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/banner/banner-ten.json */ "./src/main/js/src/data/banner/banner-ten.json", 1);
/* harmony import */ var _components_banner_BannerTenSingle_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/banner/BannerTenSingle.js */ "./src/main/js/src/components/banner/BannerTenSingle.js");




var BannerTen = function BannerTen(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "banner-area banner-area-2 ".concat(spaceTopClass ? spaceTopClass : "", " ").concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container-fluid"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "custom-row-2"
  }, _data_banner_banner_ten_json__WEBPACK_IMPORTED_MODULE_1__ && _data_banner_banner_ten_json__WEBPACK_IMPORTED_MODULE_1__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_banner_BannerTenSingle_js__WEBPACK_IMPORTED_MODULE_2__["default"], {
      spaceBottomClass: "mb-10",
      data: single,
      key: key
    });
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (BannerTen);

/***/ }),

/***/ "./src/main/js/src/wrappers/header/HeaderFive.js":
/*!*******************************************************!*\
  !*** ./src/main/js/src/wrappers/header/HeaderFive.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_header_Logo__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/header/Logo */ "./src/main/js/src/components/header/Logo.js");
/* harmony import */ var _components_header_IconGroup__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/header/IconGroup */ "./src/main/js/src/components/header/IconGroup.js");
/* harmony import */ var _components_header_MobileMenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/header/MobileMenu */ "./src/main/js/src/components/header/MobileMenu.js");
/* harmony import */ var _components_header_NavMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/header/NavMenu */ "./src/main/js/src/components/header/NavMenu.js");






var HeaderFive = function HeaderFive() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "home-sidebar-left"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header_Logo__WEBPACK_IMPORTED_MODULE_1__["default"], {
    imageUrl: "/assets/img/logo/logo.png",
    logoClass: "logo"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header_IconGroup__WEBPACK_IMPORTED_MODULE_2__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header_NavMenu__WEBPACK_IMPORTED_MODULE_4__["default"], {
    sidebarMenu: true
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sidebar-copyright"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "\xA92020", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "//www.hasthemes.com",
    target: "_blank",
    rel: "noopener noreferrer"
  }, "Flone"), ".All Rights Reserved."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("header", {
    className: "header-area header-padding-1 sticky-bar header-res-padding clearfix header-hm4-none"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container-fluid"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-xl-2 col-lg-2 col-md-6 col-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header_Logo__WEBPACK_IMPORTED_MODULE_1__["default"], {
    imageUrl: "/assets/img/logo/logo.png",
    logoClass: "logo"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-xl-10 col-lg-102 col-md-6 col-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header_IconGroup__WEBPACK_IMPORTED_MODULE_2__["default"], null))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header_MobileMenu__WEBPACK_IMPORTED_MODULE_3__["default"], null))));
};

/* harmony default export */ __webpack_exports__["default"] = (HeaderFive);

/***/ }),

/***/ "./src/main/js/src/wrappers/hero-slider/HeroSliderThirteen.js":
/*!********************************************************************!*\
  !*** ./src/main/js/src/wrappers/hero-slider/HeroSliderThirteen.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-id-swiper */ "./node_modules/react-id-swiper/lib/index.js");
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_id_swiper__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _data_hero_sliders_hero_slider_thirteen_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../data/hero-sliders/hero-slider-thirteen.json */ "./src/main/js/src/data/hero-sliders/hero-slider-thirteen.json");
var _data_hero_sliders_hero_slider_thirteen_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/hero-sliders/hero-slider-thirteen.json */ "./src/main/js/src/data/hero-sliders/hero-slider-thirteen.json", 1);
/* harmony import */ var _components_hero_slider_HeroSliderThirteenSingle_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/hero-slider/HeroSliderThirteenSingle.js */ "./src/main/js/src/components/hero-slider/HeroSliderThirteenSingle.js");





var HeroSliderThirteen = function HeroSliderThirteen() {
  var params = {
    effect: "fade",
    loop: true,
    speed: 1000,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    watchSlidesVisibility: true,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    renderPrevButton: function renderPrevButton() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "swiper-button-prev ht-swiper-button-nav"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "pe-7s-angle-left"
      }));
    },
    renderNextButton: function renderNextButton() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "swiper-button-next ht-swiper-button-nav"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "pe-7s-angle-right"
      }));
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "slider-area"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "slider-active nav-style-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_id_swiper__WEBPACK_IMPORTED_MODULE_1___default.a, params, _data_hero_sliders_hero_slider_thirteen_json__WEBPACK_IMPORTED_MODULE_2__ && _data_hero_sliders_hero_slider_thirteen_json__WEBPACK_IMPORTED_MODULE_2__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_hero_slider_HeroSliderThirteenSingle_js__WEBPACK_IMPORTED_MODULE_3__["default"], {
      sliderClassName: "swiper-slide",
      data: single,
      key: key
    });
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (HeroSliderThirteen);

/***/ }),

/***/ "./src/main/js/src/wrappers/newsletter/Newsletter.js":
/*!***********************************************************!*\
  !*** ./src/main/js/src/wrappers/newsletter/Newsletter.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_newsletter_SubscribeEmail__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/newsletter/SubscribeEmail */ "./src/main/js/src/components/newsletter/SubscribeEmail.js");




var Newsletter = function Newsletter(_ref) {
  var bgColorClass = _ref.bgColorClass,
      spaceBottomClass = _ref.spaceBottomClass,
      spaceLeftClass = _ref.spaceLeftClass,
      spaceRightClass = _ref.spaceRightClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "subscribe-area ".concat(bgColorClass ? bgColorClass : "", " ").concat(spaceBottomClass ? spaceBottomClass : "", " ").concat(spaceLeftClass ? spaceLeftClass : "", "  ").concat(spaceRightClass ? spaceRightClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-xl-8 col-lg-8 ml-auto mr-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "subscribe-style-2 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", null, "Subscribe "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Subscribe to our newsletter to receive news on update"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_newsletter_SubscribeEmail__WEBPACK_IMPORTED_MODULE_2__["default"], {
    mailchimpUrl: "//devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&id=05d85f18ef"
  }))))));
};

Newsletter.propTypes = {
  bgColorClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceLeftClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceRightClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (Newsletter);

/***/ }),

/***/ "./src/main/js/src/wrappers/product/TabProductSeven.js":
/*!*************************************************************!*\
  !*** ./src/main/js/src/wrappers/product/TabProductSeven.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/Tab */ "./node_modules/react-bootstrap/esm/Tab.js");
/* harmony import */ var react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap/Nav */ "./node_modules/react-bootstrap/esm/Nav.js");
/* harmony import */ var _components_section_title_SectionTitle__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/section-title/SectionTitle */ "./src/main/js/src/components/section-title/SectionTitle.js");
/* harmony import */ var _ProductGridTwo__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ProductGridTwo */ "./src/main/js/src/wrappers/product/ProductGridTwo.js");








var TabProductSeven = function TabProductSeven(_ref) {
  var spaceBottomClass = _ref.spaceBottomClass,
      category = _ref.category,
      containerClass = _ref.containerClass,
      extraClass = _ref.extraClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "product-area ".concat(spaceBottomClass ? spaceBottomClass : "", " ").concat(extraClass ? extraClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "".concat(containerClass ? containerClass : "container")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_section_title_SectionTitle__WEBPACK_IMPORTED_MODULE_5__["default"], {
    titleText: "DAILY DEALS!",
    positionClass: "text-center"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Container, {
    defaultActiveKey: "bestSeller"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"], {
    variant: "pills",
    className: "product-tab-list pt-30 pb-55 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Link, {
    eventKey: "newArrival"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, "New Arrivals"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Link, {
    eventKey: "bestSeller"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, "Best Sellers"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Link, {
    eventKey: "saleItems"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, "Sale Items")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Content, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
    eventKey: "newArrival"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductGridTwo__WEBPACK_IMPORTED_MODULE_6__["default"], {
    category: category,
    type: "new",
    limit: 8,
    spaceBottomClass: "mb-25"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
    eventKey: "bestSeller"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductGridTwo__WEBPACK_IMPORTED_MODULE_6__["default"], {
    category: category,
    type: "bestSeller",
    limit: 8,
    spaceBottomClass: "mb-25"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
    eventKey: "saleItems"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductGridTwo__WEBPACK_IMPORTED_MODULE_6__["default"], {
    category: category,
    type: "saleItems",
    limit: 8,
    spaceBottomClass: "mb-25"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "view-more text-center mt-20 toggle-btn6 col-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    className: "loadMore6",
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, "VIEW MORE PRODUCTS"))));
};

TabProductSeven.propTypes = {
  category: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  containerClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  extraClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (TabProductSeven);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ })

}]);
//# sourceMappingURL=44.bundle.js.map