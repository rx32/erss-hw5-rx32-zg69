(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[51],{

/***/ "./src/main/js/src/components/hero-slider/HeroSliderTwelveSingle.js":
/*!**************************************************************************!*\
  !*** ./src/main/js/src/components/hero-slider/HeroSliderTwelveSingle.js ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");




var HeroSliderTwelveSingle = function HeroSliderTwelveSingle(_ref) {
  var data = _ref.data,
      sliderClass = _ref.sliderClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-height-4 d-flex align-items-center bg-img ".concat(sliderClass ? sliderClass : ""),
    style: {
      backgroundImage: "url(".concat(process.env.PUBLIC_URL + data.image, ")")
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-xl-12 col-lg-12 col-md-12 col-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-content-5 slider-animated-1 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", {
    className: "animated"
  }, data.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "animated"
  }, data.subtitle), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "animated"
  }, data.text), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-btn-5 btn-hover"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    className: "animated",
    to: process.env.PUBLIC_URL + data.url
  }, "SHOP NOW")))))));
};

HeroSliderTwelveSingle.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  sliderClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (HeroSliderTwelveSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/product/ProductGridSingleFive.js":
/*!*********************************************************************!*\
  !*** ./src/main/js/src/components/product/ProductGridSingleFive.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var react_toast_notifications__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-toast-notifications */ "./node_modules/react-toast-notifications/dist/index.js");
/* harmony import */ var react_toast_notifications__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_toast_notifications__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _helpers_product__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../helpers/product */ "./src/main/js/src/helpers/product.js");
/* harmony import */ var _ProductModal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ProductModal */ "./src/main/js/src/components/product/ProductModal.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(n); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }








var ProductGridSingleFive = function ProductGridSingleFive(_ref) {
  var product = _ref.product,
      currency = _ref.currency,
      addToCart = _ref.addToCart,
      addToWishlist = _ref.addToWishlist,
      addToCompare = _ref.addToCompare,
      cartItem = _ref.cartItem,
      wishlistItem = _ref.wishlistItem,
      compareItem = _ref.compareItem,
      sliderClassName = _ref.sliderClassName,
      spaceBottomClass = _ref.spaceBottomClass;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      modalShow = _useState2[0],
      setModalShow = _useState2[1];

  var _useToasts = Object(react_toast_notifications__WEBPACK_IMPORTED_MODULE_3__["useToasts"])(),
      addToast = _useToasts.addToast;

  var discountedPrice = Object(_helpers_product__WEBPACK_IMPORTED_MODULE_4__["getDiscountPrice"])(product.price, product.discount);
  var finalProductPrice = +(product.price * currency.currencyRate).toFixed(2);
  var finalDiscountedPrice = +(discountedPrice * currency.currencyRate).toFixed(2);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 ".concat(sliderClassName ? sliderClassName : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "product-wrap-3 scroll-zoom ".concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "product-img"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    to: process.env.PUBLIC_URL + "/product/" + product.id
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    className: "default-img",
    src: process.env.PUBLIC_URL + product.image[0],
    alt: ""
  })), product.discount || product["new"] ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "product-img-badges"
  }, product.discount ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "pink"
  }, "-", product.discount, "%") : "", product["new"] ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "purple"
  }, "New") : "") : "", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "product-content-3-wrap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "product-content-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "product-title"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    to: process.env.PUBLIC_URL + "/product/" + product.id
  }, product.name))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "price-3"
  }, discountedPrice !== null ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, currency.currencySymbol + finalDiscountedPrice), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "old"
  }, currency.currencySymbol + finalProductPrice)) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, currency.currencySymbol + finalProductPrice, " ")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "product-action-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "pro-same-action pro-wishlist"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: wishlistItem !== undefined ? "active" : "",
    disabled: wishlistItem !== undefined,
    title: wishlistItem !== undefined ? "Added to wishlist" : "Add to wishlist",
    onClick: function onClick() {
      return addToWishlist(product, addToast);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-heart-o"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "pro-same-action pro-cart"
  }, product.affiliateLink ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: product.affiliateLink,
    rel: "noopener noreferrer",
    target: "_blank",
    title: "Buy now"
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-shopping-cart"
  }), " ") : product.variation && product.variation.length >= 1 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    to: "".concat(process.env.PUBLIC_URL, "/product/").concat(product.id),
    title: "Select options"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    "class": "fa fa-cog"
  })) : product.stock && product.stock > 0 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    onClick: function onClick() {
      return addToCart(product, addToast);
    },
    className: cartItem !== undefined && cartItem.quantity > 0 ? "active" : "",
    disabled: cartItem !== undefined && cartItem.quantity > 0,
    title: cartItem !== undefined ? "Added to cart" : "Add to cart"
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-shopping-cart"
  }), " ") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    disabled: true,
    className: "active",
    title: "Out of stock"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-shopping-cart"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "pro-same-action pro-compare"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: compareItem !== undefined ? "active" : "",
    disabled: compareItem !== undefined,
    title: compareItem !== undefined ? "Added to compare" : "Add to compare",
    onClick: function onClick() {
      return addToCompare(product, addToast);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-retweet"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "pro-same-action pro-quickview"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    onClick: function onClick() {
      return setModalShow(true);
    },
    title: "Quick View"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-eye"
  }))))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductModal__WEBPACK_IMPORTED_MODULE_5__["default"], {
    show: modalShow,
    onHide: function onHide() {
      return setModalShow(false);
    },
    product: product,
    currency: currency,
    discountedprice: discountedPrice,
    finalproductprice: finalProductPrice,
    finaldiscountedprice: finalDiscountedPrice,
    cartitem: cartItem,
    wishlistitem: wishlistItem,
    compareitem: compareItem,
    addtocart: addToCart,
    addtowishlist: addToWishlist,
    addtocompare: addToCompare,
    addtoast: addToast
  }));
};

ProductGridSingleFive.propTypes = {
  addToCart: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func,
  addToCompare: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func,
  addToWishlist: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func,
  cartItem: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  compareItem: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  currency: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  product: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  sliderClassName: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  wishlistItem: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object
};
/* harmony default export */ __webpack_exports__["default"] = (ProductGridSingleFive);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/data/hero-sliders/hero-slider-twelve.json":
/*!*******************************************************************!*\
  !*** ./src/main/js/src/data/hero-sliders/hero-slider-twelve.json ***!
  \*******************************************************************/
/*! exports provided: 0, 1, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"title\":\"New Arrival\",\"subtitle\":\"Final Sale\",\"text\":\"30% off Summer Vacation\",\"image\":\"/assets/img/slider/slider-3.jpg\",\"url\":\"/shop-grid-standard\"},{\"id\":2,\"title\":\"New Arrival\",\"subtitle\":\"Final Sale\",\"text\":\"40% off Summer Vacation\",\"image\":\"/assets/img/slider/slider-4.jpg\",\"url\":\"/shop-grid-standard\"}]");

/***/ }),

/***/ "./src/main/js/src/layouts/LayoutFive.js":
/*!***********************************************!*\
  !*** ./src/main/js/src/layouts/LayoutFive.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wrappers_header_HeaderFour__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../wrappers/header/HeaderFour */ "./src/main/js/src/wrappers/header/HeaderFour.js");
/* harmony import */ var _wrappers_footer_FooterTwo__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../wrappers/footer/FooterTwo */ "./src/main/js/src/wrappers/footer/FooterTwo.js");





var LayoutFive = function LayoutFive(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "wrapper"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_header_HeaderFour__WEBPACK_IMPORTED_MODULE_2__["default"], null), children, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_footer_FooterTwo__WEBPACK_IMPORTED_MODULE_3__["default"], {
    backgroundColorClass: "bg-black",
    footerTopBackgroundColorClass: "bg-black",
    footerTopSpaceTopClass: "pt-80",
    spaceBottomClass: "pb-25",
    footerLogo: "/assets/img/logo/logo-2.png"
  }));
};

LayoutFive.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.any
};
/* harmony default export */ __webpack_exports__["default"] = (LayoutFive);

/***/ }),

/***/ "./src/main/js/src/pages/home/HomeFashionFour.js":
/*!*******************************************************!*\
  !*** ./src/main/js/src/pages/home/HomeFashionFour.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-meta-tags */ "./node_modules/react-meta-tags/lib/index.js");
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_meta_tags__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_LayoutFive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../layouts/LayoutFive */ "./src/main/js/src/layouts/LayoutFive.js");
/* harmony import */ var _wrappers_hero_slider_HeroSliderTwelve__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../wrappers/hero-slider/HeroSliderTwelve */ "./src/main/js/src/wrappers/hero-slider/HeroSliderTwelve.js");
/* harmony import */ var _wrappers_newsletter_NewsletterTwo__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../wrappers/newsletter/NewsletterTwo */ "./src/main/js/src/wrappers/newsletter/NewsletterTwo.js");
/* harmony import */ var _wrappers_product_ProductGridFiveContainer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../wrappers/product/ProductGridFiveContainer */ "./src/main/js/src/wrappers/product/ProductGridFiveContainer.js");







var HomeFashionFour = function HomeFashionFour() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_meta_tags__WEBPACK_IMPORTED_MODULE_1___default.a, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", null, "Flone | Fashion Home"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: "Fashion home of flone react minimalist eCommerce template."
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_LayoutFive__WEBPACK_IMPORTED_MODULE_2__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_hero_slider_HeroSliderTwelve__WEBPACK_IMPORTED_MODULE_3__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_product_ProductGridFiveContainer__WEBPACK_IMPORTED_MODULE_5__["default"], {
    spaceTopClass: "pt-100",
    spaceBottomClass: "pb-100",
    category: "accessories"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_newsletter_NewsletterTwo__WEBPACK_IMPORTED_MODULE_4__["default"], {
    spaceBottomClass: "pb-100"
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (HomeFashionFour);

/***/ }),

/***/ "./src/main/js/src/wrappers/header/HeaderFour.js":
/*!*******************************************************!*\
  !*** ./src/main/js/src/wrappers/header/HeaderFour.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _components_header_NavMenu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/header/NavMenu */ "./src/main/js/src/components/header/NavMenu.js");
/* harmony import */ var _components_header_IconGroup__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/header/IconGroup */ "./src/main/js/src/components/header/IconGroup.js");
/* harmony import */ var _components_header_MobileMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/header/MobileMenu */ "./src/main/js/src/components/header/MobileMenu.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(n); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }







var HeaderFour = function HeaderFour() {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(0),
      _useState2 = _slicedToArray(_useState, 2),
      scroll = _useState2[0],
      setScroll = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(0),
      _useState4 = _slicedToArray(_useState3, 2),
      headerTop = _useState4[0],
      setHeaderTop = _useState4[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var header = document.querySelector(".sticky-bar");
    setHeaderTop(header.offsetTop);
    window.addEventListener("scroll", handleScroll);
    return function () {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  var handleScroll = function handleScroll() {
    setScroll(window.scrollY);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("header", {
    className: "header-area sticky-bar header-padding-3 header-res-padding clearfix transparent-bar ".concat(scroll > headerTop ? "stick" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container-fluid"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-xl-5 col-lg-6 d-none d-lg-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header_NavMenu__WEBPACK_IMPORTED_MODULE_2__["default"], {
    menuWhiteClass: "menu-white"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-xl-2 col-lg-2 col-md-6 col-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "logo text-center logo-hm5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    className: "sticky-none",
    to: process.env.PUBLIC_URL + "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    alt: "",
    src: "assets/img/logo/logo-2.png"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    className: "sticky-block",
    to: process.env.PUBLIC_URL + "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    alt: "",
    src: "assets/img/logo/logo.png"
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-xl-5 col-lg-4 col-md-6 col-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header_IconGroup__WEBPACK_IMPORTED_MODULE_3__["default"], {
    iconWhiteClass: "header-right-wrap-white"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header_MobileMenu__WEBPACK_IMPORTED_MODULE_4__["default"], null)));
};

/* harmony default export */ __webpack_exports__["default"] = (HeaderFour);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/wrappers/hero-slider/HeroSliderTwelve.js":
/*!******************************************************************!*\
  !*** ./src/main/js/src/wrappers/hero-slider/HeroSliderTwelve.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-id-swiper */ "./node_modules/react-id-swiper/lib/index.js");
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_id_swiper__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _data_hero_sliders_hero_slider_twelve_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../data/hero-sliders/hero-slider-twelve.json */ "./src/main/js/src/data/hero-sliders/hero-slider-twelve.json");
var _data_hero_sliders_hero_slider_twelve_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/hero-sliders/hero-slider-twelve.json */ "./src/main/js/src/data/hero-sliders/hero-slider-twelve.json", 1);
/* harmony import */ var _components_hero_slider_HeroSliderTwelveSingle_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/hero-slider/HeroSliderTwelveSingle.js */ "./src/main/js/src/components/hero-slider/HeroSliderTwelveSingle.js");





var HeroSliderTwelve = function HeroSliderTwelve() {
  var params = {
    effect: "fade",
    loop: true,
    speed: 1000,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    watchSlidesVisibility: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    renderPrevButton: function renderPrevButton() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "swiper-button-prev ht-swiper-button-nav"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "pe-7s-angle-left"
      }));
    },
    renderNextButton: function renderNextButton() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "swiper-button-next ht-swiper-button-nav"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "pe-7s-angle-right"
      }));
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "slider-area"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "slider-active-2 nav-style-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_id_swiper__WEBPACK_IMPORTED_MODULE_1___default.a, params, _data_hero_sliders_hero_slider_twelve_json__WEBPACK_IMPORTED_MODULE_2__ && _data_hero_sliders_hero_slider_twelve_json__WEBPACK_IMPORTED_MODULE_2__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_hero_slider_HeroSliderTwelveSingle_js__WEBPACK_IMPORTED_MODULE_3__["default"], {
      data: single,
      key: key,
      sliderClass: "swiper-slide"
    });
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (HeroSliderTwelve);

/***/ }),

/***/ "./src/main/js/src/wrappers/product/ProductGridFive.js":
/*!*************************************************************!*\
  !*** ./src/main/js/src/wrappers/product/ProductGridFive.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _helpers_product__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../helpers/product */ "./src/main/js/src/helpers/product.js");
/* harmony import */ var _components_product_ProductGridSingleFive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/product/ProductGridSingleFive */ "./src/main/js/src/components/product/ProductGridSingleFive.js");
/* harmony import */ var _redux_actions_cartActions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../redux/actions/cartActions */ "./src/main/js/src/redux/actions/cartActions.js");
/* harmony import */ var _redux_actions_wishlistActions__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../redux/actions/wishlistActions */ "./src/main/js/src/redux/actions/wishlistActions.js");
/* harmony import */ var _redux_actions_compareActions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../redux/actions/compareActions */ "./src/main/js/src/redux/actions/compareActions.js");









var ProductGridFive = function ProductGridFive(_ref) {
  var products = _ref.products,
      currency = _ref.currency,
      addToCart = _ref.addToCart,
      addToWishlist = _ref.addToWishlist,
      addToCompare = _ref.addToCompare,
      cartItems = _ref.cartItems,
      wishlistItems = _ref.wishlistItems,
      compareItems = _ref.compareItems,
      sliderClassName = _ref.sliderClassName,
      spaceBottomClass = _ref.spaceBottomClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, products.map(function (product) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_product_ProductGridSingleFive__WEBPACK_IMPORTED_MODULE_4__["default"], {
      sliderClassName: sliderClassName,
      spaceBottomClass: spaceBottomClass,
      product: product,
      currency: currency,
      addToCart: addToCart,
      addToWishlist: addToWishlist,
      addToCompare: addToCompare,
      cartItem: cartItems.filter(function (cartItem) {
        return cartItem.id === product.id;
      })[0],
      wishlistItem: wishlistItems.filter(function (wishlistItem) {
        return wishlistItem.id === product.id;
      })[0],
      compareItem: compareItems.filter(function (compareItem) {
        return compareItem.id === product.id;
      })[0],
      key: product.id
    });
  }));
};

ProductGridFive.propTypes = {
  addToCart: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func,
  addToCompare: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func,
  addToWishlist: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func,
  cartItems: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.array,
  compareItems: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.array,
  currency: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  products: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.array,
  sliderClassName: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  wishlistItems: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.array
};

var mapStateToProps = function mapStateToProps(state, ownProps) {
  return {
    products: Object(_helpers_product__WEBPACK_IMPORTED_MODULE_3__["getProducts"])(state.productData.products, ownProps.category, ownProps.type, ownProps.limit),
    currency: state.currencyData,
    cartItems: state.cartData,
    wishlistItems: state.wishlistData,
    compareItems: state.compareData
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    addToCart: function addToCart(item, addToast, quantityCount, selectedProductColor, selectedProductSize) {
      dispatch(Object(_redux_actions_cartActions__WEBPACK_IMPORTED_MODULE_5__["addToCart"])(item, addToast, quantityCount, selectedProductColor, selectedProductSize));
    },
    addToWishlist: function addToWishlist(item, addToast) {
      dispatch(Object(_redux_actions_wishlistActions__WEBPACK_IMPORTED_MODULE_6__["addToWishlist"])(item, addToast));
    },
    addToCompare: function addToCompare(item, addToast) {
      dispatch(Object(_redux_actions_compareActions__WEBPACK_IMPORTED_MODULE_7__["addToCompare"])(item, addToast));
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps, mapDispatchToProps)(ProductGridFive));

/***/ }),

/***/ "./src/main/js/src/wrappers/product/ProductGridFiveContainer.js":
/*!**********************************************************************!*\
  !*** ./src/main/js/src/wrappers/product/ProductGridFiveContainer.js ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ProductGridFive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProductGridFive */ "./src/main/js/src/wrappers/product/ProductGridFive.js");




var ProductGridFiveContainer = function ProductGridFiveContainer(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass,
      category = _ref.category;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "product-area hm5-section-padding ".concat(spaceTopClass ? spaceTopClass : "", "  ").concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container-fluid"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductGridFive__WEBPACK_IMPORTED_MODULE_2__["default"], {
    category: category,
    limit: 12,
    spaceBottomClass: "mb-20"
  }))));
};

ProductGridFiveContainer.propTypes = {
  category: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (ProductGridFiveContainer);

/***/ })

}]);
//# sourceMappingURL=51.bundle.js.map