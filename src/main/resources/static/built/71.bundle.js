(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[71],{

/***/ "./src/main/js/src/pages/blog/BlogRightSidebar.js":
/*!********************************************************!*\
  !*** ./src/main/js/src/pages/blog/BlogRightSidebar.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-meta-tags */ "./node_modules/react-meta-tags/lib/index.js");
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_meta_tags__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_breadcrumbs_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-breadcrumbs-dynamic */ "./node_modules/react-breadcrumbs-dynamic/dist/src/index.js");
/* harmony import */ var react_breadcrumbs_dynamic__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_breadcrumbs_dynamic__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _layouts_LayoutOne__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../layouts/LayoutOne */ "./src/main/js/src/layouts/LayoutOne.js");
/* harmony import */ var _wrappers_breadcrumb_Breadcrumb__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../wrappers/breadcrumb/Breadcrumb */ "./src/main/js/src/wrappers/breadcrumb/Breadcrumb.js");
/* harmony import */ var _wrappers_blog_BlogSidebar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../wrappers/blog/BlogSidebar */ "./src/main/js/src/wrappers/blog/BlogSidebar.js");
/* harmony import */ var _wrappers_blog_BlogPagination__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../wrappers/blog/BlogPagination */ "./src/main/js/src/wrappers/blog/BlogPagination.js");
/* harmony import */ var _wrappers_blog_BlogPosts__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../wrappers/blog/BlogPosts */ "./src/main/js/src/wrappers/blog/BlogPosts.js");










var BlogRightSidebar = function BlogRightSidebar(_ref) {
  var location = _ref.location;
  var pathname = location.pathname;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_meta_tags__WEBPACK_IMPORTED_MODULE_2___default.a, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("title", null, "Flone | Blog"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "description",
    content: "Blog of flone react minimalist eCommerce template."
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_breadcrumbs_dynamic__WEBPACK_IMPORTED_MODULE_3__["BreadcrumbsItem"], {
    to: process.env.PUBLIC_URL + "/"
  }, "Home"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_breadcrumbs_dynamic__WEBPACK_IMPORTED_MODULE_3__["BreadcrumbsItem"], {
    to: process.env.PUBLIC_URL + pathname
  }, "Blog"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_layouts_LayoutOne__WEBPACK_IMPORTED_MODULE_4__["default"], {
    headerTop: "visible"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_breadcrumb_Breadcrumb__WEBPACK_IMPORTED_MODULE_5__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "blog-area pt-100 pb-100"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-lg-9"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mr-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_blog_BlogPosts__WEBPACK_IMPORTED_MODULE_8__["default"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_blog_BlogPagination__WEBPACK_IMPORTED_MODULE_7__["default"], null))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-lg-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_blog_BlogSidebar__WEBPACK_IMPORTED_MODULE_6__["default"], null)))))));
};

BlogRightSidebar.propTypes = {
  location: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object
};
/* harmony default export */ __webpack_exports__["default"] = (BlogRightSidebar);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ })

}]);
//# sourceMappingURL=71.bundle.js.map