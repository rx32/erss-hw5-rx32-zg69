(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[55],{

/***/ "./node_modules/react-sticky-el/lib/helpers/events.js":
/*!************************************************************!*\
  !*** ./node_modules/react-sticky-el/lib/helpers/events.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.listen = listen;
exports.unlisten = unlisten;
////////////////////////////////////////////////////////////////////////////////////////
// Small helpers that provide an easy and effecient way to add/remove event listeners //
////////////////////////////////////////////////////////////////////////////////////////
var elementsWithListeners = [],
    registeredListeners = [];

function addListener(el, event, cb) {
  var idx = elementsWithListeners.indexOf(el);

  if (idx === -1) {
    idx = elementsWithListeners.length;
    elementsWithListeners.push(el);
    registeredListeners.push({
      el: el,
      totalCount: 0
    });
  }

  var listeners = registeredListeners[idx],
      listener = listeners[event];

  if (!listener) {
    listener = listeners[event] = {
      callbacks: []
    };

    listener.cb = function (e) {
      for (var i = 0, l = listener.callbacks.length; i < l; i += 1) {
        listener.callbacks[i](e);
      }
    };

    listeners.totalCount += 1;
    listeners.el.addEventListener(event, listener.cb);
  } // just to prevent double listeners


  if (listener.callbacks.indexOf(cb) !== -1) {
    return;
  }

  listener.callbacks.push(cb);
}

function removeListener(el, event, cb) {
  var idx = elementsWithListeners.indexOf(el);

  if (idx === -1) {
    return;
  }

  var listeners = registeredListeners[idx],
      listener = listeners[event],
      callbacks = listener ? listener.callbacks : [];

  if (!listener || callbacks.indexOf(cb) === -1) {
    return;
  }

  callbacks.splice(callbacks.indexOf(cb), 1);

  if (callbacks.length > 0) {
    return;
  }

  listeners.el.removeEventListener(event, listener.cb);
  listeners.totalCount -= 1;
  delete listeners[event];

  if (listeners.totalCount > 0) {
    return;
  }

  elementsWithListeners.splice(idx, 1);
  registeredListeners.splice(idx, 1);
}
/**
 * Subscribe cb to events list
 * @param  {HTMLElement}   el       target element
 * @param  {Array}         events   array of event names
 * @param  {Function} cb   callback that should be called
 */


function listen(el, events, cb) {
  for (var i = 0, l = events.length; i < l; i += 1) {
    addListener(el, events[i], cb);
  }
}
/**
 * Unsubscribe cb from events list
 * @param  {HTMLElement}   el       target element
 * @param  {Array}         events   array of event names
 * @param  {Function} cb   callback that should be unsubscribed
 */


function unlisten(el, events, cb) {
  for (var i = 0, l = events.length; i < l; i += 1) {
    removeListener(el, events[i], cb);
  }
}

/***/ }),

/***/ "./node_modules/react-sticky-el/lib/helpers/find.js":
/*!**********************************************************!*\
  !*** ./node_modules/react-sticky-el/lib/helpers/find.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = find;
var basicSelectors = {};

if (typeof document !== 'undefined') {
  basicSelectors.body = document.body;
  basicSelectors.window = window;
  basicSelectors.document = document;
}

var matchesMethodName = function () {
  if (typeof document !== 'undefined') {
    var body = document.body;
    return typeof body.matches === 'function' ? 'matches' : typeof body.webkitMatchesSelector === 'function' ? 'webkitMatchesSelector' : //webkit
    typeof body.mozMatchesSelector === 'function' ? 'mozMatchesSelector' : //mozilla
    typeof body.msMatchesSelector === 'function' ? 'msMatchesSelector' : //ie
    typeof body.oMatchesSelector === 'function' ? 'oMatchesSelector' : //old opera
    null;
  }
}();

function find(selector, el) {
  if (!selector) {
    return null;
  }

  if (basicSelectors.hasOwnProperty(selector)) {
    return basicSelectors[selector];
  } // select by id


  if (selector[0] === '#') {
    return document.getElementById(selector.slice(1));
  }

  if (!matchesMethodName) {
    return null;
  } // eslint-disable-next-line no-cond-assign


  while (el = el.parentElement) {
    if (el[matchesMethodName](selector)) {
      return el;
    }
  } // nothing has been found :(


  return null;
}

module.exports = exports.default;

/***/ }),

/***/ "./node_modules/react-sticky-el/lib/helpers/getClosestTransformedParent.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/react-sticky-el/lib/helpers/getClosestTransformedParent.js ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getClosestTransformedParent;

function getClosestTransformedParent(el) {
  do {
    var style = window.getComputedStyle(el);
    if (style.transform !== 'none' || style.webkitTransform !== 'none') return el;
    el = el.parentElement || el.parentNode;
  } while (el !== null && el.nodeType === 1);

  return null;
}

module.exports = exports.default;

/***/ }),

/***/ "./node_modules/react-sticky-el/lib/index.js":
/*!***************************************************!*\
  !*** ./node_modules/react-sticky-el/lib/index.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _sticky = _interopRequireDefault(__webpack_require__(/*! ./sticky */ "./node_modules/react-sticky-el/lib/sticky.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = _sticky.default;
exports.default = _default;
module.exports = exports.default;

/***/ }),

/***/ "./node_modules/react-sticky-el/lib/sticky.js":
/*!****************************************************!*\
  !*** ./node_modules/react-sticky-el/lib/sticky.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var _propTypes = _interopRequireDefault(__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js"));

var _reactDom = _interopRequireDefault(__webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js"));

var _events = __webpack_require__(/*! ./helpers/events */ "./node_modules/react-sticky-el/lib/helpers/events.js");

var _find = _interopRequireDefault(__webpack_require__(/*! ./helpers/find */ "./node_modules/react-sticky-el/lib/helpers/find.js"));

var _getClosestTransformedParent = _interopRequireDefault(__webpack_require__(/*! ./helpers/getClosestTransformedParent */ "./node_modules/react-sticky-el/lib/helpers/getClosestTransformedParent.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var stickyOwnProps = ['mode', 'disabled', 'onFixedToggle', 'stickyStyle', 'stickyClassName', 'boundaryElement', 'scrollElement', 'bottomOffset', 'topOffset', 'positionRecheckInterval', 'noExceptionOnMissedScrollElement', 'wrapperCmp', 'holderCmp', 'hideOnBoundaryHit', 'offsetTransforms', 'holderProps'];

var isEqual = function isEqual(obj1, obj2) {
  var styles1 = obj1.styles;
  var styles2 = obj2.styles;

  if (obj1.fixed !== obj2.fixed || obj1.height !== obj2.height || !styles1 && styles2 || styles1 && !styles2) {
    return false;
  }

  for (var field in styles1) {
    if (styles1.hasOwnProperty(field) && styles1[field] !== styles2[field]) {
      return false;
    }
  }

  return true;
};

var buildTopStyles = function buildTopStyles(container, props) {
  var bottomOffset = props.bottomOffset,
      hideOnBoundaryHit = props.hideOnBoundaryHit;
  var top = container.top,
      height = container.height,
      width = container.width,
      boundaryBottom = container.boundaryBottom;

  if (hideOnBoundaryHit || top + height + bottomOffset < boundaryBottom) {
    return {
      top: top,
      width: width,
      position: 'fixed'
    };
  }

  return {
    width: width,
    bottom: bottomOffset,
    position: 'absolute'
  };
};

var buildBottomStyles = function buildBottomStyles(container, props) {
  var bottomOffset = props.bottomOffset,
      hideOnBoundaryHit = props.hideOnBoundaryHit;
  var bottom = container.bottom,
      height = container.height,
      width = container.width,
      boundaryTop = container.boundaryTop;

  if (hideOnBoundaryHit || bottom - height - bottomOffset > boundaryTop) {
    return {
      width: width,
      top: bottom - height,
      position: 'fixed'
    };
  }

  return {
    width: width,
    top: bottomOffset,
    position: 'absolute'
  };
};

var buildStickyStyle = function buildStickyStyle(mode, props, container) {
  return (mode === 'top' ? buildTopStyles : buildBottomStyles)(container, props);
};

var Sticky =
/*#__PURE__*/
function (_Component) {
  _inherits(Sticky, _Component);

  function Sticky(props) {
    var _this;

    _classCallCheck(this, Sticky);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Sticky).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "createWrapperRef", function (wrapper) {
      _this.wrapperEl = wrapper;
    });

    _defineProperty(_assertThisInitialized(_this), "createHolderRef", function (holder) {
      _this.holderEl = holder;
    });

    _defineProperty(_assertThisInitialized(_this), "checkPosition", function () {
      var _assertThisInitialize = _assertThisInitialized(_this),
          holderEl = _assertThisInitialize.holderEl,
          wrapperEl = _assertThisInitialize.wrapperEl,
          boundaryElement = _assertThisInitialize.boundaryElement,
          scrollElement = _assertThisInitialize.scrollElement,
          disabled = _assertThisInitialize.disabled;

      var _this$props = _this.props,
          mode = _this$props.mode,
          onFixedToggle = _this$props.onFixedToggle,
          offsetTransforms = _this$props.offsetTransforms;

      if (disabled) {
        if (_this.state.fixed) {
          _this.setState({
            fixed: false
          });
        }

        return;
      }

      if (!holderEl.getBoundingClientRect || !wrapperEl.getBoundingClientRect) {
        return;
      }

      var holderRect = holderEl.getBoundingClientRect();
      var wrapperRect = wrapperEl.getBoundingClientRect();
      var boundaryRect = boundaryElement ? getRect(boundaryElement) : {
        top: -Infinity,
        bottom: Infinity
      };
      var scrollRect = getRect(scrollElement);

      var fixed = _this.isFixed(holderRect, wrapperRect, boundaryRect, scrollRect);

      var offsets = null;

      if (offsetTransforms && fixed) {
        var closestTransformedParent = (0, _getClosestTransformedParent.default)(scrollElement);
        if (closestTransformedParent) offsets = getRect(closestTransformedParent);
      }

      var newState = {
        fixed: fixed,
        height: wrapperRect.height,
        styles: fixed ? buildStickyStyle(mode, _this.props, {
          boundaryTop: mode === 'bottom' ? boundaryRect.top : 0,
          boundaryBottom: mode === 'top' ? boundaryRect.bottom : 0,
          top: mode === 'top' ? scrollRect.top - (offsets ? offsets.top : 0) : 0,
          bottom: mode === 'bottom' ? scrollRect.bottom - (offsets ? offsets.bottom : 0) : 0,
          width: holderRect.width,
          height: wrapperRect.height
        }) : null
      };

      if (fixed !== _this.state.fixed && onFixedToggle && typeof onFixedToggle === 'function') {
        onFixedToggle(_this.state.fixed);
      }

      if (!isEqual(_this.state, newState)) {
        _this.setState(newState);
      }
    });

    _this.disabled = props.disabled;
    _this.state = {
      height: 0,
      fixed: false,
      styles: null
    };
    return _this;
  }

  _createClass(Sticky, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var me = _reactDom.default.findDOMNode(this);

      var _this$props2 = this.props,
          boundaryElement = _this$props2.boundaryElement,
          scrollElement = _this$props2.scrollElement,
          noExceptionOnMissedScrollElement = _this$props2.noExceptionOnMissedScrollElement,
          positionRecheckInterval = _this$props2.positionRecheckInterval,
          disabled = _this$props2.disabled;
      this.disabled = disabled;
      this.boundaryElement = (0, _find.default)(boundaryElement, me);

      if (this.boundaryElement === window || this.boundaryElement === document) {
        // such objects can't be used as boundary
        // and in fact there is no point in such a case
        this.boundaryElement = null;
      }

      this.scrollElement = scrollElement;

      if (typeof scrollElement === 'string') {
        this.scrollElement = (0, _find.default)(scrollElement, me);
      }

      if (this.scrollElement) {
        (0, _events.listen)(this.scrollElement, ['scroll'], this.checkPosition);
      } else if (!noExceptionOnMissedScrollElement) {
        throw new Error('Cannot find scrollElement ' + scrollElement);
      }

      (0, _events.listen)(window, ['scroll', 'resize', 'pageshow', 'load'], this.checkPosition);
      this.checkPosition();

      if (positionRecheckInterval) {
        this.checkPositionIntervalId = setInterval(this.checkPosition, positionRecheckInterval);
      }
    }
  }, {
    key: "UNSAFE_componentWillReceiveProps",
    value: function UNSAFE_componentWillReceiveProps(_ref) {
      var disabled = _ref.disabled;

      if (this.disabled !== disabled) {
        this.disabled = disabled;
        this.checkPosition();
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.scrollElement) {
        (0, _events.unlisten)(this.scrollElement, ['scroll'], this.checkPosition);
      }

      (0, _events.unlisten)(window, ['scroll', 'resize', 'pageshow', 'load'], this.checkPosition);
      this.boundaryElement = null;
      this.scrollElement = null;
      clearTimeout(this.checkPositionIntervalId);
    }
  }, {
    key: "isFixed",
    value: function isFixed(holderRect, wrapperRect, boundaryRect, scrollRect) {
      var _this$props3 = this.props,
          hideOnBoundaryHit = _this$props3.hideOnBoundaryHit,
          bottomOffset = _this$props3.bottomOffset,
          topOffset = _this$props3.topOffset,
          mode = _this$props3.mode;

      if (this.disabled) {
        return false;
      }

      if (boundaryRect && !instersect(boundaryRect, scrollRect, topOffset, bottomOffset)) {
        return false;
      }

      var hideOffset = hideOnBoundaryHit ? wrapperRect.height + bottomOffset : 0;

      if (mode === 'top') {
        return holderRect.top + topOffset < scrollRect.top && scrollRect.top + hideOffset <= boundaryRect.bottom;
      }

      return holderRect.bottom - topOffset > scrollRect.bottom && scrollRect.bottom - hideOffset >= boundaryRect.top;
    }
  }, {
    key: "render",
    value: function render() {
      var props = this.props;
      var _this$state = this.state,
          fixed = _this$state.fixed,
          height = _this$state.height;
      var stickyClassName = props.stickyClassName,
          stickyStyle = props.stickyStyle,
          holderCmp = props.holderCmp,
          wrapperCmp = props.wrapperCmp,
          holderProps = props.holderProps,
          children = props.children;
      var wrapperProps = sanitizeProps(props, stickyOwnProps); // To ensure that this component becomes sticky immediately on mobile devices instead
      // of disappearing until the scroll event completes, we add `transform: translateZ(0)`
      // to 'kick' rendering of this element to the GPU
      // @see http://stackoverflow.com/questions/32875046

      var wrapperStyle = {
        transform: 'translateZ(0)',
        WebkitTransform: 'translateZ(0)'
      };

      if (wrapperProps.style) {
        wrapperStyle = _objectSpread({}, wrapperStyle, {}, wrapperProps.style);
      }

      if (fixed) {
        wrapperProps.className += ' ' + stickyClassName;
        wrapperStyle = _objectSpread({}, wrapperStyle, {}, stickyStyle, {}, this.state.styles);
      }

      holderProps.style = _objectSpread({}, holderProps.style, {
        minHeight: height + 'px'
      });
      holderProps.ref = this.createHolderRef;
      wrapperProps.style = wrapperStyle;
      wrapperProps.ref = this.createWrapperRef;
      return _react.default.createElement(holderCmp, holderProps, _react.default.createElement(wrapperCmp, wrapperProps, children));
    }
  }]);

  return Sticky;
}(_react.Component); // some helpers


exports.default = Sticky;

_defineProperty(Sticky, "propTypes", {
  mode: _propTypes.default.oneOf(['top', 'bottom']),
  onFixedToggle: _propTypes.default.func,
  stickyStyle: _propTypes.default.object,
  stickyClassName: _propTypes.default.string,
  hideOnBoundaryHit: _propTypes.default.bool,
  offsetTransforms: _propTypes.default.bool,
  disabled: _propTypes.default.bool,
  boundaryElement: _propTypes.default.string,
  scrollElement: _propTypes.default.any,
  bottomOffset: _propTypes.default.number,
  topOffset: _propTypes.default.number,
  positionRecheckInterval: _propTypes.default.number,
  noExceptionOnMissedScrollElement: _propTypes.default.bool,
  wrapperCmp: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.func]),
  holderCmp: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.func]),
  holderProps: _propTypes.default.object
});

_defineProperty(Sticky, "defaultProps", {
  className: '',
  style: {},
  mode: 'top',
  holderCmp: 'div',
  holderProps: {},
  wrapperCmp: 'div',
  stickyClassName: 'sticky',
  stickyStyle: null,
  hideOnBoundaryHit: true,
  offsetTransforms: false,
  disabled: false,
  boundaryElement: null,
  scrollElement: 'window',
  topOffset: 0,
  bottomOffset: 0,
  noExceptionOnMissedScrollElement: false,
  positionRecheckInterval: 0
});

function getRect(el) {
  if (el && typeof el.getBoundingClientRect === 'function') {
    return el.getBoundingClientRect();
  }

  if (el === window || el === document) {
    return {
      top: 0,
      left: 0,
      bottom: window.innerHeight,
      height: window.innerHeight,
      width: window.innerWidth,
      right: window.innerWidth
    };
  }

  return {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: 0,
    height: 0
  };
}

function instersect(r1, r2, topOffset, bottomOffset) {
  var r1Top = r1.top + topOffset,
      r1Bottom = r1.bottom + bottomOffset;
  return r1Top >= r2.top && r1Top <= r2.bottom || r1Bottom >= r2.top && r1Bottom <= r2.bottom || r1Bottom >= r2.bottom && r1Top <= r2.top;
}
/**
 * Simply removes all unwanted props in order to avoid react 'unkown prop' warning
 * @param  {Object} props     that should be sanitized
 * @param  {Object} toRemove  array of prop names to remove
 * @return {Object}           cloned and sanitized props
 */


function sanitizeProps(props, toRemove) {
  props = _objectSpread({}, props);

  for (var i = 0, l = toRemove.length; i < l; i += 1) {
    delete props[toRemove[i]];
  }

  return props;
}

module.exports = exports.default;

/***/ })

}]);
//# sourceMappingURL=55.bundle.js.map