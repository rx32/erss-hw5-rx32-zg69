(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[46],{

/***/ "./src/main/js/src/components/banner/BannerFifteenSingle.js":
/*!******************************************************************!*\
  !*** ./src/main/js/src/components/banner/BannerFifteenSingle.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");



var BannerFifteenSingle = function BannerFifteenSingle(_ref) {
  var data = _ref.data,
      spaceBottomClass = _ref.spaceBottomClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-xl-4 col-md-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "single-banner ".concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + data.link
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + data.image,
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "banner-content banner-content--style2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", null, data.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", null, data.subtitle), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + data.link
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-long-arrow-right"
  })))));
};

/* harmony default export */ __webpack_exports__["default"] = (BannerFifteenSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/banner/BannerSixteenSingle.js":
/*!******************************************************************!*\
  !*** ./src/main/js/src/components/banner/BannerSixteenSingle.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");




var BannerSixteenSingle = function BannerSixteenSingle(_ref) {
  var data = _ref.data,
      spaceBottomClass = _ref.spaceBottomClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-lg-6 col-md-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "single-banner-2 ".concat(spaceBottomClass ? spaceBottomClass : "", " ").concat(data.textAlign === "right" ? "align_right" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    to: process.env.PUBLIC_URL + data.link
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + data.image,
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "banner-content-2 banner-content-2--style2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", null, data.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, data.subtitle, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, data.price)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    to: process.env.PUBLIC_URL + data.link
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-long-arrow-right"
  })))));
};

BannerSixteenSingle.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (BannerSixteenSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/feature-icon/FeatureIconFourSingle.js":
/*!**************************************************************************!*\
  !*** ./src/main/js/src/components/feature-icon/FeatureIconFourSingle.js ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



var FeatureIconFourSingle = function FeatureIconFourSingle(_ref) {
  var data = _ref.data,
      spaceBottomClass = _ref.spaceBottomClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-lg-4 col-md-6 col-sm-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "support-wrap-3 text-center ".concat(spaceBottomClass ? spaceBottomClass : ""),
    style: {
      backgroundColor: "".concat(data.backgroundColor)
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "support-icon-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    className: "animated",
    src: process.env.PUBLIC_URL + data.iconImage,
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "support-content-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + data.titleImage,
    alt: ""
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, data.title))));
};

FeatureIconFourSingle.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (FeatureIconFourSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/hero-slider/HeroSliderNineteenSingle.js":
/*!****************************************************************************!*\
  !*** ./src/main/js/src/components/hero-slider/HeroSliderNineteenSingle.js ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");




var HeroSliderNineteenSingle = function HeroSliderNineteenSingle(_ref) {
  var data = _ref.data,
      sliderClass = _ref.sliderClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "single-slider-2 slider-height-2 d-flex align-items-center bg-img ".concat(sliderClass ? sliderClass : ""),
    style: {
      backgroundImage: "url(".concat(process.env.PUBLIC_URL + data.image, ")")
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-xl-6 col-lg-7 col-md-8 col-12 ml-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-content-2 slider-animated-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", {
    className: "animated no-style"
  }, data.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "animated",
    dangerouslySetInnerHTML: {
      __html: data.subtitle
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-btn slider-btn--style2 btn-hover"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    className: "animated rounden-btn",
    to: process.env.PUBLIC_URL + data.url
  }, "SHOP NOW")))))));
};

HeroSliderNineteenSingle.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  sliderClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (HeroSliderNineteenSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/data/banner/banner-fifteen.json":
/*!*********************************************************!*\
  !*** ./src/main/js/src/data/banner/banner-fifteen.json ***!
  \*********************************************************/
/*! exports provided: 0, 1, 2, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"image\":\"/assets/img/banner/banner14.jpg\",\"title\":\"New Arrivals\",\"subtitle\":\"Choose your products here\",\"link\":\"/shop-grid-standard\"},{\"id\":2,\"image\":\"/assets/img/banner/banner15.jpg\",\"title\":\"Featured Products\",\"subtitle\":\"Choose your products here\",\"link\":\"/shop-grid-standard\"},{\"id\":3,\"image\":\"/assets/img/banner/banner16.jpg\",\"title\":\"Best Seller\",\"subtitle\":\"Choose your products here\",\"link\":\"/shop-grid-standard\"}]");

/***/ }),

/***/ "./src/main/js/src/data/banner/banner-sixteen.json":
/*!*********************************************************!*\
  !*** ./src/main/js/src/data/banner/banner-sixteen.json ***!
  \*********************************************************/
/*! exports provided: 0, 1, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"image\":\"/assets/img/banner/banner17.jpg\",\"title\":\"Featured Products\",\"subtitle\":\"Choose Your Products Here\",\"link\":\"/shop-grid-standard\",\"textAlign\":\"right\"},{\"id\":2,\"image\":\"/assets/img/banner/banner18.jpg\",\"title\":\"New Products\",\"subtitle\":\"Choose Your Products Here\",\"link\":\"/shop-grid-standard\",\"textAlign\":\"left\"}]");

/***/ }),

/***/ "./src/main/js/src/data/feature-icons/feature-icon-four.json":
/*!*******************************************************************!*\
  !*** ./src/main/js/src/data/feature-icons/feature-icon-four.json ***!
  \*******************************************************************/
/*! exports provided: 0, 1, 2, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"titleImage\":\"/assets/img/icon-img/support-8.png\",\"title\":\"Free shipping on all order\",\"iconImage\":\"/assets/img/icon-img/support-5.png\",\"backgroundColor\":\"#ccfbe9\"},{\"id\":2,\"titleImage\":\"/assets/img/icon-img/support-9.png\",\"title\":\"Back guarantee under 5 days\",\"iconImage\":\"/assets/img/icon-img/support-6.png\",\"backgroundColor\":\"#f2fbcc\"},{\"id\":3,\"titleImage\":\"/assets/img/icon-img/support-10.png\",\"title\":\"On every order over $150\",\"iconImage\":\"/assets/img/icon-img/support-7.png\",\"backgroundColor\":\"#ddfbcc\"}]");

/***/ }),

/***/ "./src/main/js/src/data/hero-sliders/hero-slider-nineteen.json":
/*!*********************************************************************!*\
  !*** ./src/main/js/src/data/hero-sliders/hero-slider-nineteen.json ***!
  \*********************************************************************/
/*! exports provided: 0, 1, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"title\":\"Enjoy This Offer Today\",\"subtitle\":\"New Collection <br /> Sale 40%\",\"image\":\"/assets/img/slider/slider-17.jpg\",\"url\":\"/shop-grid-standard\"},{\"id\":2,\"title\":\"Enjoy This Offer Today\",\"subtitle\":\"New Collection <br /> Sale 20%\",\"image\":\"/assets/img/slider/slider-18.jpg\",\"url\":\"/shop-grid-standard\"}]");

/***/ }),

/***/ "./src/main/js/src/pages/home/HomeFurnitureThree.js":
/*!**********************************************************!*\
  !*** ./src/main/js/src/pages/home/HomeFurnitureThree.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-meta-tags */ "./node_modules/react-meta-tags/lib/index.js");
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_meta_tags__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_LayoutOne__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../layouts/LayoutOne */ "./src/main/js/src/layouts/LayoutOne.js");
/* harmony import */ var _wrappers_hero_slider_HeroSliderNineteen__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../wrappers/hero-slider/HeroSliderNineteen */ "./src/main/js/src/wrappers/hero-slider/HeroSliderNineteen.js");
/* harmony import */ var _wrappers_banner_BannerSixteen__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../wrappers/banner/BannerSixteen */ "./src/main/js/src/wrappers/banner/BannerSixteen.js");
/* harmony import */ var _wrappers_feature_icon_FeatureIconFour__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../wrappers/feature-icon/FeatureIconFour */ "./src/main/js/src/wrappers/feature-icon/FeatureIconFour.js");
/* harmony import */ var _wrappers_countdown_CountDownThree__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../wrappers/countdown/CountDownThree */ "./src/main/js/src/wrappers/countdown/CountDownThree.js");
/* harmony import */ var _wrappers_newsletter_NewsletterThree__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../wrappers/newsletter/NewsletterThree */ "./src/main/js/src/wrappers/newsletter/NewsletterThree.js");
/* harmony import */ var _wrappers_banner_BannerFifteen__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../wrappers/banner/BannerFifteen */ "./src/main/js/src/wrappers/banner/BannerFifteen.js");
/* harmony import */ var _wrappers_product_TabProductTwelve__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../wrappers/product/TabProductTwelve */ "./src/main/js/src/wrappers/product/TabProductTwelve.js");











var HomeFurnitureThree = function HomeFurnitureThree() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_meta_tags__WEBPACK_IMPORTED_MODULE_1___default.a, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", null, "Flone | Furniture Home"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: "Furniture home of flone react minimalist eCommerce template."
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_LayoutOne__WEBPACK_IMPORTED_MODULE_2__["default"], {
    headerContainerClass: "container-fluid",
    headerPaddingClass: "header-padding-2",
    headerTop: "visible"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_hero_slider_HeroSliderNineteen__WEBPACK_IMPORTED_MODULE_3__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_banner_BannerFifteen__WEBPACK_IMPORTED_MODULE_8__["default"], {
    spaceTopClass: "pt-10",
    spaceBottomClass: "pb-85"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_countdown_CountDownThree__WEBPACK_IMPORTED_MODULE_6__["default"], {
    spaceTopClass: "pt-100",
    spaceBottomClass: "pb-100",
    dateTime: "November 13, 2020 12:12:00",
    countDownImage: "/assets/img/banner/deal-5.png"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_feature_icon_FeatureIconFour__WEBPACK_IMPORTED_MODULE_5__["default"], {
    bgImg: "/assets/img/bg/shape.png",
    containerClass: "container-fluid",
    gutterClass: "padding-10-row-col",
    spaceTopClass: "pt-50",
    spaceBottomClass: "pb-40"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_product_TabProductTwelve__WEBPACK_IMPORTED_MODULE_9__["default"], {
    category: "furniture",
    spaceTopClass: "pt-95",
    sectionTitle: "Best Products"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_banner_BannerSixteen__WEBPACK_IMPORTED_MODULE_4__["default"], {
    spaceTopClass: "pt-95"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_newsletter_NewsletterThree__WEBPACK_IMPORTED_MODULE_7__["default"], {
    spaceTopClass: "pt-100",
    spaceBottomClass: "pb-100",
    subscribeBtnClass: "dark-red-subscribe"
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (HomeFurnitureThree);

/***/ }),

/***/ "./src/main/js/src/wrappers/banner/BannerFifteen.js":
/*!**********************************************************!*\
  !*** ./src/main/js/src/wrappers/banner/BannerFifteen.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _data_banner_banner_fifteen_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../data/banner/banner-fifteen.json */ "./src/main/js/src/data/banner/banner-fifteen.json");
var _data_banner_banner_fifteen_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/banner/banner-fifteen.json */ "./src/main/js/src/data/banner/banner-fifteen.json", 1);
/* harmony import */ var _components_banner_BannerFifteenSingle_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/banner/BannerFifteenSingle.js */ "./src/main/js/src/components/banner/BannerFifteenSingle.js");





var BannerFifteen = function BannerFifteen(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "banner-area banner-area-2 ".concat(spaceTopClass ? spaceTopClass : "", " ").concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container-fluid"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "custom-row-2"
  }, _data_banner_banner_fifteen_json__WEBPACK_IMPORTED_MODULE_2__ && _data_banner_banner_fifteen_json__WEBPACK_IMPORTED_MODULE_2__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_banner_BannerFifteenSingle_js__WEBPACK_IMPORTED_MODULE_3__["default"], {
      spaceBottomClass: "mb-10",
      data: single,
      key: key
    });
  }))));
};

BannerFifteen.propTypes = {
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (BannerFifteen);

/***/ }),

/***/ "./src/main/js/src/wrappers/banner/BannerSixteen.js":
/*!**********************************************************!*\
  !*** ./src/main/js/src/wrappers/banner/BannerSixteen.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _data_banner_banner_sixteen_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../data/banner/banner-sixteen.json */ "./src/main/js/src/data/banner/banner-sixteen.json");
var _data_banner_banner_sixteen_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/banner/banner-sixteen.json */ "./src/main/js/src/data/banner/banner-sixteen.json", 1);
/* harmony import */ var _components_banner_BannerSixteenSingle_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/banner/BannerSixteenSingle.js */ "./src/main/js/src/components/banner/BannerSixteenSingle.js");





var BannerSixteen = function BannerSixteen(_ref) {
  var spaceBottomClass = _ref.spaceBottomClass,
      spaceTopClass = _ref.spaceTopClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "banner-area ".concat(spaceTopClass ? spaceTopClass : "", " ").concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row no-gutters"
  }, _data_banner_banner_sixteen_json__WEBPACK_IMPORTED_MODULE_2__ && _data_banner_banner_sixteen_json__WEBPACK_IMPORTED_MODULE_2__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_banner_BannerSixteenSingle_js__WEBPACK_IMPORTED_MODULE_3__["default"], {
      data: single,
      key: key,
      spaceBottomClass: "mb-30"
    });
  })));
};

BannerSixteen.propTypes = {
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (BannerSixteen);

/***/ }),

/***/ "./src/main/js/src/wrappers/feature-icon/FeatureIconFour.js":
/*!******************************************************************!*\
  !*** ./src/main/js/src/wrappers/feature-icon/FeatureIconFour.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _data_feature_icons_feature_icon_four_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../data/feature-icons/feature-icon-four.json */ "./src/main/js/src/data/feature-icons/feature-icon-four.json");
var _data_feature_icons_feature_icon_four_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/feature-icons/feature-icon-four.json */ "./src/main/js/src/data/feature-icons/feature-icon-four.json", 1);
/* harmony import */ var _components_feature_icon_FeatureIconFourSingle_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/feature-icon/FeatureIconFourSingle.js */ "./src/main/js/src/components/feature-icon/FeatureIconFourSingle.js");





var FeatureIconFour = function FeatureIconFour(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass,
      containerClass = _ref.containerClass,
      gutterClass = _ref.gutterClass,
      responsiveClass = _ref.responsiveClass,
      bgImg = _ref.bgImg;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "support-area hm9-section-padding ".concat(spaceTopClass ? spaceTopClass : "", " ").concat(spaceBottomClass ? spaceBottomClass : "", " ").concat(responsiveClass ? responsiveClass : ""),
    style: bgImg ? {
      backgroundImage: "url(".concat(process.env.PUBLIC_URL + bgImg, ")")
    } : {}
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "".concat(containerClass ? containerClass : "", " ").concat(gutterClass ? gutterClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, _data_feature_icons_feature_icon_four_json__WEBPACK_IMPORTED_MODULE_2__ && _data_feature_icons_feature_icon_four_json__WEBPACK_IMPORTED_MODULE_2__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_feature_icon_FeatureIconFourSingle_js__WEBPACK_IMPORTED_MODULE_3__["default"], {
      data: single,
      spaceBottomClass: "mb-10",
      key: key
    });
  }))));
};

FeatureIconFour.propTypes = {
  bgImg: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  containerClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  gutterClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  responsiveClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (FeatureIconFour);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/wrappers/hero-slider/HeroSliderNineteen.js":
/*!********************************************************************!*\
  !*** ./src/main/js/src/wrappers/hero-slider/HeroSliderNineteen.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-id-swiper */ "./node_modules/react-id-swiper/lib/index.js");
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_id_swiper__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _data_hero_sliders_hero_slider_nineteen_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../data/hero-sliders/hero-slider-nineteen.json */ "./src/main/js/src/data/hero-sliders/hero-slider-nineteen.json");
var _data_hero_sliders_hero_slider_nineteen_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/hero-sliders/hero-slider-nineteen.json */ "./src/main/js/src/data/hero-sliders/hero-slider-nineteen.json", 1);
/* harmony import */ var _components_hero_slider_HeroSliderNineteenSingle_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/hero-slider/HeroSliderNineteenSingle.js */ "./src/main/js/src/components/hero-slider/HeroSliderNineteenSingle.js");





var HeroSliderNineteen = function HeroSliderNineteen() {
  var params = {
    effect: "fade",
    loop: true,
    speed: 1000,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    watchSlidesVisibility: true,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    renderPrevButton: function renderPrevButton() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "swiper-button-prev ht-swiper-button-nav"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "pe-7s-angle-left"
      }));
    },
    renderNextButton: function renderNextButton() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "swiper-button-next ht-swiper-button-nav"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "pe-7s-angle-right"
      }));
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "slider-area"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "slider-active nav-style-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_id_swiper__WEBPACK_IMPORTED_MODULE_1___default.a, params, _data_hero_sliders_hero_slider_nineteen_json__WEBPACK_IMPORTED_MODULE_2__ && _data_hero_sliders_hero_slider_nineteen_json__WEBPACK_IMPORTED_MODULE_2__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_hero_slider_HeroSliderNineteenSingle_js__WEBPACK_IMPORTED_MODULE_3__["default"], {
      data: single,
      key: key,
      sliderClass: "swiper-slide"
    });
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (HeroSliderNineteen);

/***/ })

}]);
//# sourceMappingURL=46.bundle.js.map