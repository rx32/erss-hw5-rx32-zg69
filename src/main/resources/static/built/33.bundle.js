(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[33],{

/***/ "./src/main/js/src/components/banner/BannerSevenSingle.js":
/*!****************************************************************!*\
  !*** ./src/main/js/src/components/banner/BannerSevenSingle.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");



var BannerSevenSingle = function BannerSevenSingle(_ref) {
  var data = _ref.data,
      spaceBottomClass = _ref.spaceBottomClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-lg-4 col-md-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "single-banner ".concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + data.link
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + data.image,
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "banner-content banner-pink"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", null, data.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", null, data.subtitle, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, data.price)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: data.link
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-long-arrow-right"
  })))));
};

/* harmony default export */ __webpack_exports__["default"] = (BannerSevenSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/header/Logo.js":
/*!***************************************************!*\
  !*** ./src/main/js/src/components/header/Logo.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");




var Logo = function Logo(_ref) {
  var imageUrl = _ref.imageUrl,
      logoClass = _ref.logoClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "".concat(logoClass ? logoClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    to: process.env.PUBLIC_URL + "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    alt: "",
    src: process.env.PUBLIC_URL + imageUrl
  })));
};

Logo.propTypes = {
  imageUrl: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  logoClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (Logo);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/header/sub-components/LanguageCurrencyChanger.js":
/*!*************************************************************************************!*\
  !*** ./src/main/js/src/components/header/sub-components/LanguageCurrencyChanger.js ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var redux_multilanguage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux-multilanguage */ "./node_modules/redux-multilanguage/lib/bundle.js");
/* harmony import */ var redux_multilanguage__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(redux_multilanguage__WEBPACK_IMPORTED_MODULE_2__);




var LanguageCurrencyChanger = function LanguageCurrencyChanger(_ref) {
  var currency = _ref.currency,
      setCurrency = _ref.setCurrency,
      currentLanguageCode = _ref.currentLanguageCode,
      dispatch = _ref.dispatch;

  var changeLanguageTrigger = function changeLanguageTrigger(e) {
    var languageCode = e.target.value;
    dispatch(Object(redux_multilanguage__WEBPACK_IMPORTED_MODULE_2__["changeLanguage"])(languageCode));
  };

  var setCurrencyTrigger = function setCurrencyTrigger(e) {
    var currencyName = e.target.value;
    setCurrency(currencyName);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "language-currency-wrap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "same-language-currency language-style"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, currentLanguageCode === "en" ? "English" : currentLanguageCode === "fn" ? "French" : currentLanguageCode === "de" ? "Germany" : "", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-angle-down"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lang-car-dropdown"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    value: "en",
    onClick: function onClick(e) {
      return changeLanguageTrigger(e);
    }
  }, "English")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    value: "fn",
    onClick: function onClick(e) {
      return changeLanguageTrigger(e);
    }
  }, "French")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    value: "de",
    onClick: function onClick(e) {
      return changeLanguageTrigger(e);
    }
  }, "Germany"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "same-language-currency use-style"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, currency.currencyName, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-angle-down"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lang-car-dropdown"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    value: "USD",
    onClick: function onClick(e) {
      return setCurrencyTrigger(e);
    }
  }, "USD")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    value: "EUR",
    onClick: function onClick(e) {
      return setCurrencyTrigger(e);
    }
  }, "EUR")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    value: "GBP",
    onClick: function onClick(e) {
      return setCurrencyTrigger(e);
    }
  }, "GBP"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "same-language-currency"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Call Us 3965410")));
};

LanguageCurrencyChanger.propTypes = {
  setCurrency: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func,
  currency: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  currentLanguageCode: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  dispatch: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func
};
/* harmony default export */ __webpack_exports__["default"] = (LanguageCurrencyChanger);

/***/ }),

/***/ "./src/main/js/src/components/hero-slider/HeroSliderSixSingle.js":
/*!***********************************************************************!*\
  !*** ./src/main/js/src/components/hero-slider/HeroSliderSixSingle.js ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");




var HeroSliderSixSingle = function HeroSliderSixSingle(_ref) {
  var data = _ref.data,
      sliderClass = _ref.sliderClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "single-slider-2 slider-height-9 d-flex align-items-center slider-height-res hm-13-slider ".concat(sliderClass ? sliderClass : ""),
    style: {
      backgroundImage: "url(".concat(process.env.PUBLIC_URL + data.image, ")")
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-xl-12 col-lg-12 col-md-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-content-7 slider-content-mrg-hm17 slider-animated-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", {
    className: "animated"
  }, data.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "animated",
    dangerouslySetInnerHTML: {
      __html: data.subtitle
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-btn-9 btn-hover"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    className: "animated",
    to: process.env.PUBLIC_URL + data.url
  }, "SHOP NOW")))))));
};

HeroSliderSixSingle.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  sliderClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (HeroSliderSixSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/newsletter/SubscribeEmailTwo.js":
/*!********************************************************************!*\
  !*** ./src/main/js/src/components/newsletter/SubscribeEmailTwo.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_mailchimp_subscribe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-mailchimp-subscribe */ "./node_modules/react-mailchimp-subscribe/es/index.js");




var CustomForm = function CustomForm(_ref) {
  var status = _ref.status,
      message = _ref.message,
      onValidated = _ref.onValidated,
      spaceTopClass = _ref.spaceTopClass,
      subscribeBtnClass = _ref.subscribeBtnClass;
  var email;

  var submit = function submit() {
    email && email.value.indexOf("@") > -1 && onValidated({
      EMAIL: email.value
    });
    email.value = "";
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "subscribe-form-3 ".concat(spaceTopClass ? spaceTopClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mc-form"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    className: "email",
    ref: function ref(node) {
      return email = node;
    },
    type: "email",
    placeholder: "Youe Email Addres",
    required: true
  })), status === "sending" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      color: "#3498db",
      fontSize: "12px"
    }
  }, "sending..."), status === "error" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      color: "#e74c3c",
      fontSize: "12px"
    },
    dangerouslySetInnerHTML: {
      __html: message
    }
  }), status === "success" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      color: "#2ecc71",
      fontSize: "12px"
    },
    dangerouslySetInnerHTML: {
      __html: message
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "clear-3 ".concat(subscribeBtnClass ? subscribeBtnClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "button",
    onClick: submit
  }, "SUBSCRIBE"))));
};

var SubscribeEmailTwo = function SubscribeEmailTwo(_ref2) {
  var mailchimpUrl = _ref2.mailchimpUrl,
      spaceTopClass = _ref2.spaceTopClass,
      subscribeBtnClass = _ref2.subscribeBtnClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_mailchimp_subscribe__WEBPACK_IMPORTED_MODULE_2__["default"], {
    url: mailchimpUrl,
    render: function render(_ref3) {
      var subscribe = _ref3.subscribe,
          status = _ref3.status,
          message = _ref3.message;
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(CustomForm, {
        status: status,
        message: message,
        onValidated: function onValidated(formData) {
          return subscribe(formData);
        },
        spaceTopClass: spaceTopClass,
        subscribeBtnClass: subscribeBtnClass
      });
    }
  }));
};

SubscribeEmailTwo.propTypes = {
  mailchimpUrl: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (SubscribeEmailTwo);

/***/ }),

/***/ "./src/main/js/src/components/section-title/SectionTitleThree.js":
/*!***********************************************************************!*\
  !*** ./src/main/js/src/components/section-title/SectionTitleThree.js ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



var SectionTitleThree = function SectionTitleThree(_ref) {
  var titleText = _ref.titleText,
      positionClass = _ref.positionClass,
      spaceClass = _ref.spaceClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "section-title-5 ".concat(positionClass ? positionClass : "", " ").concat(spaceClass ? spaceClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", null, titleText));
};

SectionTitleThree.propTypes = {
  positionClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  titleText: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (SectionTitleThree);

/***/ }),

/***/ "./src/main/js/src/components/testimonial/TestimonialOneSingle.js":
/*!************************************************************************!*\
  !*** ./src/main/js/src/components/testimonial/TestimonialOneSingle.js ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



var TestimonialOneSingle = function TestimonialOneSingle(_ref) {
  var data = _ref.data,
      sliderClass = _ref.sliderClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "single-testimonial text-center ".concat(sliderClass ? sliderClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + data.image,
    alt: ""
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, data.content), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "client-info"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-map-signs"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, data.customerName), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, data.title)));
};

TestimonialOneSingle.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  sliderClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (TestimonialOneSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/data/banner/banner-seven.json":
/*!*******************************************************!*\
  !*** ./src/main/js/src/data/banner/banner-seven.json ***!
  \*******************************************************/
/*! exports provided: 0, 1, 2, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"image\":\"/assets/img/banner/banner-35.png\",\"title\":\"Flower\",\"subtitle\":\"Starting at\",\"price\":\"$99.00\",\"link\":\"/shop-grid-standard\"},{\"id\":2,\"image\":\"/assets/img/banner/banner-36.png\",\"title\":\"Flower\",\"subtitle\":\"Starting at\",\"price\":\"$79.00\",\"link\":\"/shop-grid-standard\"},{\"id\":3,\"image\":\"/assets/img/banner/banner-37.png\",\"title\":\"Flower\",\"subtitle\":\"Starting at\",\"price\":\"$29.00\",\"link\":\"/shop-grid-standard\"}]");

/***/ }),

/***/ "./src/main/js/src/data/hero-sliders/hero-slider-six.json":
/*!****************************************************************!*\
  !*** ./src/main/js/src/data/hero-sliders/hero-slider-six.json ***!
  \****************************************************************/
/*! exports provided: 0, 1, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"title\":\"New Arrivals\",\"subtitle\":\"Fresh Your Mind & <br/> Feeling love\",\"image\":\"/assets/img/slider/slider-10.jpg\",\"url\":\"/shop-grid-standard\"},{\"id\":2,\"title\":\"New Arrivals\",\"subtitle\":\"Fresh Your Mind & <br/> Feeling love\",\"image\":\"/assets/img/slider/slider-10-2.jpg\",\"url\":\"/shop-grid-standard\"}]");

/***/ }),

/***/ "./src/main/js/src/data/testimonial/testimonial-one.json":
/*!***************************************************************!*\
  !*** ./src/main/js/src/data/testimonial/testimonial-one.json ***!
  \***************************************************************/
/*! exports provided: 0, 1, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":\"1\",\"image\":\"/assets/img/testimonial/1.jpg\",\"content\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\",\"customerName\":\"Grace Alvarado\",\"title\":\"customer\"},{\"id\":\"2\",\"image\":\"/assets/img/testimonial/2.jpg\",\"content\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\",\"customerName\":\"John Doe\",\"title\":\"customer\"}]");

/***/ }),

/***/ "./src/main/js/src/layouts/LayoutTwo.js":
/*!**********************************************!*\
  !*** ./src/main/js/src/layouts/LayoutTwo.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wrappers_header_HeaderTwo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../wrappers/header/HeaderTwo */ "./src/main/js/src/wrappers/header/HeaderTwo.js");
/* harmony import */ var _wrappers_footer_FooterOne__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../wrappers/footer/FooterOne */ "./src/main/js/src/wrappers/footer/FooterOne.js");





var LayoutTwo = function LayoutTwo(_ref) {
  var children = _ref.children,
      footerBgClass = _ref.footerBgClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_header_HeaderTwo__WEBPACK_IMPORTED_MODULE_2__["default"], null), children, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_footer_FooterOne__WEBPACK_IMPORTED_MODULE_3__["default"], {
    backgroundColorClass: footerBgClass ? footerBgClass : "bg-gray",
    spaceTopClass: "pt-100",
    spaceBottomClass: "pb-70"
  }));
};

LayoutTwo.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.any,
  footerBgClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (LayoutTwo);

/***/ }),

/***/ "./src/main/js/src/pages/home/HomeFlowerShop.js":
/*!******************************************************!*\
  !*** ./src/main/js/src/pages/home/HomeFlowerShop.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-meta-tags */ "./node_modules/react-meta-tags/lib/index.js");
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_meta_tags__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_LayoutTwo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../layouts/LayoutTwo */ "./src/main/js/src/layouts/LayoutTwo.js");
/* harmony import */ var _wrappers_hero_slider_HeroSliderSix__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../wrappers/hero-slider/HeroSliderSix */ "./src/main/js/src/wrappers/hero-slider/HeroSliderSix.js");
/* harmony import */ var _wrappers_banner_BannerSix__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../wrappers/banner/BannerSix */ "./src/main/js/src/wrappers/banner/BannerSix.js");
/* harmony import */ var _wrappers_product_TabProductFour__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../wrappers/product/TabProductFour */ "./src/main/js/src/wrappers/product/TabProductFour.js");
/* harmony import */ var _wrappers_testimonial_TestimonialOne__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../wrappers/testimonial/TestimonialOne */ "./src/main/js/src/wrappers/testimonial/TestimonialOne.js");
/* harmony import */ var _wrappers_newsletter_NewsletterTwo__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../wrappers/newsletter/NewsletterTwo */ "./src/main/js/src/wrappers/newsletter/NewsletterTwo.js");
/* harmony import */ var _wrappers_banner_BannerSeven__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../wrappers/banner/BannerSeven */ "./src/main/js/src/wrappers/banner/BannerSeven.js");










var HomeFlowerShop = function HomeFlowerShop() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_meta_tags__WEBPACK_IMPORTED_MODULE_1___default.a, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", null, "Flone | Flower Shop Home"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: "Flower shop home of flone react minimalist eCommerce template."
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_LayoutTwo__WEBPACK_IMPORTED_MODULE_2__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_hero_slider_HeroSliderSix__WEBPACK_IMPORTED_MODULE_3__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_banner_BannerSix__WEBPACK_IMPORTED_MODULE_4__["default"], {
    spaceTopClass: "pt-100",
    spaceBottomClass: "pb-80"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_product_TabProductFour__WEBPACK_IMPORTED_MODULE_5__["default"], {
    spaceBottomClass: "pb-100",
    category: "flower",
    productTabClass: "product-tab-pink2"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_testimonial_TestimonialOne__WEBPACK_IMPORTED_MODULE_6__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_banner_BannerSeven__WEBPACK_IMPORTED_MODULE_8__["default"], {
    spaceTopClass: "pt-95",
    spaceBottomClass: "pb-70"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_newsletter_NewsletterTwo__WEBPACK_IMPORTED_MODULE_7__["default"], {
    spaceBottomClass: "pb-100",
    subscribeBtnClass: "pink-subscribe"
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (HomeFlowerShop);

/***/ }),

/***/ "./src/main/js/src/wrappers/banner/BannerSeven.js":
/*!********************************************************!*\
  !*** ./src/main/js/src/wrappers/banner/BannerSeven.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _data_banner_banner_seven_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../data/banner/banner-seven.json */ "./src/main/js/src/data/banner/banner-seven.json");
var _data_banner_banner_seven_json__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/banner/banner-seven.json */ "./src/main/js/src/data/banner/banner-seven.json", 1);
/* harmony import */ var _components_banner_BannerSevenSingle_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/banner/BannerSevenSingle.js */ "./src/main/js/src/components/banner/BannerSevenSingle.js");




var BannerSeven = function BannerSeven(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "banner-area ".concat(spaceTopClass ? spaceTopClass : "", "  ").concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row"
  }, _data_banner_banner_seven_json__WEBPACK_IMPORTED_MODULE_1__ && _data_banner_banner_seven_json__WEBPACK_IMPORTED_MODULE_1__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_banner_BannerSevenSingle_js__WEBPACK_IMPORTED_MODULE_2__["default"], {
      data: single,
      key: key,
      spaceBottomClass: "mb-30"
    });
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (BannerSeven);

/***/ }),

/***/ "./src/main/js/src/wrappers/banner/BannerSix.js":
/*!******************************************************!*\
  !*** ./src/main/js/src/wrappers/banner/BannerSix.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");



var BannerSix = function BannerSix(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "banner-area ".concat(spaceTopClass ? spaceTopClass : "", "  ").concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container padding-20-row-col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-lg-6 col-md-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "single-banner mb-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + "/assets/img/banner/banner-32.png",
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "banner-content-4 banner-position-hm15-2 pink-banner"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "-20% Off"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", null, "New Tulip"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", null, "Best for your Mind."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, "SHOP NOW")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-lg-6 col-md-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "single-banner mb-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + "/assets/img/banner/banner-33.png",
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "banner-content-3 banner-position-hm15-2 pink-banner"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", null, "Pink Tulip "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Starting At ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "$99.00")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-long-arrow-right"
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "single-banner mb-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + "/assets/img/banner/banner-34.png",
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "banner-content-3 banner-position-hm17-1 pink-banner"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", null, "Pink Tulip "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Starting At ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "$99.00")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-long-arrow-right"
  }))))))));
};

/* harmony default export */ __webpack_exports__["default"] = (BannerSix);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/wrappers/header/HeaderTwo.js":
/*!******************************************************!*\
  !*** ./src/main/js/src/wrappers/header/HeaderTwo.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _redux_actions_currencyActions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../redux/actions/currencyActions */ "./src/main/js/src/redux/actions/currencyActions.js");
/* harmony import */ var redux_multilanguage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! redux-multilanguage */ "./node_modules/redux-multilanguage/lib/bundle.js");
/* harmony import */ var redux_multilanguage__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(redux_multilanguage__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_header_Logo__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/header/Logo */ "./src/main/js/src/components/header/Logo.js");
/* harmony import */ var _components_header_IconGroup__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/header/IconGroup */ "./src/main/js/src/components/header/IconGroup.js");
/* harmony import */ var _components_header_NavMenu__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/header/NavMenu */ "./src/main/js/src/components/header/NavMenu.js");
/* harmony import */ var _components_header_MobileMenu__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../components/header/MobileMenu */ "./src/main/js/src/components/header/MobileMenu.js");
/* harmony import */ var _components_header_sub_components_LanguageCurrencyChanger__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../components/header/sub-components/LanguageCurrencyChanger */ "./src/main/js/src/components/header/sub-components/LanguageCurrencyChanger.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(n); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }












var HeaderTwo = function HeaderTwo(_ref) {
  var currency = _ref.currency,
      setCurrency = _ref.setCurrency,
      currentLanguageCode = _ref.currentLanguageCode,
      dispatch = _ref.dispatch;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      _useState2 = _slicedToArray(_useState, 2),
      scroll = _useState2[0],
      setScroll = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      _useState4 = _slicedToArray(_useState3, 2),
      headerTop = _useState4[0],
      setHeaderTop = _useState4[1];

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var header = document.querySelector(".sticky-bar");
    setHeaderTop(header.offsetTop);
    window.addEventListener("scroll", handleScroll);
    return function () {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  var handleScroll = function handleScroll() {
    setScroll(window.scrollY);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("header", {
    className: "header-area clearfix header-hm9 transparent-bar"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "header-top-area d-none d-lg-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-lg-5 col-md-8 col-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_header_sub_components_LanguageCurrencyChanger__WEBPACK_IMPORTED_MODULE_9__["default"], {
    currency: currency,
    setCurrency: setCurrency,
    currentLanguageCode: currentLanguageCode,
    dispatch: dispatch
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-lg-2 d-none d-lg-block text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_header_Logo__WEBPACK_IMPORTED_MODULE_5__["default"], {
    imageUrl: "/assets/img/logo/logo.png",
    logoClass: "logo-hm-9"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-lg-5 col-md-4 col-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_header_IconGroup__WEBPACK_IMPORTED_MODULE_6__["default"], null))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "header-bottom sticky-bar header-res-padding header-padding-2 ".concat(scroll > headerTop ? "stick" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-6 d-block d-lg-none"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_header_Logo__WEBPACK_IMPORTED_MODULE_5__["default"], {
    imageUrl: "/assets/img/logo/logo.png"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-6 d-block d-lg-none"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_header_IconGroup__WEBPACK_IMPORTED_MODULE_6__["default"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-xl-12 col-lg-12 d-none d-lg-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_header_NavMenu__WEBPACK_IMPORTED_MODULE_7__["default"], null))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_header_MobileMenu__WEBPACK_IMPORTED_MODULE_8__["default"], null))));
};

HeaderTwo.propTypes = {
  setCurrency: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func,
  currency: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  currentLanguageCode: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  dispatch: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.func
};

var mapStateToProps = function mapStateToProps(state) {
  return {
    currency: state.currencyData
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    setCurrency: function setCurrency(currencyName) {
      dispatch(Object(_redux_actions_currencyActions__WEBPACK_IMPORTED_MODULE_3__["setCurrency"])(currencyName));
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps, mapDispatchToProps)(Object(redux_multilanguage__WEBPACK_IMPORTED_MODULE_4__["multilanguage"])(HeaderTwo)));

/***/ }),

/***/ "./src/main/js/src/wrappers/hero-slider/HeroSliderSix.js":
/*!***************************************************************!*\
  !*** ./src/main/js/src/wrappers/hero-slider/HeroSliderSix.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-id-swiper */ "./node_modules/react-id-swiper/lib/index.js");
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_id_swiper__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _data_hero_sliders_hero_slider_six_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../data/hero-sliders/hero-slider-six.json */ "./src/main/js/src/data/hero-sliders/hero-slider-six.json");
var _data_hero_sliders_hero_slider_six_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/hero-sliders/hero-slider-six.json */ "./src/main/js/src/data/hero-sliders/hero-slider-six.json", 1);
/* harmony import */ var _components_hero_slider_HeroSliderSixSingle_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/hero-slider/HeroSliderSixSingle.js */ "./src/main/js/src/components/hero-slider/HeroSliderSixSingle.js");





var HeroSliderSix = function HeroSliderSix() {
  var params = {
    effect: "fade",
    loop: true,
    speed: 1000,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    watchSlidesVisibility: true,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    renderPrevButton: function renderPrevButton() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "swiper-button-prev ht-swiper-button-nav"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "pe-7s-angle-left"
      }));
    },
    renderNextButton: function renderNextButton() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "swiper-button-next ht-swiper-button-nav"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "pe-7s-angle-right"
      }));
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "slider-area"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "slider-active nav-style-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_id_swiper__WEBPACK_IMPORTED_MODULE_1___default.a, params, _data_hero_sliders_hero_slider_six_json__WEBPACK_IMPORTED_MODULE_2__ && _data_hero_sliders_hero_slider_six_json__WEBPACK_IMPORTED_MODULE_2__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_hero_slider_HeroSliderSixSingle_js__WEBPACK_IMPORTED_MODULE_3__["default"], {
      data: single,
      key: key,
      sliderClass: "swiper-slide"
    });
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (HeroSliderSix);

/***/ }),

/***/ "./src/main/js/src/wrappers/newsletter/NewsletterTwo.js":
/*!**************************************************************!*\
  !*** ./src/main/js/src/wrappers/newsletter/NewsletterTwo.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_newsletter_SubscribeEmailTwo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/newsletter/SubscribeEmailTwo */ "./src/main/js/src/components/newsletter/SubscribeEmailTwo.js");




var NewsletterTwo = function NewsletterTwo(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass,
      subscribeBtnClass = _ref.subscribeBtnClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "subscribe-area-3 ".concat(spaceTopClass ? spaceTopClass : "", " ").concat(spaceBottomClass ? spaceBottomClass : "", " ")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container-fluid"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-xl-5 col-lg-7 col-md-10 ml-auto mr-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "subscribe-style-3 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", null, "Subscribe "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Subscribe to our newsletter to receive news on update"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_newsletter_SubscribeEmailTwo__WEBPACK_IMPORTED_MODULE_2__["default"], {
    mailchimpUrl: "//devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&id=05d85f18ef",
    spaceTopClass: "mt-35",
    subscribeBtnClass: subscribeBtnClass
  }))))));
};

NewsletterTwo.propTypes = {
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (NewsletterTwo);

/***/ }),

/***/ "./src/main/js/src/wrappers/product/TabProductFour.js":
/*!************************************************************!*\
  !*** ./src/main/js/src/wrappers/product/TabProductFour.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/Tab */ "./node_modules/react-bootstrap/esm/Tab.js");
/* harmony import */ var react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap/Nav */ "./node_modules/react-bootstrap/esm/Nav.js");
/* harmony import */ var _components_section_title_SectionTitleThree__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/section-title/SectionTitleThree */ "./src/main/js/src/components/section-title/SectionTitleThree.js");
/* harmony import */ var _ProductGridTwo__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ProductGridTwo */ "./src/main/js/src/wrappers/product/ProductGridTwo.js");








var TabProductFour = function TabProductFour(_ref) {
  var spaceBottomClass = _ref.spaceBottomClass,
      category = _ref.category,
      productTabClass = _ref.productTabClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "product-area ".concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_section_title_SectionTitleThree__WEBPACK_IMPORTED_MODULE_5__["default"], {
    titleText: "Featured Product",
    positionClass: "text-center"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Container, {
    defaultActiveKey: "bestSeller"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"], {
    variant: "pills",
    className: "product-tab-list pt-35 pb-60 text-center ".concat(productTabClass ? productTabClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Link, {
    eventKey: "newArrival"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, "New Arrivals"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Link, {
    eventKey: "bestSeller"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, "Best Sellers"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Link, {
    eventKey: "saleItems"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, "Sale Items")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Content, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
    eventKey: "newArrival"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductGridTwo__WEBPACK_IMPORTED_MODULE_6__["default"], {
    category: category,
    type: "new",
    limit: 8,
    spaceBottomClass: "mb-25"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
    eventKey: "bestSeller"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductGridTwo__WEBPACK_IMPORTED_MODULE_6__["default"], {
    category: category,
    type: "bestSeller",
    limit: 8,
    spaceBottomClass: "mb-25"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
    eventKey: "saleItems"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductGridTwo__WEBPACK_IMPORTED_MODULE_6__["default"], {
    category: category,
    type: "saleItems",
    limit: 8,
    spaceBottomClass: "mb-25"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "view-more text-center mt-20 toggle-btn6 col-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    className: "loadMore6",
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, "VIEW MORE PRODUCTS"))));
};

TabProductFour.propTypes = {
  category: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (TabProductFour);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/wrappers/testimonial/TestimonialOne.js":
/*!****************************************************************!*\
  !*** ./src/main/js/src/wrappers/testimonial/TestimonialOne.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-id-swiper */ "./node_modules/react-id-swiper/lib/index.js");
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_id_swiper__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _data_testimonial_testimonial_one_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../data/testimonial/testimonial-one.json */ "./src/main/js/src/data/testimonial/testimonial-one.json");
var _data_testimonial_testimonial_one_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/testimonial/testimonial-one.json */ "./src/main/js/src/data/testimonial/testimonial-one.json", 1);
/* harmony import */ var _components_testimonial_TestimonialOneSingle_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/testimonial/TestimonialOneSingle.js */ "./src/main/js/src/components/testimonial/TestimonialOneSingle.js");






var TestimonialOne = function TestimonialOne(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass,
      spaceLeftClass = _ref.spaceLeftClass,
      spaceRightClass = _ref.spaceRightClass,
      bgColorClass = _ref.bgColorClass;
  // swiper slider settings
  var settings = {
    slidesPerView: 1,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "testimonial-area ".concat(spaceTopClass ? spaceTopClass : "", "  ").concat(spaceBottomClass ? spaceBottomClass : "", " ").concat(spaceLeftClass ? spaceLeftClass : "", "  ").concat(spaceRightClass ? spaceRightClass : "", " ").concat(bgColorClass ? bgColorClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-lg-10 ml-auto mr-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "testimonial-active nav-style-1 nav-testi-style"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_id_swiper__WEBPACK_IMPORTED_MODULE_2___default.a, settings, _data_testimonial_testimonial_one_json__WEBPACK_IMPORTED_MODULE_3__ && _data_testimonial_testimonial_one_json__WEBPACK_IMPORTED_MODULE_3__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_testimonial_TestimonialOneSingle_js__WEBPACK_IMPORTED_MODULE_4__["default"], {
      data: single,
      key: key,
      sliderClass: "swiper-slide"
    });
  })))))));
};

TestimonialOne.propTypes = {
  bgColorClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceLeftClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceRightClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (TestimonialOne);

/***/ })

}]);
//# sourceMappingURL=33.bundle.js.map