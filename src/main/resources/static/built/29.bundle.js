(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[29],{

/***/ "./src/main/js/src/components/banner/BannerNineSingle.js":
/*!***************************************************************!*\
  !*** ./src/main/js/src/components/banner/BannerNineSingle.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");




var BannerNineSingle = function BannerNineSingle(_ref) {
  var data = _ref.data,
      spaceBottomClass = _ref.spaceBottomClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-lg-6 col-md-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "single-banner-2 ".concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    to: process.env.PUBLIC_URL + data.link
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + data.image,
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "banner-content-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", null, data.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, data.subtitle, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, data.price)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    to: process.env.PUBLIC_URL + data.link
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-long-arrow-right"
  })))));
};

BannerNineSingle.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (BannerNineSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/blog-featured/BlogFeaturedSingle.js":
/*!************************************************************************!*\
  !*** ./src/main/js/src/components/blog-featured/BlogFeaturedSingle.js ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");




var BlogFeaturedSingle = function BlogFeaturedSingle(_ref) {
  var singlePost = _ref.singlePost;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-lg-4 col-sm-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "blog-wrap mb-30 scroll-zoom"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "blog-img"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    to: process.env.PUBLIC_URL + singlePost.url
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + singlePost.image,
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "blog-category-names"
  }, singlePost.category.map(function (singleCategory, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "purple",
      key: key
    }, singleCategory);
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "blog-content-wrap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "blog-content text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    to: process.env.PUBLIC_URL + singlePost.url
  }, singlePost.title)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "By", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    to: process.env.PUBLIC_URL + singlePost.authorUrl
  }, singlePost.author))))));
};

BlogFeaturedSingle.propTypes = {
  singlePost: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object
};
/* harmony default export */ __webpack_exports__["default"] = (BlogFeaturedSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/brand-logo/BrandLogoOneSingle.js":
/*!*********************************************************************!*\
  !*** ./src/main/js/src/components/brand-logo/BrandLogoOneSingle.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



var BrandLogoOneSingle = function BrandLogoOneSingle(_ref) {
  var data = _ref.data,
      sliderClassName = _ref.sliderClassName,
      spaceBottomClass = _ref.spaceBottomClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "single-brand-logo ".concat(sliderClassName ? sliderClassName : "", " ").concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + data.image,
    alt: ""
  }));
};

BrandLogoOneSingle.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  sliderClassName: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (BrandLogoOneSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/feature-icon/FeatureIconThreeSingle.js":
/*!***************************************************************************!*\
  !*** ./src/main/js/src/components/feature-icon/FeatureIconThreeSingle.js ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



var FeatureIconThreeSingle = function FeatureIconThreeSingle(_ref) {
  var data = _ref.data,
      spaceBottomClass = _ref.spaceBottomClass,
      featureShapeClass = _ref.featureShapeClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-lg-4 col-md-4 col-sm-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "support-wrap-2 support-padding-2 text-center ".concat(featureShapeClass ? featureShapeClass : "", " ").concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "support-content-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    className: "animated",
    src: process.env.PUBLIC_URL + data.image,
    alt: ""
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, data.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, data.subtitle))));
};

FeatureIconThreeSingle.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  featureShapeClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (FeatureIconThreeSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/hero-slider/HeroSliderEightSingle.js":
/*!*************************************************************************!*\
  !*** ./src/main/js/src/components/hero-slider/HeroSliderEightSingle.js ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");




var HeroSliderEightSingle = function HeroSliderEightSingle(_ref) {
  var data = _ref.data,
      sliderClass = _ref.sliderClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "single-slider-2 slider-height-1 d-flex align-items-center slider-height-res hm-13-slider ".concat(sliderClass ? sliderClass : ""),
    style: {
      backgroundImage: "url(".concat(process.env.PUBLIC_URL + data.image, ")")
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-xl-12 col-lg-12 col-md-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-content-13 slider-animated-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", {
    className: "animated"
  }, data.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "animated",
    dangerouslySetInnerHTML: {
      __html: data.subtitle
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-btn btn-hover"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    className: "animated",
    to: process.env.PUBLIC_URL + data.url
  }, "SHOP NOW")))))));
};

HeroSliderEightSingle.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  sliderClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (HeroSliderEightSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/section-title/SectionTitle.js":
/*!******************************************************************!*\
  !*** ./src/main/js/src/components/section-title/SectionTitle.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



var SectionTitle = function SectionTitle(_ref) {
  var titleText = _ref.titleText,
      subtitleText = _ref.subtitleText,
      positionClass = _ref.positionClass,
      spaceClass = _ref.spaceClass,
      borderClass = _ref.borderClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "section-title ".concat(positionClass ? positionClass : "", " ").concat(spaceClass ? spaceClass : "", " ").concat(borderClass ? borderClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", null, titleText), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, subtitleText));
};

SectionTitle.propTypes = {
  borderClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  positionClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  subtitleText: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  titleText: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (SectionTitle);

/***/ }),

/***/ "./src/main/js/src/components/testimonial/TestimonialOneSingle.js":
/*!************************************************************************!*\
  !*** ./src/main/js/src/components/testimonial/TestimonialOneSingle.js ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



var TestimonialOneSingle = function TestimonialOneSingle(_ref) {
  var data = _ref.data,
      sliderClass = _ref.sliderClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "single-testimonial text-center ".concat(sliderClass ? sliderClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + data.image,
    alt: ""
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, data.content), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "client-info"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-map-signs"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, data.customerName), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, data.title)));
};

TestimonialOneSingle.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  sliderClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (TestimonialOneSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/data/banner/banner-nine.json":
/*!******************************************************!*\
  !*** ./src/main/js/src/data/banner/banner-nine.json ***!
  \******************************************************/
/*! exports provided: 0, 1, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"image\":\"/assets/img/banner/banner-16.png\",\"title\":\"Eye Lashes\",\"subtitle\":\"Starting at\",\"price\":\"$9.00\",\"link\":\"/shop-grid-standard\"},{\"id\":2,\"image\":\"/assets/img/banner/banner-17.png\",\"title\":\"Anti-Wrinkles\",\"subtitle\":\"Starting at\",\"price\":\"$7.00\",\"link\":\"/shop-grid-standard\"}]");

/***/ }),

/***/ "./src/main/js/src/data/blog-featured/blog-featured.json":
/*!***************************************************************!*\
  !*** ./src/main/js/src/data/blog-featured/blog-featured.json ***!
  \***************************************************************/
/*! exports provided: 0, 1, 2, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"image\":\"/assets/img/blog/blog-1.jpg\",\"category\":[\"lifestyle\",\"men\"],\"title\":\"A guide to latest trends\",\"url\":\"/blog-details-standard\",\"author\":\"Admin\",\"authorUrl\":\"/blog-standard\"},{\"id\":2,\"image\":\"/assets/img/blog/blog-2.jpg\",\"category\":[\"lifestyle\"],\"title\":\"Five ways to lead a happy life\",\"url\":\"/blog-details-standard\",\"author\":\"Admin\",\"authorUrl\":\"/blog-standard\"},{\"id\":3,\"image\":\"/assets/img/blog/blog-3.jpg\",\"category\":[\"lifestyle\"],\"title\":\"Tips on having a happy life\",\"url\":\"/blog-details-standard\",\"author\":\"Admin\",\"authorUrl\":\"/blog-standard\"}]");

/***/ }),

/***/ "./src/main/js/src/data/brand-logos/brand-logo-one.json":
/*!**************************************************************!*\
  !*** ./src/main/js/src/data/brand-logos/brand-logo-one.json ***!
  \**************************************************************/
/*! exports provided: 0, 1, 2, 3, 4, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":\"1\",\"image\":\"/assets/img/brand-logo/brand-logo-1.png\"},{\"id\":\"2\",\"image\":\"/assets/img/brand-logo/brand-logo-2.png\"},{\"id\":\"3\",\"image\":\"/assets/img/brand-logo/brand-logo-3.png\"},{\"id\":\"4\",\"image\":\"/assets/img/brand-logo/brand-logo-4.png\"},{\"id\":\"5\",\"image\":\"/assets/img/brand-logo/brand-logo-5.png\"}]");

/***/ }),

/***/ "./src/main/js/src/data/feature-icons/feature-icon-three.json":
/*!********************************************************************!*\
  !*** ./src/main/js/src/data/feature-icons/feature-icon-three.json ***!
  \********************************************************************/
/*! exports provided: 0, 1, 2, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"image\":\"/assets/img/icon-img/support-1.png\",\"title\":\"Free Shipping\",\"subtitle\":\"Free shipping on all order\"},{\"id\":2,\"image\":\"/assets/img/icon-img/support-2.png\",\"title\":\"Support 24/7\",\"subtitle\":\"Free shipping on all order\"},{\"id\":3,\"image\":\"/assets/img/icon-img/support-3.png\",\"title\":\"Money Return\",\"subtitle\":\"Free shipping on all order\"}]");

/***/ }),

/***/ "./src/main/js/src/data/hero-sliders/hero-slider-eight.json":
/*!******************************************************************!*\
  !*** ./src/main/js/src/data/hero-sliders/hero-slider-eight.json ***!
  \******************************************************************/
/*! exports provided: 0, 1, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"title\":\"Cosmetic Best Selling\",\"subtitle\":\"Fashion <br /> Cosmetic\",\"image\":\"/assets/img/slider/hm13-slider-1.jpg\",\"url\":\"/shop-grid-standard\"},{\"id\":2,\"title\":\"Cosmetic Best Selling\",\"subtitle\":\"Fashion  <br /> Cosmetic\",\"image\":\"/assets/img/slider/hm13-slider-1.jpg\",\"url\":\"/shop-grid-standard\"}]");

/***/ }),

/***/ "./src/main/js/src/data/testimonial/testimonial-one.json":
/*!***************************************************************!*\
  !*** ./src/main/js/src/data/testimonial/testimonial-one.json ***!
  \***************************************************************/
/*! exports provided: 0, 1, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":\"1\",\"image\":\"/assets/img/testimonial/1.jpg\",\"content\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\",\"customerName\":\"Grace Alvarado\",\"title\":\"customer\"},{\"id\":\"2\",\"image\":\"/assets/img/testimonial/2.jpg\",\"content\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\",\"customerName\":\"John Doe\",\"title\":\"customer\"}]");

/***/ }),

/***/ "./src/main/js/src/layouts/LayoutOne.js":
/*!**********************************************!*\
  !*** ./src/main/js/src/layouts/LayoutOne.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wrappers_header_HeaderOne__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../wrappers/header/HeaderOne */ "./src/main/js/src/wrappers/header/HeaderOne.js");
/* harmony import */ var _wrappers_footer_FooterOne__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../wrappers/footer/FooterOne */ "./src/main/js/src/wrappers/footer/FooterOne.js");





var LayoutOne = function LayoutOne(_ref) {
  var children = _ref.children,
      headerContainerClass = _ref.headerContainerClass,
      headerTop = _ref.headerTop,
      headerPaddingClass = _ref.headerPaddingClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_header_HeaderOne__WEBPACK_IMPORTED_MODULE_2__["default"], {
    layout: headerContainerClass,
    top: headerTop,
    headerPaddingClass: headerPaddingClass
  }), children, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_footer_FooterOne__WEBPACK_IMPORTED_MODULE_3__["default"], {
    backgroundColorClass: "bg-gray",
    spaceTopClass: "pt-100",
    spaceBottomClass: "pb-70"
  }));
};

LayoutOne.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.any,
  headerContainerClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  headerPaddingClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  headerTop: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (LayoutOne);

/***/ }),

/***/ "./src/main/js/src/pages/home/HomeCosmetics.js":
/*!*****************************************************!*\
  !*** ./src/main/js/src/pages/home/HomeCosmetics.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-meta-tags */ "./node_modules/react-meta-tags/lib/index.js");
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_meta_tags__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_LayoutOne__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../layouts/LayoutOne */ "./src/main/js/src/layouts/LayoutOne.js");
/* harmony import */ var _wrappers_testimonial_TestimonialOne__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../wrappers/testimonial/TestimonialOne */ "./src/main/js/src/wrappers/testimonial/TestimonialOne.js");
/* harmony import */ var _wrappers_brand_logo_BrandLogoSliderOne__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../wrappers/brand-logo/BrandLogoSliderOne */ "./src/main/js/src/wrappers/brand-logo/BrandLogoSliderOne.js");
/* harmony import */ var _wrappers_blog_featured_BlogFeatured__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../wrappers/blog-featured/BlogFeatured */ "./src/main/js/src/wrappers/blog-featured/BlogFeatured.js");
/* harmony import */ var _wrappers_hero_slider_HeroSliderEight__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../wrappers/hero-slider/HeroSliderEight */ "./src/main/js/src/wrappers/hero-slider/HeroSliderEight.js");
/* harmony import */ var _wrappers_feature_icon_FeatureIconThree__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../wrappers/feature-icon/FeatureIconThree */ "./src/main/js/src/wrappers/feature-icon/FeatureIconThree.js");
/* harmony import */ var _wrappers_banner_BannerNine__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../wrappers/banner/BannerNine */ "./src/main/js/src/wrappers/banner/BannerNine.js");
/* harmony import */ var _wrappers_product_TabProductFive__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../wrappers/product/TabProductFive */ "./src/main/js/src/wrappers/product/TabProductFive.js");











var HomeCosmetics = function HomeCosmetics() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_meta_tags__WEBPACK_IMPORTED_MODULE_1___default.a, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", null, "Flone | Cosmetics Home"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: "Cosmetics home of flone react minimalist eCommerce template."
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_LayoutOne__WEBPACK_IMPORTED_MODULE_2__["default"], {
    headerContainerClass: "container-fluid",
    headerPaddingClass: "header-padding-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_hero_slider_HeroSliderEight__WEBPACK_IMPORTED_MODULE_6__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_product_TabProductFive__WEBPACK_IMPORTED_MODULE_9__["default"], {
    spaceTopClass: "pt-95",
    spaceBottomClass: "pb-70",
    category: "cosmetics"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_feature_icon_FeatureIconThree__WEBPACK_IMPORTED_MODULE_7__["default"], {
    spaceBottomClass: "pb-70",
    featureShapeClass: "support-shape-3"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_testimonial_TestimonialOne__WEBPACK_IMPORTED_MODULE_3__["default"], {
    spaceBottomClass: "pb-95"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_banner_BannerNine__WEBPACK_IMPORTED_MODULE_8__["default"], {
    spaceBottomClass: "pb-70"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_brand_logo_BrandLogoSliderOne__WEBPACK_IMPORTED_MODULE_4__["default"], {
    spaceBottomClass: "pb-95"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_blog_featured_BlogFeatured__WEBPACK_IMPORTED_MODULE_5__["default"], {
    spaceBottomClass: "pb-55"
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (HomeCosmetics);

/***/ }),

/***/ "./src/main/js/src/wrappers/banner/BannerNine.js":
/*!*******************************************************!*\
  !*** ./src/main/js/src/wrappers/banner/BannerNine.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _data_banner_banner_nine_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../data/banner/banner-nine.json */ "./src/main/js/src/data/banner/banner-nine.json");
var _data_banner_banner_nine_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/banner/banner-nine.json */ "./src/main/js/src/data/banner/banner-nine.json", 1);
/* harmony import */ var _components_banner_BannerNineSingle_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/banner/BannerNineSingle.js */ "./src/main/js/src/components/banner/BannerNineSingle.js");





var BannerNine = function BannerNine(_ref) {
  var spaceBottomClass = _ref.spaceBottomClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "banner-area ".concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, _data_banner_banner_nine_json__WEBPACK_IMPORTED_MODULE_2__ && _data_banner_banner_nine_json__WEBPACK_IMPORTED_MODULE_2__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_banner_BannerNineSingle_js__WEBPACK_IMPORTED_MODULE_3__["default"], {
      data: single,
      key: key,
      spaceBottomClass: "mb-30"
    });
  }))));
};

BannerNine.propTypes = {
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (BannerNine);

/***/ }),

/***/ "./src/main/js/src/wrappers/blog-featured/BlogFeatured.js":
/*!****************************************************************!*\
  !*** ./src/main/js/src/wrappers/blog-featured/BlogFeatured.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _data_blog_featured_blog_featured_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../data/blog-featured/blog-featured.json */ "./src/main/js/src/data/blog-featured/blog-featured.json");
var _data_blog_featured_blog_featured_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/blog-featured/blog-featured.json */ "./src/main/js/src/data/blog-featured/blog-featured.json", 1);
/* harmony import */ var _components_blog_featured_BlogFeaturedSingle__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/blog-featured/BlogFeaturedSingle */ "./src/main/js/src/components/blog-featured/BlogFeaturedSingle.js");
/* harmony import */ var _components_section_title_SectionTitle__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/section-title/SectionTitle */ "./src/main/js/src/components/section-title/SectionTitle.js");






var BlogFeatured = function BlogFeatured(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "blog-area ".concat(spaceTopClass ? spaceTopClass : "", " ").concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_section_title_SectionTitle__WEBPACK_IMPORTED_MODULE_4__["default"], {
    titleText: "OUR BLOG",
    positionClass: "text-center",
    spaceClass: "mb-55"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, _data_blog_featured_blog_featured_json__WEBPACK_IMPORTED_MODULE_2__.map(function (singlePost) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_blog_featured_BlogFeaturedSingle__WEBPACK_IMPORTED_MODULE_3__["default"], {
      singlePost: singlePost,
      key: singlePost.id
    });
  }))));
};

BlogFeatured.propTypes = {
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (BlogFeatured);

/***/ }),

/***/ "./src/main/js/src/wrappers/brand-logo/BrandLogoSliderOne.js":
/*!*******************************************************************!*\
  !*** ./src/main/js/src/wrappers/brand-logo/BrandLogoSliderOne.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-id-swiper */ "./node_modules/react-id-swiper/lib/index.js");
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_id_swiper__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_brand_logo_BrandLogoOneSingle__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/brand-logo/BrandLogoOneSingle */ "./src/main/js/src/components/brand-logo/BrandLogoOneSingle.js");
/* harmony import */ var _data_brand_logos_brand_logo_one_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../data/brand-logos/brand-logo-one.json */ "./src/main/js/src/data/brand-logos/brand-logo-one.json");
var _data_brand_logos_brand_logo_one_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/brand-logos/brand-logo-one.json */ "./src/main/js/src/data/brand-logos/brand-logo-one.json", 1);






var BrandLogoSliderOne = function BrandLogoSliderOne(_ref) {
  var spaceBottomClass = _ref.spaceBottomClass,
      spaceTopClass = _ref.spaceTopClass;
  var settings = {
    loop: true,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false
    },
    grabCursor: true,
    breakpoints: {
      1024: {
        slidesPerView: 5
      },
      768: {
        slidesPerView: 4
      },
      640: {
        slidesPerView: 3
      },
      320: {
        slidesPerView: 2
      }
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "brand-logo-area ".concat(spaceBottomClass ? spaceBottomClass : "", "  ").concat(spaceTopClass ? spaceTopClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "brand-logo-active"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_id_swiper__WEBPACK_IMPORTED_MODULE_2___default.a, settings, _data_brand_logos_brand_logo_one_json__WEBPACK_IMPORTED_MODULE_4__ && _data_brand_logos_brand_logo_one_json__WEBPACK_IMPORTED_MODULE_4__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_brand_logo_BrandLogoOneSingle__WEBPACK_IMPORTED_MODULE_3__["default"], {
      data: single,
      key: key,
      sliderClassName: "swiper-slide",
      spaceBottomClass: "mb-30"
    });
  })))));
};

BrandLogoSliderOne.propTypes = {
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (BrandLogoSliderOne);

/***/ }),

/***/ "./src/main/js/src/wrappers/feature-icon/FeatureIconThree.js":
/*!*******************************************************************!*\
  !*** ./src/main/js/src/wrappers/feature-icon/FeatureIconThree.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _data_feature_icons_feature_icon_three_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../data/feature-icons/feature-icon-three.json */ "./src/main/js/src/data/feature-icons/feature-icon-three.json");
var _data_feature_icons_feature_icon_three_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/feature-icons/feature-icon-three.json */ "./src/main/js/src/data/feature-icons/feature-icon-three.json", 1);
/* harmony import */ var _components_feature_icon_FeatureIconThreeSingle_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/feature-icon/FeatureIconThreeSingle.js */ "./src/main/js/src/components/feature-icon/FeatureIconThreeSingle.js");





var FeatureIconThree = function FeatureIconThree(_ref) {
  var bgColorClass = _ref.bgColorClass,
      spaceBottomClass = _ref.spaceBottomClass,
      featureShapeClass = _ref.featureShapeClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "support-area ".concat(bgColorClass ? bgColorClass : "", " ").concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, _data_feature_icons_feature_icon_three_json__WEBPACK_IMPORTED_MODULE_2__ && _data_feature_icons_feature_icon_three_json__WEBPACK_IMPORTED_MODULE_2__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_feature_icon_FeatureIconThreeSingle_js__WEBPACK_IMPORTED_MODULE_3__["default"], {
      data: single,
      spaceBottomClass: "mb-30",
      key: key,
      featureShapeClass: featureShapeClass
    });
  }))));
};

FeatureIconThree.propTypes = {
  bgColorClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  featureShapeClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (FeatureIconThree);

/***/ }),

/***/ "./src/main/js/src/wrappers/hero-slider/HeroSliderEight.js":
/*!*****************************************************************!*\
  !*** ./src/main/js/src/wrappers/hero-slider/HeroSliderEight.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-id-swiper */ "./node_modules/react-id-swiper/lib/index.js");
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_id_swiper__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_hero_slider_HeroSliderEightSingle_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/hero-slider/HeroSliderEightSingle.js */ "./src/main/js/src/components/hero-slider/HeroSliderEightSingle.js");
/* harmony import */ var _data_hero_sliders_hero_slider_eight_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../data/hero-sliders/hero-slider-eight.json */ "./src/main/js/src/data/hero-sliders/hero-slider-eight.json");
var _data_hero_sliders_hero_slider_eight_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/hero-sliders/hero-slider-eight.json */ "./src/main/js/src/data/hero-sliders/hero-slider-eight.json", 1);





var HeroSliderEight = function HeroSliderEight() {
  var params = {
    effect: "fade",
    loop: true,
    speed: 1000,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    watchSlidesVisibility: true,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    renderPrevButton: function renderPrevButton() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "swiper-button-prev ht-swiper-button-nav"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "pe-7s-angle-left"
      }));
    },
    renderNextButton: function renderNextButton() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        className: "swiper-button-next ht-swiper-button-nav"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "pe-7s-angle-right"
      }));
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "slider-area"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "slider-active nav-style-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_id_swiper__WEBPACK_IMPORTED_MODULE_1___default.a, params, _data_hero_sliders_hero_slider_eight_json__WEBPACK_IMPORTED_MODULE_3__ && _data_hero_sliders_hero_slider_eight_json__WEBPACK_IMPORTED_MODULE_3__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_hero_slider_HeroSliderEightSingle_js__WEBPACK_IMPORTED_MODULE_2__["default"], {
      data: single,
      key: key,
      sliderClass: "swiper-slide"
    });
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (HeroSliderEight);

/***/ }),

/***/ "./src/main/js/src/wrappers/product/TabProductFive.js":
/*!************************************************************!*\
  !*** ./src/main/js/src/wrappers/product/TabProductFive.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/Tab */ "./node_modules/react-bootstrap/esm/Tab.js");
/* harmony import */ var react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap/Nav */ "./node_modules/react-bootstrap/esm/Nav.js");
/* harmony import */ var _ProductGridTwo__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ProductGridTwo */ "./src/main/js/src/wrappers/product/ProductGridTwo.js");







var TabProductFive = function TabProductFive(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass,
      category = _ref.category,
      productTabClass = _ref.productTabClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "product-area ".concat(spaceTopClass ? spaceTopClass : "", " ").concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Container, {
    defaultActiveKey: "bestSeller"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"], {
    variant: "pills",
    className: "product-tab-list-2 mb-60 ".concat(productTabClass ? productTabClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Link, {
    eventKey: "newArrival"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, "New Arrivals"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Link, {
    eventKey: "bestSeller"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, "Best Sellers"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Link, {
    eventKey: "saleItems"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, "Sale Items")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Content, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
    eventKey: "newArrival"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductGridTwo__WEBPACK_IMPORTED_MODULE_5__["default"], {
    category: category,
    type: "new",
    limit: 8,
    spaceBottomClass: "mb-25"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
    eventKey: "bestSeller"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductGridTwo__WEBPACK_IMPORTED_MODULE_5__["default"], {
    category: category,
    type: "bestSeller",
    limit: 8,
    spaceBottomClass: "mb-25"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
    eventKey: "saleItems"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductGridTwo__WEBPACK_IMPORTED_MODULE_5__["default"], {
    category: category,
    type: "saleItems",
    limit: 8,
    spaceBottomClass: "mb-25"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "view-more text-center mt-20 toggle-btn6 col-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    className: "loadMore6",
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, "VIEW MORE PRODUCTS"))));
};

TabProductFive.propTypes = {
  category: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  productTabClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (TabProductFive);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/wrappers/testimonial/TestimonialOne.js":
/*!****************************************************************!*\
  !*** ./src/main/js/src/wrappers/testimonial/TestimonialOne.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-id-swiper */ "./node_modules/react-id-swiper/lib/index.js");
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_id_swiper__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _data_testimonial_testimonial_one_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../data/testimonial/testimonial-one.json */ "./src/main/js/src/data/testimonial/testimonial-one.json");
var _data_testimonial_testimonial_one_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/testimonial/testimonial-one.json */ "./src/main/js/src/data/testimonial/testimonial-one.json", 1);
/* harmony import */ var _components_testimonial_TestimonialOneSingle_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/testimonial/TestimonialOneSingle.js */ "./src/main/js/src/components/testimonial/TestimonialOneSingle.js");






var TestimonialOne = function TestimonialOne(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass,
      spaceLeftClass = _ref.spaceLeftClass,
      spaceRightClass = _ref.spaceRightClass,
      bgColorClass = _ref.bgColorClass;
  // swiper slider settings
  var settings = {
    slidesPerView: 1,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "testimonial-area ".concat(spaceTopClass ? spaceTopClass : "", "  ").concat(spaceBottomClass ? spaceBottomClass : "", " ").concat(spaceLeftClass ? spaceLeftClass : "", "  ").concat(spaceRightClass ? spaceRightClass : "", " ").concat(bgColorClass ? bgColorClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-lg-10 ml-auto mr-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "testimonial-active nav-style-1 nav-testi-style"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_id_swiper__WEBPACK_IMPORTED_MODULE_2___default.a, settings, _data_testimonial_testimonial_one_json__WEBPACK_IMPORTED_MODULE_3__ && _data_testimonial_testimonial_one_json__WEBPACK_IMPORTED_MODULE_3__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_testimonial_TestimonialOneSingle_js__WEBPACK_IMPORTED_MODULE_4__["default"], {
      data: single,
      key: key,
      sliderClass: "swiper-slide"
    });
  })))))));
};

TestimonialOne.propTypes = {
  bgColorClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceLeftClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceRightClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (TestimonialOne);

/***/ })

}]);
//# sourceMappingURL=29.bundle.js.map