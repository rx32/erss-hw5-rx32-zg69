(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[42],{

/***/ "./src/main/js/src/components/countdown/Renderer.js":
/*!**********************************************************!*\
  !*** ./src/main/js/src/components/countdown/Renderer.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



var Renderer = function Renderer(_ref) {
  var days = _ref.days,
      hours = _ref.hours,
      minutes = _ref.minutes,
      seconds = _ref.seconds;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "timer timer-style"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "cdown day"
  }, days, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Days")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "cdown hour"
  }, hours, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Hours")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "cdown minutes"
  }, minutes, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Minutes")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, seconds, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Secs"))));
};

Renderer.propTypes = {
  days: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.number,
  hours: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.number,
  minutes: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.number,
  seconds: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.number
};
/* harmony default export */ __webpack_exports__["default"] = (Renderer);

/***/ }),

/***/ "./src/main/js/src/components/feature-icon/FeatureIconFourSingle.js":
/*!**************************************************************************!*\
  !*** ./src/main/js/src/components/feature-icon/FeatureIconFourSingle.js ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



var FeatureIconFourSingle = function FeatureIconFourSingle(_ref) {
  var data = _ref.data,
      spaceBottomClass = _ref.spaceBottomClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-lg-4 col-md-6 col-sm-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "support-wrap-3 text-center ".concat(spaceBottomClass ? spaceBottomClass : ""),
    style: {
      backgroundColor: "".concat(data.backgroundColor)
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "support-icon-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    className: "animated",
    src: process.env.PUBLIC_URL + data.iconImage,
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "support-content-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + data.titleImage,
    alt: ""
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, data.title))));
};

FeatureIconFourSingle.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (FeatureIconFourSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/hero-slider/HeroSliderFiveSingle.js":
/*!************************************************************************!*\
  !*** ./src/main/js/src/components/hero-slider/HeroSliderFiveSingle.js ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");




var HeroSliderFiveSingle = function HeroSliderFiveSingle(_ref) {
  var data = _ref.data,
      sliderClass = _ref.sliderClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "single-slider-2 slider-height-1 slider-height-res15 d-flex align-items-center slider-height-res bg-img ".concat(sliderClass ? sliderClass : ""),
    style: {
      backgroundImage: "url(".concat(process.env.PUBLIC_URL + data.image, ")")
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-xl-6 col-lg-6 col-md-7 ml-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-content-2 slider-content-fruits slider-animated-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", {
    className: "animated"
  }, data.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "animated"
  }, data.subtitle), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-btn btn-hover"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    className: "animated",
    to: process.env.PUBLIC_URL + data.url
  }, "SHOP NOW")))))));
};

HeroSliderFiveSingle.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  sliderClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (HeroSliderFiveSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/components/section-title/SectionTitleThree.js":
/*!***********************************************************************!*\
  !*** ./src/main/js/src/components/section-title/SectionTitleThree.js ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



var SectionTitleThree = function SectionTitleThree(_ref) {
  var titleText = _ref.titleText,
      positionClass = _ref.positionClass,
      spaceClass = _ref.spaceClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "section-title-5 ".concat(positionClass ? positionClass : "", " ").concat(spaceClass ? spaceClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", null, titleText));
};

SectionTitleThree.propTypes = {
  positionClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  titleText: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (SectionTitleThree);

/***/ }),

/***/ "./src/main/js/src/components/testimonial/TestimonialOneSingle.js":
/*!************************************************************************!*\
  !*** ./src/main/js/src/components/testimonial/TestimonialOneSingle.js ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



var TestimonialOneSingle = function TestimonialOneSingle(_ref) {
  var data = _ref.data,
      sliderClass = _ref.sliderClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "single-testimonial text-center ".concat(sliderClass ? sliderClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + data.image,
    alt: ""
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, data.content), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "client-info"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
    className: "fa fa-map-signs"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, data.customerName), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, data.title)));
};

TestimonialOneSingle.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.object,
  sliderClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (TestimonialOneSingle);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/data/feature-icons/feature-icon-four.json":
/*!*******************************************************************!*\
  !*** ./src/main/js/src/data/feature-icons/feature-icon-four.json ***!
  \*******************************************************************/
/*! exports provided: 0, 1, 2, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"titleImage\":\"/assets/img/icon-img/support-8.png\",\"title\":\"Free shipping on all order\",\"iconImage\":\"/assets/img/icon-img/support-5.png\",\"backgroundColor\":\"#ccfbe9\"},{\"id\":2,\"titleImage\":\"/assets/img/icon-img/support-9.png\",\"title\":\"Back guarantee under 5 days\",\"iconImage\":\"/assets/img/icon-img/support-6.png\",\"backgroundColor\":\"#f2fbcc\"},{\"id\":3,\"titleImage\":\"/assets/img/icon-img/support-10.png\",\"title\":\"On every order over $150\",\"iconImage\":\"/assets/img/icon-img/support-7.png\",\"backgroundColor\":\"#ddfbcc\"}]");

/***/ }),

/***/ "./src/main/js/src/data/hero-sliders/hero-slider-five.json":
/*!*****************************************************************!*\
  !*** ./src/main/js/src/data/hero-sliders/hero-slider-five.json ***!
  \*****************************************************************/
/*! exports provided: 0, 1, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"title\":\"Natural & Healthy\",\"subtitle\":\"100% Organic Fruits Collection\",\"image\":\"/assets/img/slider/slider-8.jpg\",\"url\":\"/shop-grid-standard\"},{\"id\":2,\"title\":\"Natural & Healthy\",\"subtitle\":\"100% Organic Veg Collection\",\"image\":\"/assets/img/slider/slider-8-2.jpg\",\"url\":\"/shop-grid-standard\"}]");

/***/ }),

/***/ "./src/main/js/src/data/testimonial/testimonial-one.json":
/*!***************************************************************!*\
  !*** ./src/main/js/src/data/testimonial/testimonial-one.json ***!
  \***************************************************************/
/*! exports provided: 0, 1, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":\"1\",\"image\":\"/assets/img/testimonial/1.jpg\",\"content\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\",\"customerName\":\"Grace Alvarado\",\"title\":\"customer\"},{\"id\":\"2\",\"image\":\"/assets/img/testimonial/2.jpg\",\"content\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\",\"customerName\":\"John Doe\",\"title\":\"customer\"}]");

/***/ }),

/***/ "./src/main/js/src/layouts/LayoutThree.js":
/*!************************************************!*\
  !*** ./src/main/js/src/layouts/LayoutThree.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wrappers_header_HeaderOne__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../wrappers/header/HeaderOne */ "./src/main/js/src/wrappers/header/HeaderOne.js");
/* harmony import */ var _wrappers_footer_FooterTwo__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../wrappers/footer/FooterTwo */ "./src/main/js/src/wrappers/footer/FooterTwo.js");





var LayoutThree = function LayoutThree(_ref) {
  var children = _ref.children,
      headerContainerClass = _ref.headerContainerClass,
      headerTop = _ref.headerTop,
      headerBorderStyle = _ref.headerBorderStyle,
      headerPaddingClass = _ref.headerPaddingClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_header_HeaderOne__WEBPACK_IMPORTED_MODULE_2__["default"], {
    layout: headerContainerClass,
    top: headerTop,
    borderStyle: headerBorderStyle,
    headerPaddingClass: headerPaddingClass
  }), children, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wrappers_footer_FooterTwo__WEBPACK_IMPORTED_MODULE_3__["default"], {
    backgroundColorClass: "footer-white",
    spaceLeftClass: "ml-70",
    spaceRightClass: "mr-70",
    footerTopBackgroundColorClass: "bg-gray-2",
    footerTopSpaceTopClass: "pt-80",
    footerTopSpaceBottomClass: "pb-60",
    copyrightColorClass: "copyright-gray",
    footerLogo: "/assets/img/logo/logo.png"
  }));
};

LayoutThree.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.any,
  headerBorderStyle: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  headerContainerClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  headerPaddingClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  headerTop: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (LayoutThree);

/***/ }),

/***/ "./src/main/js/src/pages/home/HomeOrganicFood.js":
/*!*******************************************************!*\
  !*** ./src/main/js/src/pages/home/HomeOrganicFood.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-meta-tags */ "./node_modules/react-meta-tags/lib/index.js");
/* harmony import */ var react_meta_tags__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_meta_tags__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_LayoutThree__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../layouts/LayoutThree */ "./src/main/js/src/layouts/LayoutThree.js");
/* harmony import */ var _wrappers_hero_slider_HeroSliderFive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../wrappers/hero-slider/HeroSliderFive */ "./src/main/js/src/wrappers/hero-slider/HeroSliderFive.js");
/* harmony import */ var _wrappers_feature_icon_FeatureIconFour__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../wrappers/feature-icon/FeatureIconFour */ "./src/main/js/src/wrappers/feature-icon/FeatureIconFour.js");
/* harmony import */ var _wrappers_product_TabProductFour__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../wrappers/product/TabProductFour */ "./src/main/js/src/wrappers/product/TabProductFour.js");
/* harmony import */ var _wrappers_banner_BannerFive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../wrappers/banner/BannerFive */ "./src/main/js/src/wrappers/banner/BannerFive.js");
/* harmony import */ var _wrappers_countdown_CountDownTwo__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../wrappers/countdown/CountDownTwo */ "./src/main/js/src/wrappers/countdown/CountDownTwo.js");
/* harmony import */ var _wrappers_testimonial_TestimonialOne__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../wrappers/testimonial/TestimonialOne */ "./src/main/js/src/wrappers/testimonial/TestimonialOne.js");
/* harmony import */ var _wrappers_newsletter_NewsletterTwo__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../wrappers/newsletter/NewsletterTwo */ "./src/main/js/src/wrappers/newsletter/NewsletterTwo.js");











var HomeOrganicFood = function HomeOrganicFood() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_meta_tags__WEBPACK_IMPORTED_MODULE_1___default.a, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", null, "Flone | Organic Food Home"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: "Organic food home of flone react minimalist eCommerce template."
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_LayoutThree__WEBPACK_IMPORTED_MODULE_2__["default"], {
    headerTop: "visible",
    headerContainerClass: "container-fluid",
    headerBorderStyle: "fluid-border",
    headerPaddingClass: "header-padding-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_hero_slider_HeroSliderFive__WEBPACK_IMPORTED_MODULE_3__["default"], {
    spaceLeftClass: "ml-70",
    spaceRightClass: "mr-70"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_feature_icon_FeatureIconFour__WEBPACK_IMPORTED_MODULE_4__["default"], {
    spaceTopClass: "pt-10",
    spaceBottomClass: "pb-90",
    containerClass: "container-fluid",
    gutterClass: "padding-10-row-col"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_product_TabProductFour__WEBPACK_IMPORTED_MODULE_5__["default"], {
    spaceBottomClass: "pb-100",
    category: "organic food",
    productTabClass: "product-tab-fruits"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_banner_BannerFive__WEBPACK_IMPORTED_MODULE_6__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_countdown_CountDownTwo__WEBPACK_IMPORTED_MODULE_7__["default"], {
    spaceTopClass: "pt-80",
    spaceBottomClass: "pb-95",
    dateTime: "November 13, 2020 12:12:00"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_testimonial_TestimonialOne__WEBPACK_IMPORTED_MODULE_8__["default"], {
    spaceTopClass: "pt-100",
    spaceBottomClass: "pb-95",
    spaceLeftClass: "ml-70",
    spaceRightClass: "mr-70",
    bgColorClass: "bg-gray-3"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_wrappers_newsletter_NewsletterTwo__WEBPACK_IMPORTED_MODULE_9__["default"], {
    spaceTopClass: "pt-100",
    spaceBottomClass: "pb-100",
    subscribeBtnClass: "green-subscribe"
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (HomeOrganicFood);

/***/ }),

/***/ "./src/main/js/src/wrappers/banner/BannerFive.js":
/*!*******************************************************!*\
  !*** ./src/main/js/src/wrappers/banner/BannerFive.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");



var BannerFive = function BannerFive() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "banner-area hm9-section-padding"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container-fluid"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-lg-4 col-md-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-lg-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "single-banner mb-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + "/assets/img/banner/banner-21.png",
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "banner-content-3 banner-position-hm15-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", null, "Green Apple "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Starting At ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "$99.00")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-long-arrow-right"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-lg-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "single-banner mb-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + "/assets/img/banner/banner-22.png",
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "banner-content-3 banner-position-hm15-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", null, "Ripe orange"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Starting At ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "$99.00")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-long-arrow-right"
  }))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-lg-4 col-md-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "single-banner mb-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + "/assets/img/banner/banner-23.png",
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "banner-content-4 banner-position-hm15-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "-20% Off"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", null, "New Fruits"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", null, "Best for your health"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, "SHOP NOW")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-lg-4 col-md-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-lg-12 col-md-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "single-banner mb-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + "/assets/img/banner/banner-24.png",
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "banner-content-3 banner-position-hm15-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", null, "Ripe Corn "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Starting At ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "$99.00")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-long-arrow-right"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-lg-12 col-md-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "single-banner mb-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + "/assets/img/banner/banner-25.png",
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "banner-content-3 banner-position-hm15-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", null, "Green guava "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Starting At ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "$99.00")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-long-arrow-right"
  }))))))))));
};

/* harmony default export */ __webpack_exports__["default"] = (BannerFive);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/wrappers/countdown/CountDownTwo.js":
/*!************************************************************!*\
  !*** ./src/main/js/src/wrappers/countdown/CountDownTwo.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var react_countdown_now__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-countdown-now */ "./node_modules/react-countdown-now/dist/index.es.js");
/* harmony import */ var _components_countdown_Renderer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/countdown/Renderer */ "./src/main/js/src/components/countdown/Renderer.js");






var CountDownTwo = function CountDownTwo(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass,
      dateTime = _ref.dateTime;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "funfact-area ".concat(spaceTopClass ? spaceTopClass : "", " ").concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row align-items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-md-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "funfact-content funfact-res text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", null, "Deal of the day"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "timer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_countdown_now__WEBPACK_IMPORTED_MODULE_3__["default"], {
    date: new Date(dateTime),
    renderer: _components_countdown_Renderer__WEBPACK_IMPORTED_MODULE_4__["default"]
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "funfact-btn funfact-btn-green btn-hover"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, "SHOP NOW")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-md-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "fruits-deal-img"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: process.env.PUBLIC_URL + "/assets/img/banner/deal.png",
    alt: ""
  })))))));
};

CountDownTwo.propTypes = {
  dateTime: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (CountDownTwo);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/wrappers/feature-icon/FeatureIconFour.js":
/*!******************************************************************!*\
  !*** ./src/main/js/src/wrappers/feature-icon/FeatureIconFour.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _data_feature_icons_feature_icon_four_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../data/feature-icons/feature-icon-four.json */ "./src/main/js/src/data/feature-icons/feature-icon-four.json");
var _data_feature_icons_feature_icon_four_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/feature-icons/feature-icon-four.json */ "./src/main/js/src/data/feature-icons/feature-icon-four.json", 1);
/* harmony import */ var _components_feature_icon_FeatureIconFourSingle_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/feature-icon/FeatureIconFourSingle.js */ "./src/main/js/src/components/feature-icon/FeatureIconFourSingle.js");





var FeatureIconFour = function FeatureIconFour(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass,
      containerClass = _ref.containerClass,
      gutterClass = _ref.gutterClass,
      responsiveClass = _ref.responsiveClass,
      bgImg = _ref.bgImg;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "support-area hm9-section-padding ".concat(spaceTopClass ? spaceTopClass : "", " ").concat(spaceBottomClass ? spaceBottomClass : "", " ").concat(responsiveClass ? responsiveClass : ""),
    style: bgImg ? {
      backgroundImage: "url(".concat(process.env.PUBLIC_URL + bgImg, ")")
    } : {}
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "".concat(containerClass ? containerClass : "", " ").concat(gutterClass ? gutterClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, _data_feature_icons_feature_icon_four_json__WEBPACK_IMPORTED_MODULE_2__ && _data_feature_icons_feature_icon_four_json__WEBPACK_IMPORTED_MODULE_2__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_feature_icon_FeatureIconFourSingle_js__WEBPACK_IMPORTED_MODULE_3__["default"], {
      data: single,
      spaceBottomClass: "mb-10",
      key: key
    });
  }))));
};

FeatureIconFour.propTypes = {
  bgImg: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  containerClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  gutterClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  responsiveClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (FeatureIconFour);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/wrappers/hero-slider/HeroSliderFive.js":
/*!****************************************************************!*\
  !*** ./src/main/js/src/wrappers/hero-slider/HeroSliderFive.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-id-swiper */ "./node_modules/react-id-swiper/lib/index.js");
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_id_swiper__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _data_hero_sliders_hero_slider_five_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../data/hero-sliders/hero-slider-five.json */ "./src/main/js/src/data/hero-sliders/hero-slider-five.json");
var _data_hero_sliders_hero_slider_five_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/hero-sliders/hero-slider-five.json */ "./src/main/js/src/data/hero-sliders/hero-slider-five.json", 1);
/* harmony import */ var _components_hero_slider_HeroSliderFiveSingle_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/hero-slider/HeroSliderFiveSingle.js */ "./src/main/js/src/components/hero-slider/HeroSliderFiveSingle.js");






var HeroSliderFive = function HeroSliderFive(_ref) {
  var spaceLeftClass = _ref.spaceLeftClass,
      spaceRightClass = _ref.spaceRightClass;
  var params = {
    effect: "fade",
    loop: true,
    speed: 1000,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    watchSlidesVisibility: true,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    renderPrevButton: function renderPrevButton() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        className: "swiper-button-prev ht-swiper-button-nav"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
        className: "pe-7s-angle-left"
      }));
    },
    renderNextButton: function renderNextButton() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        className: "swiper-button-next ht-swiper-button-nav"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
        className: "pe-7s-angle-right"
      }));
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-area ".concat(spaceLeftClass ? spaceLeftClass : "", " ").concat(spaceRightClass ? spaceRightClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "slider-active nav-style-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_id_swiper__WEBPACK_IMPORTED_MODULE_2___default.a, params, _data_hero_sliders_hero_slider_five_json__WEBPACK_IMPORTED_MODULE_3__ && _data_hero_sliders_hero_slider_five_json__WEBPACK_IMPORTED_MODULE_3__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_hero_slider_HeroSliderFiveSingle_js__WEBPACK_IMPORTED_MODULE_4__["default"], {
      data: single,
      key: key,
      sliderClass: "swiper-slide"
    });
  }))));
};

HeroSliderFive.propTypes = {
  spaceLeftClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceRightClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (HeroSliderFive);

/***/ }),

/***/ "./src/main/js/src/wrappers/product/TabProductFour.js":
/*!************************************************************!*\
  !*** ./src/main/js/src/wrappers/product/TabProductFour.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/Tab */ "./node_modules/react-bootstrap/esm/Tab.js");
/* harmony import */ var react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap/Nav */ "./node_modules/react-bootstrap/esm/Nav.js");
/* harmony import */ var _components_section_title_SectionTitleThree__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/section-title/SectionTitleThree */ "./src/main/js/src/components/section-title/SectionTitleThree.js");
/* harmony import */ var _ProductGridTwo__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ProductGridTwo */ "./src/main/js/src/wrappers/product/ProductGridTwo.js");








var TabProductFour = function TabProductFour(_ref) {
  var spaceBottomClass = _ref.spaceBottomClass,
      category = _ref.category,
      productTabClass = _ref.productTabClass;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "product-area ".concat(spaceBottomClass ? spaceBottomClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_section_title_SectionTitleThree__WEBPACK_IMPORTED_MODULE_5__["default"], {
    titleText: "Featured Product",
    positionClass: "text-center"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Container, {
    defaultActiveKey: "bestSeller"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"], {
    variant: "pills",
    className: "product-tab-list pt-35 pb-60 text-center ".concat(productTabClass ? productTabClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Link, {
    eventKey: "newArrival"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, "New Arrivals"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Link, {
    eventKey: "bestSeller"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, "Best Sellers"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Item, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Nav__WEBPACK_IMPORTED_MODULE_4__["default"].Link, {
    eventKey: "saleItems"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", null, "Sale Items")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Content, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
    eventKey: "newArrival"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductGridTwo__WEBPACK_IMPORTED_MODULE_6__["default"], {
    category: category,
    type: "new",
    limit: 8,
    spaceBottomClass: "mb-25"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
    eventKey: "bestSeller"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductGridTwo__WEBPACK_IMPORTED_MODULE_6__["default"], {
    category: category,
    type: "bestSeller",
    limit: 8,
    spaceBottomClass: "mb-25"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_bootstrap_Tab__WEBPACK_IMPORTED_MODULE_3__["default"].Pane, {
    eventKey: "saleItems"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ProductGridTwo__WEBPACK_IMPORTED_MODULE_6__["default"], {
    category: category,
    type: "saleItems",
    limit: 8,
    spaceBottomClass: "mb-25"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "view-more text-center mt-20 toggle-btn6 col-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    className: "loadMore6",
    to: process.env.PUBLIC_URL + "/shop-grid-standard"
  }, "VIEW MORE PRODUCTS"))));
};

TabProductFour.propTypes = {
  category: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (TabProductFour);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./src/main/js/src/wrappers/testimonial/TestimonialOne.js":
/*!****************************************************************!*\
  !*** ./src/main/js/src/wrappers/testimonial/TestimonialOne.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-id-swiper */ "./node_modules/react-id-swiper/lib/index.js");
/* harmony import */ var react_id_swiper__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_id_swiper__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _data_testimonial_testimonial_one_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../data/testimonial/testimonial-one.json */ "./src/main/js/src/data/testimonial/testimonial-one.json");
var _data_testimonial_testimonial_one_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../data/testimonial/testimonial-one.json */ "./src/main/js/src/data/testimonial/testimonial-one.json", 1);
/* harmony import */ var _components_testimonial_TestimonialOneSingle_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/testimonial/TestimonialOneSingle.js */ "./src/main/js/src/components/testimonial/TestimonialOneSingle.js");






var TestimonialOne = function TestimonialOne(_ref) {
  var spaceTopClass = _ref.spaceTopClass,
      spaceBottomClass = _ref.spaceBottomClass,
      spaceLeftClass = _ref.spaceLeftClass,
      spaceRightClass = _ref.spaceRightClass,
      bgColorClass = _ref.bgColorClass;
  // swiper slider settings
  var settings = {
    slidesPerView: 1,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "testimonial-area ".concat(spaceTopClass ? spaceTopClass : "", "  ").concat(spaceBottomClass ? spaceBottomClass : "", " ").concat(spaceLeftClass ? spaceLeftClass : "", "  ").concat(spaceRightClass ? spaceRightClass : "", " ").concat(bgColorClass ? bgColorClass : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "col-lg-10 ml-auto mr-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "testimonial-active nav-style-1 nav-testi-style"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_id_swiper__WEBPACK_IMPORTED_MODULE_2___default.a, settings, _data_testimonial_testimonial_one_json__WEBPACK_IMPORTED_MODULE_3__ && _data_testimonial_testimonial_one_json__WEBPACK_IMPORTED_MODULE_3__.map(function (single, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_testimonial_TestimonialOneSingle_js__WEBPACK_IMPORTED_MODULE_4__["default"], {
      data: single,
      key: key,
      sliderClass: "swiper-slide"
    });
  })))))));
};

TestimonialOne.propTypes = {
  bgColorClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceBottomClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceLeftClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceRightClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  spaceTopClass: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (TestimonialOne);

/***/ })

}]);
//# sourceMappingURL=42.bundle.js.map