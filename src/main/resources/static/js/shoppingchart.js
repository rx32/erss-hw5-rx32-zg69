
function addToCart(productID, productName, productDesc, productImg, productPrice) {

    if ($("#quantity").val() === "0") {
        $('#error_info_add_to_cart').html("Quantity Must Larger Than 0.");
        return;
    }
    $('#error_info_add_to_cart').html("");
    console.log(productID);
    console.log(productName);
    console.log(productDesc);
    console.log(productImg);
    console.log(productPrice);

    let cart;
    if (localStorage.getItem("cart") === null) {
        cart = {products:[]};
    } else {
        cart = JSON.parse(localStorage.getItem("cart"));
    }
    let product = {productID, productName, productDesc, productImg, productPrice};
    cart.products.push({product, quantity: $("#quantity").val()});
    localStorage.setItem("cart", JSON.stringify(cart));
    showInNavBar();
}

function cleanCart() {
    cart = {products:[]};
    localStorage.setItem("cart", JSON.stringify(cart));
    showInNavBar();
    showInCartTable();
}

function showInNavBar() {
    let cart = JSON.parse(localStorage.getItem("cart"));
    $("#navbar-cart").empty();
    for (let productInfo of cart.products) {
        console.log(productInfo);
        $("#navbar-cart").append(`
            <li class="furgan-mini-cart-item mini_cart_item">
                <a href="#">
                    <img src="${productInfo.product.productImg}"
                    style="object-fit: cover; object-position: 100% 0; width: 30px; height: 50px;"
                         class="attachment-furgan_thumbnail size-furgan_thumbnail"
                         alt="img" width="600" height="778">${productInfo.product.productName.substr(0, 20)}&nbsp;
                </a>
                <span class="quantity">${productInfo.quantity} × <span
                    class="furgan-Price-amount amount"><span
                    class="furgan-Price-currencySymbol">$</span>150.00</span></span>
            </li>
        `);
    }
    $("#cart-count").html(cart.products.length);

}


function showIntbody() {
    let cart = JSON.parse(localStorage.getItem("cart"));
    window.jquery = $;
    for (let productInfo of cart.products) {
        $("#cart_table tbody").append(`
            <tr class="cart_item">
                <td class="product-name">
                    ${productInfo.product.productName.substr(0, 10)}&nbsp;&nbsp; <strong class="product-quantity">×
                    ${productInfo.quantity}</strong></td>
                <td class="product-total">
                    <span class="furgan-Price-amount amount">
                    <span class="furgan-Price-currencySymbol">$</span>150.00</span>
                </td>
            </tr>
        `);
    }
}


function showInCartTable() {
    $("#cart-stupid-table tbody").empty();
    let cart = JSON.parse(localStorage.getItem("cart"));
    for (let productInfo of cart.products) {
        console.log(productInfo);
        $("#cart-stupid-table tbody").append(`
                                <tr class="furgan-cart-form__cart-item cart_item">
                                <td></td>
                                    <td class="product-thumbnail">
                                        <a href="#"><img
                                                style="object-fit: cover; object-position: 100% 0; width: 40px; height: 30px;"
                                                src="${productInfo.product.productImg}"
                                                class="attachment-furgan_thumbnail size-furgan_thumbnail"
                                                alt="img" width="600" height="778"></a></td>
                                    <td class="product-name" data-title="Product">
                                        <a href="#">${productInfo.product.productName}</a></td>
                                    <td class="product-price" data-title="Price">
                                        <span class="furgan-Price-amount amount"><span
                                                class="furgan-Price-currencySymbol">$</span>${productInfo.product.productPrice}</span></td>
                                    <td class="product-quantity" data-title="Quantity">
                                        <div class="quantity">
                                            <span class="furgan-Price-amount amount"><span
                                                class="furgan-Price-currencySymbol"></span>${productInfo.quantity}</span></td>
                                        </div>
                                    </td>
                                    <td class="product-subtotal" data-title="Total">
                                        <span class="furgan-Price-amount amount"><span
                                                class="furgan-Price-currencySymbol">$</span>0</span></td>
                                </tr>
        `);
    }
    $("#cart-stupid-table tbody").append(`
        <tr>
            <td colspan="6" class="actions">
                <div class="coupon">
                    <button onclick="cleanCart()"
                            type="button" class="button" name="apply_coupon"
                            value="Apply coupon">Clean Cart
                    </button>
                </div>
                <button type="submit"  class="button">
                    <a href="/checkout" style="color: whitesmoke" >Check Out</a>
                </button>
            </td>
        </tr>
`);
}


function checkOut() {

    if ($("#billing_addr_text_X").val() === "" || $("#billing_addr_text_Y").val() === "" || $("#upsAccount").val() === "" || $("#billing_first_name").val() === "" || $("#billing_last_name").val() === "") {
        $("#error_info_checkout").html("Required Field Cannot Be Empty!");
        return;
    }
    if (!Number.isInteger(parseInt($("#billing_addr_text_X").val())) || !Number.isInteger(parseInt($("#billing_addr_text_Y").val()))) {
        $("#error_info_checkout").html("Address Cordinate Should Be Integer!");
        return;
    }
    $("#error_info_checkout").html("");

    let cart = JSON.parse(localStorage.getItem("cart"));

    $("#place_order").prop("disabled", true);
    for (let productInfo of cart.products) {
        console.log(productInfo);
        let data = {
            "quantity": productInfo.quantity,
            "x": $("#billing_addr_text_X").val(),
            "y": $("#billing_addr_text_Y").val(),
        };
        console.log(data);
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: `/buy/${productInfo.product.productID}`,
            data: JSON.stringify(data),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
                var json = "<h4>Ajax Response</h4>&lt;pre&gt;"
                    + JSON.stringify(data, null, 4) + "&lt;/pre&gt;";
                $('#feedback').html(json);
                console.log("SUCCESS : ", data);
                // window.location.replace("http://www.w3schools.com");
                $("#place_order").prop("disabled", false);
                window.location.replace(location.protocol + '//' + location.hostname + ':8080/order');
                cleanCart();
            },
            error: function (e) {
                var json = "<h4>Ajax Response</h4>&lt;pre&gt;"
                    + e.responseText + "&lt;/pre&gt;";
                $('#feedback').html(json);
                console.log("ERROR : ", e);
                // window.location.replace(location.protocol + '//' + location.hostname + '/order');
                $("#place_order").prop("disabled", false);

            }
        });
    }
}