package rxzg.miniamazon.protobuf;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import static org.junit.jupiter.api.Assertions.*;

class WorldConnectorTest {
//  @Test
  public void test() throws IOException {
    Socket clientSocket = new Socket("vcm-13262.vm.duke.edu", 23456);
    OutputStream outputStream = clientSocket.getOutputStream();
    InputStream inputStream = clientSocket.getInputStream();

    WorldAmazon.AInitWarehouse.Builder aInitWarehouseBuilder = WorldAmazon.AInitWarehouse.newBuilder();
    WorldAmazon.AConnect.Builder aConnectBuilder = WorldAmazon.AConnect.newBuilder();

    aInitWarehouseBuilder.setId(0).setX(0).setY(50);
    aConnectBuilder.addInitwh(aInitWarehouseBuilder.build());

    aInitWarehouseBuilder.setId(1).setX(50).setY(0);
    aConnectBuilder.addInitwh(aInitWarehouseBuilder.build());

    aConnectBuilder.setIsAmazon(true);

    aConnectBuilder.build().writeDelimitedTo(outputStream);

    WorldAmazon.AConnected aConnected = WorldAmazon.AConnected.parseDelimitedFrom(inputStream);
    System.out.println(aConnected);

    WorldAmazon.AProduct aProduct = WorldAmazon.AProduct.newBuilder()
        .setId(1)
        .setCount(20)
        .setDescription("organic orange from CA")
        .build();
    WorldAmazon.APurchaseMore aPurchaseMoreTest = WorldAmazon.APurchaseMore.newBuilder()
        .setSeqnum(1)
        .addThings(aProduct)
        .setWhnum(1)
        .build();
    try {
      WorldAmazon.ACommands aCommands = WorldAmazon.ACommands.newBuilder().addBuy(aPurchaseMoreTest).build();
      aCommands.writeDelimitedTo(outputStream);
    } catch (IOException e) {
      // we do not need to handle this exception, retry mechanism takes care of it.
      e.printStackTrace();
    }

    try {
      WorldAmazon.AResponses aResponses = WorldAmazon.AResponses.parseDelimitedFrom(inputStream);
      for (WorldAmazon.APurchaseMore aPurchaseMore : aResponses.getArrivedList()) {
        System.out.println(aPurchaseMore);
      }

      for (WorldAmazon.APacked aPacked : aResponses.getReadyList()) {

      }

    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}