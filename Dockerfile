# FROM maven:3.6.3-jdk-8
# COPY src /home/app/src
# COPY pom.xml /home/app
# RUN mvn -f /home/app/pom.xml install -DskipTests
# RUN mvn -f /home/app/pom.xml spring-boot:run
FROM openjdk:8
RUN mkdir /code
WORKDIR /code
COPY ./target/mini-amazon-0.0.1-SNAPSHOT.jar /code/mini-amazon-0.0.1-SNAPSHOT.jar
ADD ./runserver.sh /code/runserver.sh
RUN ls /code